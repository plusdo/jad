package com.jad.commons.enums;

/**
 * 控件类型
 * @author Administrator
 *
 */
public enum DataType {
	
	DEFAULT(0, "默认"), 
	INT(1, "整数"), FLOAT(2, "数字"),
	STR(3, "字符串");
	
	private final int type;

	private final String desc;

	DataType(final int type, final String desc) {
		this.type = type;
		this.desc = desc;
	}
	
}



