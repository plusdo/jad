package com.jad.commons.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jad.commons.enums.DataType;
import com.jad.commons.enums.WidgetType;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface InsertConf {
	
	boolean value() default true;
	
	String label() default "";
	
	DataType dataType() default DataType.DEFAULT;
	
	WidgetType widgetType() default WidgetType.DEFAULT;
	
	boolean required() default false;
	
}







