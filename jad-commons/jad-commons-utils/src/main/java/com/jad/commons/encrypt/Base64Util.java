package com.jad.commons.encrypt;

import org.apache.commons.codec.binary.Base64;

public class Base64Util {

	

	/**
	 * <p>
	 * BASE64字符串解码为二进制数据
	 * </p>
	 * 
	 * @param base64
	 * @return
	 * @throws Exception
	 */
	public static byte[] decode(String base64)  {
		return Base64.decodeBase64(base64);
	}

	/**
	 * <p>
	 * 二进制数据编码为BASE64字符串
	 * </p>
	 * 
	 * @param bytes
	 * @return
	 * @throws Exception
	 */
	public static String encode(byte[] bytes) throws Exception {
		return Base64.encodeBase64String(bytes);
	}

//	/**
//	 * BASE64 encrypt
//	 * 
//	 * @param key
//	 * @return
//	 * @throws Exception
//	 */
//	public static String encryptBASE64(byte[] key) throws Exception {
//		return encryptBASE64(key,CharsetEncoding.DEFAULT_ENCODING);
//	}
//	
//	public static String encryptBASE64(byte[] key,String encoding) throws Exception {
//		return StringUtils.newString(Base64.encodeBase64(key), encoding);
//	}
	

//	/**
//	 * BASE64 decrypt
//	 * 
//	 * @param key
//	 * @return
//	 * @throws Exception
//	 */
//	public static byte[] decryptBASE64(String key) throws Exception {
//		return decryptBASE64(key,CharsetEncoding.DEFAULT_ENCODING);
//	}
//	public static byte[] decryptBASE64(String key,String encoding) throws Exception {
//		return Base64.decodeBase64(key.getBytes(encoding));
//	}
	



}
