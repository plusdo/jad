/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service;

import com.jad.commons.service.CurdService;
import com.jad.core.cms.vo.ArticleDataVo;

/**
 * @version 2013-01-15
 */
public interface ArticleDataService extends CurdService<ArticleDataVo,String> {
}
