package com.jad.core.cms.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;
import com.jad.commons.qo.BaseQo;

public class GuestbookQo extends BaseQo<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="回复人",queryConf=@QueryConf(property="reUser.id"))
	private String reUserId; 		// 回复人
	
	@CURD(label="留言分类")
	private String type; 	// 留言分类（咨询、建议、投诉、其它）
	
	@CURD(label="留言内容")
	private String contentLike;
	
	public GuestbookQo() {
	}
	
	public GuestbookQo(String id) {
		super(id);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContentLike() {
		return contentLike;
	}

	public void setContentLike(String contentLike) {
		this.contentLike = contentLike;
	}

	public String getReUserId() {
		return reUserId;
	}

	public void setReUserId(String reUserId) {
		this.reUserId = reUserId;
	}
	
	
	
}
