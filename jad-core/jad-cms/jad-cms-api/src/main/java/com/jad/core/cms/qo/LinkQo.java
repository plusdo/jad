package com.jad.core.cms.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;
import com.jad.commons.qo.BaseQo;

public class LinkQo extends BaseQo<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="分类编号",queryConf=@QueryConf(property="category.id"))
	private String categoryId;// 分类编号
	
	@CURD(label="标题")
	private String title;	// 标题
	
	public LinkQo() {
	}
	
	public LinkQo(String id) {
		super(id);
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	
	
}
