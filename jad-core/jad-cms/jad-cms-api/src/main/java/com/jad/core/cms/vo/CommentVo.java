/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;
import com.jad.core.sys.vo.UserVo;

/**
 * 评论Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
public class CommentVo extends BaseVo<String> {

	private static final long serialVersionUID = 1L;
	private CategoryVo category;// 分类编号
	
	@CURD(label="归属分类内容的编号")
	private String contentId;	// 归属分类内容的编号（Article.id、Photo.id、Download.id）
	
	@CURD(label="归属分类内容的标题")
	private String title;	// 归属分类内容的标题（Article.title、Photo.title、Download.title）
	
	@CURD(label="评论内容")
	private String content; // 评论内容
	
	@CURD(label="评论姓名")
	private String name; 	// 评论姓名
	
	@CURD(label="评论IP")
	private String ip; 		// 评论IP
	
	@CURD(label="审核时间")
	private Date auditDate;	// 审核时间
	
	private UserVo auditUser; // 审核人

	public CommentVo() {
		super();
	}
	
	public CommentVo(String id){
		this();
		this.id = id;
	}
	
	@JsonIgnore
	public CategoryVo getCategory() {
		return category;
	}

	public void setCategory(CategoryVo category) {
		this.category = category;
	}

	@NotNull
	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	@Length(min=1, max=255)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Length(min=1, max=255)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Length(min=1, max=100)
	public String getName() {
		return name;
	}

	@JsonIgnore
	public UserVo getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(UserVo auditUser) {
		this.auditUser = auditUser;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}


}