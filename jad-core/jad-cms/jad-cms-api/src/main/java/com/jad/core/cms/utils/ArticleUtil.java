package com.jad.core.cms.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jad.core.cms.vo.ArticleVo;

public class ArticleUtil {

	 /**
     * 从图片地址中加入ContextPath地址
   	 * @param src
   	 * @return src
   	 */
    public static String formatImageSrcToWeb(String src,String contextPath) {
        if(StringUtils.isBlank(src)) return src;
        if(src.startsWith(contextPath + "/userfiles")){
            return src;
        }else{
            return contextPath+src;
        }
    }
	
   	
    /**
     * 获得文章动态URL地址
   	 * @param article
   	 * @return url
   	 */
    public static String getUrlDynamic(ArticleVo article,String contextPath,String frontPath,String urlSuffix) {
        if(StringUtils.isNotBlank(article.getLink())){
            return article.getLink();
        }
        StringBuilder str = new StringBuilder();
//        str.append(context.getContextPath()).append(Global.getFrontPath());
//        str.append("/view-").append(article.getCategory().getId()).append("-").append(article.getId()).append(Global.getUrlSuffix());
        str.append(contextPath).append(frontPath);
        str.append("/view-").append(article.getCategory().getId()).append("-").append(article.getId()).append(urlSuffix);
        return str.toString();
    }
    
    
    /**
     * 获得推荐位列表
     * @param posid
     * @return
     */
    public static List<String> getPosidList(String posid) {
    	List<String> list = new ArrayList();
		if (posid != null){
			for (String s : StringUtils.split(posid, ",")) {
				list.add(s);
			}
		}
		return list;
    	
    }
    
    
    
    
    
    
    
}
