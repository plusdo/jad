/**
\ * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.vo;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;
import com.jad.core.sys.vo.UserVo;

/**
 * 链接Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
public class LinkVo extends BaseVo<String> {
	
	private static final long serialVersionUID = 1L;
	
	private CategoryVo category;// 分类
	
	@CURD(label="链接名称")
	private String title;	// 链接名称
	
	@CURD(label="标题颜色")
	private String color;	// 标题颜色（red：红色；green：绿色；blue：蓝色；yellow：黄色；orange：橙色）
	
	@CURD(label="链接图片")
	private String image;	// 链接图片
	
	@CURD(label="链接地址")
	private String href;	// 链接地址
	
	@CURD(label="权重")
	private Integer weight;	// 权重，越大越靠前
	
	@CURD(label="权重期限")
	private Date weightDate;// 权重期限，超过期限，将weight设置为0
	
	private UserVo user;//创建人

	public LinkVo() {
		super();
	}
	
	public LinkVo(String id){
		this();
		this.id = id;
	}


	@NotNull
	@JsonIgnore
	public CategoryVo getCategory() {
		return category;
	}

	public void setCategory(CategoryVo category) {
		this.category = category;
	}

	@Length(min=1, max=255)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Length(min=0, max=50)
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Length(min=0, max=255)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Length(min=0, max=255)
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Date getWeightDate() {
		return weightDate;
	}

	public void setWeightDate(Date weightDate) {
		this.weightDate = weightDate;
	}

	@JsonIgnore
	public UserVo getUser() {
		return user;
	}

	public void setUser(UserVo user) {
		this.user = user;
	}

	
}