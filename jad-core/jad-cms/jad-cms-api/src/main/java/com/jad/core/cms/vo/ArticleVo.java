/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.vo;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;
import com.jad.core.cms.utils.ArticleUtil;
import com.jad.core.sys.vo.UserVo;

/**
 * 文章Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
public class ArticleVo extends BaseVo<String> {

    public static final String DEFAULT_TEMPLATE = "frontViewArticle";
	
	private static final long serialVersionUID = 1L;
	
	private CategoryVo category;// 分类
	
	@CURD(label="标题")
	private String title;	// 标题
	
	@CURD(label="外部链接")
    private String link;	// 外部链接
	
	@CURD(label="标题颜色")
	private String color;	// 标题颜色（red：红色；green：绿色；blue：蓝色；yellow：黄色；orange：橙色）
	
	@CURD(label="文章图片")
	private String image;	// 文章图片
	
	@CURD(label="关键字")
	private String keywords;// 关键字
	
	@CURD(label="描述")
	private String description;// 描述、摘要
	
	@CURD(label="权重")
	private Integer weight;	// 权重，越大越靠前
	
	@CURD(label="权重期限")
	private Date weightDate;// 权重期限，超过期限，将weight设置为0
	
	@CURD(label="点击数")
	private Integer hits;	// 点击数
	
	@CURD(label="推荐位")
	private String posid;	// 推荐位，多选（1：首页焦点图；2：栏目页文章推荐；）
	
	@CURD(label="自定义内容视图")
    private String customContentView;	// 自定义内容视图
	
	@CURD(label="视图参数")
   	private String viewConfig;	// 视图参数
	
	private ArticleDataVo articleData;	//文章内容
	
	private UserVo user;//创建人
    
	private String url;
	
	public ArticleVo() {
		super();
	}

	public ArticleVo(String id){
		this();
		this.id = id;
	}
	
	
	@JsonIgnore
	public UserVo getUser() {
		return user;
	}

	public void setUser(UserVo user) {
		this.user = user;
	}

	@JsonIgnore
	public CategoryVo getCategory() {
		return category;
	}

	public void setCategory(CategoryVo category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

    @Length(min=0, max=255)
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

	@Length(min=0, max=50)
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Length(min=0, max=255)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
        this.image = image;
	}

	@Length(min=0, max=255)
	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	@Length(min=0, max=255)
	public String getDescription() {
		return description;
	}

	

	public void setDescription(String description) {
		this.description = description;
	}

	@NotNull
	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Date getWeightDate() {
		return weightDate;
	}

	public void setWeightDate(Date weightDate) {
		this.weightDate = weightDate;
	}

	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}

	@Length(min=0, max=10)
	public String getPosid() {
		return posid;
	}

	public void setPosid(String posid) {
		this.posid = posid;
	}

    public String getCustomContentView() {
        return customContentView;
    }

    public void setCustomContentView(String customContentView) {
        this.customContentView = customContentView;
    }

    public String getViewConfig() {
        return viewConfig;
    }

    public void setViewConfig(String viewConfig) {
        this.viewConfig = viewConfig;
    }

    @JsonIgnore
	public ArticleDataVo getArticleData() {
		return articleData;
	}

	public void setArticleData(ArticleDataVo articleData) {
		this.articleData = articleData;
	}

	@JsonIgnore
	public List<String> getPosidList() {
		return ArticleUtil.getPosidList(posid);
	}


	public void setPosidList(List<String> list) {
		posid = ","+StringUtils.join(list, ",")+",";
	}

	public String getUrl() {
        return url;
   	}
  	
	public void setUrl(String url) {
		this.url=url;
	}
	
//   	public String getUrl(String contextPath,String frontPath,String urlSuffix) {
//        return ArticleUtil.getUrlDynamic(this,contextPath,frontPath,urlSuffix);
//   	}

   	public String getImageSrc(String contextPath) {
        return ArticleUtil.formatImageSrcToWeb(this.image,contextPath);
   	}
   	
   
    
    
}


