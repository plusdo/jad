package com.jad.core.cms.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.qo.BaseQo;


/**
 * 站点查询对像
 * @author Administrator
 *
 */
public class SiteQo extends BaseQo<String> {
	
	private static final long serialVersionUID = 1L;
	
	@CURD(label="站点名称")
	private String name;	// 站点名称
	
	@CURD(label="站点标题")
	private String title;	// 站点标题
	
	public SiteQo(String id){
		super(id);
	}
	
	public SiteQo(){}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

}
