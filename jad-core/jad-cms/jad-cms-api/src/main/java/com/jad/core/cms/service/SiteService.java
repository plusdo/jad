/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service;

import com.jad.commons.service.CurdService;
import com.jad.core.cms.vo.SiteVo;

/**
 * 站点Service
 * @author ThinkGem
 * @version 2013-01-15
 */
public interface SiteService extends CurdService<SiteVo,String> {
	
//	public void delete(SiteVo site, Boolean isRe) ;
	
	/**
	 * 获取当前编辑的站点编号
	 */
//	public String getCurrentSiteId();
	
	/**
	 * 获取默认站点编号
	 * @return
	 */
//	public String defaultSiteId();
}
