package com.jad.core.cms.qo;

import java.util.Date;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;
import com.jad.commons.qo.TreeQo;

/**
 * 栏目查询对像
 * 
 * @author Administrator
 *
 */
public class CategoryQo extends TreeQo<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 站点id
	 */
	@CURD(label="站点id",queryConf=@QueryConf(property="site.id"))
	private String siteId;

	@CURD(label="是否在导航中显示")
	private String inMenu; // 是否在导航中显示（1：显示；0：不显示）

	@CURD(label="开始时间")
	private Date updateDateBegin; // 开始时间
	
	@CURD(label="结束时间")
	private Date updateDateEnd; // 结束时间
	
	@CURD(label="归属部门",queryConf=@QueryConf(property="office.id"))
	private String officeId;//归属部门id

	public String getInMenu() {
		return inMenu;
	}

	public void setInMenu(String inMenu) {
		this.inMenu = inMenu;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public Date getUpdateDateBegin() {
		return updateDateBegin;
	}

	public void setUpdateDateBegin(Date updateDateBegin) {
		this.updateDateBegin = updateDateBegin;
	}

	public Date getUpdateDateEnd() {
		return updateDateEnd;
	}

	public void setUpdateDateEnd(Date updateDateEnd) {
		this.updateDateEnd = updateDateEnd;
	}

	public String getOfficeId() {
		return officeId;
	}

	public void setOfficeId(String officeId) {
		this.officeId = officeId;
	}

}
