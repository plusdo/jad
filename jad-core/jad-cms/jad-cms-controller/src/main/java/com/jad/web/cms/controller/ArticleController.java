/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.core.cms.qo.ArticleQo;
import com.jad.core.cms.service.ArticleDataService;
import com.jad.core.cms.service.ArticleService;
import com.jad.core.cms.service.CategoryService;
import com.jad.core.cms.service.SiteService;
import com.jad.core.cms.utils.FileTplUtil;
import com.jad.core.cms.vo.ArticleDataVo;
import com.jad.core.cms.vo.ArticleVo;
import com.jad.core.cms.vo.CategoryVo;
import com.jad.core.sys.service.UserService;
import com.jad.web.cms.util.CmsApiUtils;
import com.jad.web.cms.util.SiteHelper;
import com.jad.web.cms.util.TplApiUtils;
import com.jad.web.mvc.BaseController;

/**
 * 文章Controller
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/article")
@Api(description = "文章管理")
public class ArticleController extends BaseController {

	@Autowired
	private ArticleService articleService;
	@Autowired
	private UserService userService;
	@Autowired
	private ArticleDataService articleDataService;
	@Autowired
	private CategoryService categoryService;
    @Autowired
   	private SiteService siteService;
//    @Autowired
//    private ServletContext context;
    
//    @Autowired
//   	private SystemService systemService;
    
	
//	@ModelAttribute
//	public ArticleVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return articleService.findById(id);
//		}else{
//			return new ArticleVo();
//		}
//	}
    
    /**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:article:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看文章信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<ArticleVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(articleService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:article:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询文章列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<ArticleVo>> findPage(ArticleQo articleQo,PageQo pageQo){
		Page<ArticleVo> page = articleService.findPage(pageQo, articleQo);
		return Result.newSuccResult(page);
	}
	
	/**
	 * 修改
	 * @param drticleVo
	 * @return
	 */
	@RequiresPermissions("sys:article:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改文章信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(ArticleVo drticleVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, drticleVo)){
			return result;
		}
		if(StringUtils.isBlank(drticleVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(drticleVo,SecurityHelper.getCurrentUserId());
		articleService.update(drticleVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:article:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增文章信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(ArticleVo drticleVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, drticleVo)){
			return result;
		}
		preInsert(drticleVo,SecurityHelper.getCurrentUserId());
		articleService.add(drticleVo);
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:article:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除文章信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		ArticleVo delVo = new ArticleVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		articleService.delete(delVo);
		return result;
	}
	
	
	
	@RequiresPermissions("cms:article:view")
	@RequestMapping(value = {"list", ""})
	public String list(ArticleQo articleQo, HttpServletRequest request, HttpServletResponse response, Model model) {
//		for (int i=0; i<10000000; i++){
//			Article a = new Article();
//			a.setCategory(new Category(article.getCategory().getId()));
//			a.setTitle("测试测试测试测试测试测试测试测试"+a.getCategory().getId());
//			a.setArticleData(new ArticleData());
//			a.getArticleData().setContent(a.getTitle());
//			articleService.save(a);
//		}
		ArticleVo articleVo = new ArticleVo();
		
		CategoryVo categoryVo = new CategoryVo();
		if(StringUtils.isNotBlank(articleQo.getCategoryId())){
			categoryVo = categoryService.findById(articleQo.getCategoryId());
		}
        Page<ArticleVo> page = articleService.findPage(getPage(request, response), articleQo); 
        
        if(Page.isNotEmpty(page)){
        	for(ArticleVo vo:page.getList()){
        		if(StringUtils.isNotBlankStr(vo.getCreateBy())){
        			vo.setUser(userService.findById(vo.getCreateBy()));
        		}
        	}
        }
        articleVo.setCategory(categoryVo);
        model.addAttribute("articleVo", articleVo);
        model.addAttribute("page", page);
		return "modules/cms/articleList";
	}

	
	@RequiresPermissions("cms:article:view")
	@RequestMapping(value = "form")
	public String form(ArticleQo articleQo, Model model,HttpServletRequest request) {
		
		ArticleVo articleVo = new ArticleVo();
		CategoryVo categoryVo = new CategoryVo();
		if(StringUtils.isNotBlank(articleQo.getCategoryId())){
			categoryVo = categoryService.findById(articleQo.getCategoryId());
		}
		articleVo.setCategory(categoryVo);
		
//		// 如果当前传参有子节点，则选择取消传参选择
//		if (articleVo.getCategory()!=null && StringUtils.isNotBlank(articleVo.getCategory().getId())){
//			CategoryQo qo=new CategoryQo();
//			qo.setParentId(articleQo.getCategory().getId());
//			qo.setSiteId(SiteHelper.getCurrentSiteId());
//			List<CategoryVo> list = categoryService.findList(qo);
//			if (list.size() > 0){
//				articleVo.setCategory(null);
//			}else{
//				articleVo.setCategory(categoryService.findById(articleVo.getCategory().getId()));
//			}
//		}
		
		if(StringUtils.isNotBlank(articleQo.getId())){
			articleVo = articleService.findById(articleQo.getId());
			if(articleVo!=null){
				articleVo.setArticleData(articleDataService.findById(articleVo.getId()));
			}
			
		}
		
		
		
//		if (article.getCategory()=null && StringUtils.isNotBlank(article.getCategory().getId())){
//			Category category = categoryService.get(article.getCategory().getId());
//		}
		
		return toForm(articleVo,model,request);
//        model.addAttribute("contentViewList",getTplContent());
//        model.addAttribute("article_DEFAULT_TEMPLATE",ArticleVo.DEFAULT_TEMPLATE);
//		model.addAttribute("article", articleVo);
//		CmsApiUtils.addViewConfigAttribute(model, articleVo.getCategory());
//		return "modules/cms/articleForm";
	}

	
	public String toForm(ArticleVo articleVo, Model model,HttpServletRequest request) {
		model.addAttribute("contentViewList",getTplContent(request));
	    model.addAttribute("article_DEFAULT_TEMPLATE",ArticleVo.DEFAULT_TEMPLATE);
		model.addAttribute("articleVo", articleVo);
		CmsApiUtils.addViewConfigAttribute(model, articleVo.getCategory());
		return "modules/cms/articleForm";
		
	}
	
	@RequiresPermissions("cms:article:edit")
	@RequestMapping(value = "save")
	public String save(ArticleVo articleVo, Model model, RedirectAttributes redirectAttributes
			,HttpServletRequest request, HttpServletResponse response) {
		if (Global.isDemoMode()) {
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return this.list(new ArticleQo(), request, response, model);
		}
		if (!beanValidator(model, articleVo)){
			return toForm(articleVo, model,request);
		}
		
		if(StringUtils.isNotBlank(articleVo.getId())){
			preUpdate(articleVo, SecurityHelper.getCurrentUserId());
			articleService.update(articleVo);
			
		}else{
			preInsert(articleVo, SecurityHelper.getCurrentUserId());
			articleService.add(articleVo);
		}
//		articleService.save(article);
		addMessage(redirectAttributes, "保存文章'" + StringUtils.abbr(articleVo.getTitle(),50) + "'成功");
		String categoryId = articleVo.getCategory()!=null?articleVo.getCategory().getId():null;
		return "redirect:" + adminPath + "/cms/article/?repage&categoryId="+(categoryId!=null?categoryId:"");
	}
	
	@SuppressWarnings("rawtypes")
	protected <VO extends BaseVo> void preInsert(VO vo,String userId) {
		super.preInsert(vo, userId);
		ArticleVo articleVo = (ArticleVo)vo;
		if(articleVo.getWeight()==null){
			articleVo.setWeight(0);
		}
		if(articleVo.getHits()==null){
			articleVo.setHits(0);
		}
		if(articleVo.getPosid()==null){
			articleVo.setPosid("");
		}
	}
	
	@RequiresPermissions("cms:article:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, 
			String categoryId, @RequestParam(required=false) Boolean isRe, RedirectAttributes redirectAttributes
			,HttpServletRequest request, HttpServletResponse response) {
		// 如果没有审核权限，则不允许删除或发布。
//		if (!systemService.getSubject().isPermitted("cms:article:audit")){
//		if (!systemService.isPermitted("cms:article:audit")){
//			addMessage(redirectAttributes, "你没有删除或发布权限");
//		}
//		articleService.delete(articleVo, isRe);
		if (Global.isDemoMode()) {
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/cms/article/?repage&category.id="+(categoryId!=null?categoryId:"");
		}
		ArticleVo delVo = new ArticleVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		
		String delFlag = (isRe!=null && isRe.booleanValue())? Constants.DEL_FLAG_AUDIT:Constants.DEL_FLAG_DELETE;
		delVo.setDelFlag(delFlag);
		
		preUpdate(delVo, SecurityHelper.getCurrentUserId());
		articleService.update(delVo);
		
		addMessage(redirectAttributes, "操作成功");
		return "redirect:" + adminPath + "/cms/article/?repage&category.id="+(categoryId!=null?categoryId:"");
	}

	/**
	 * 文章选择列表
	 */
	@RequiresPermissions("cms:article:view")
	@RequestMapping(value = "selectList")
	public String selectList(ArticleQo articleQo, HttpServletRequest request, HttpServletResponse response, Model model) {
        list(articleQo, request, response, model);
		return "modules/cms/articleSelectList";
	}
	
	/**
	 * 通过编号获取文章标题
	 */
	@RequiresPermissions("cms:article:view")
	@ResponseBody
	@RequestMapping(value = "findByIds")
	public List<Object[]> findByIds(String ids) {
		List<Object[]> list = new ArrayList();
		if(StringUtils.isNotBlank(ids)){
			List<ArticleVo> voList = articleService.findByIdList(Arrays.asList(StringUtils.split(ids,",")));
			for(ArticleVo vo:voList){
				list.add(new Object[]{vo.getCategory().getId(),vo.getId(),StringUtils.abbr(vo.getTitle(),50)});
			}
		}
//		return JsonMapper.nonDefaultMapper().toJson(list);
		return list;
	}

    private List<String> getTplContent(HttpServletRequest request) {
    	String path=siteService.findById(SiteHelper.getCurrentSiteId()).getSolutionPath();
   		List<String> tplList = FileTplUtil.getNameListByPrefix(request.getSession().getServletContext().getRealPath(path),
   				request.getSession().getServletContext().getRealPath(""));
   		tplList = TplApiUtils.tplTrim(tplList, ArticleVo.DEFAULT_TEMPLATE, "");
   		return tplList;
   	}
}
