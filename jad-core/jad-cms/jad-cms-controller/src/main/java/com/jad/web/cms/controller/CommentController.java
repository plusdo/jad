/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.core.cms.qo.CommentQo;
import com.jad.core.cms.qo.CommentQo;
import com.jad.core.cms.service.CommentService;
import com.jad.core.cms.vo.CommentVo;
import com.jad.core.cms.vo.CommentVo;
import com.jad.core.cms.vo.SiteVo;
import com.jad.web.mvc.BaseController;

/**
 * 评论Controller
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/comment")
@Api(description="评论管理")
public class CommentController extends BaseController {

	@Autowired
	private CommentService commentService;
	
//	@Autowired
//	private SystemService systemService;
	
//	@ModelAttribute
//	public CommentVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return commentService.findById(id);
//		}else{
//			return new CommentVo();
//		}
//	}
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:comment:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看评论信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<CommentVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(commentService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:comment:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询评论列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<CommentVo>> findPage(CommentQo commentQo,PageQo pageQo){
		Page<CommentVo> page = commentService.findPage(pageQo, commentQo);
		return Result.newSuccResult(page);
	}
	
	/**
	 * 修改
	 * @param commentVo
	 * @return
	 */
	@RequiresPermissions("sys:comment:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改评论信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(CommentVo commentVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, commentVo)){
			return result;
		}

		if(StringUtils.isBlank(commentVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		if(commentVo.getAuditUser()==null){
			commentVo.setAuditUser(SecurityHelper.getCurrentUser());
		}
		commentVo.setDelFlag(Constants.DEL_FLAG_NORMAL);
		preUpdate(commentVo,SecurityHelper.getCurrentUserId());
		commentService.update(commentVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:comment:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增评论信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(CommentVo commentVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if(commentVo.getAuditUser()==null){
			commentVo.setAuditUser(SecurityHelper.getCurrentUser());
		}
		commentVo.setDelFlag(Constants.DEL_FLAG_NORMAL);
		
		if (!beanValidator(result, commentVo)){
			return result;
		}
		preInsert(commentVo,SecurityHelper.getCurrentUserId());
		commentService.add(commentVo);
		return result;
	}
	
	/**
	 * 删除或退回审核
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:comment:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除评论信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id,@RequestParam(required=false) Boolean isRe){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		CommentVo delVo = new CommentVo();
		delVo.setId(id);
		String delFlag = (isRe!=null && isRe.booleanValue())? Constants.DEL_FLAG_AUDIT:Constants.DEL_FLAG_DELETE;
		delVo.setDelFlag(delFlag);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		commentService.delete(delVo);
		return result;
	}
	
	/**
	 * 评论审核
	 * @param id
	 * @param isPass
	 * @return
	 */
	@RequiresPermissions("cms:comment:edit")
	@RequestMapping(value = "auditComment")
	@ResponseBody
	@ApiOperation(value = "评论审核",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<CommentVo> audit(@RequestParam(required=true)String id,  @RequestParam(required=false) Boolean isPass) {
		CommentVo vo = new CommentVo();
		vo.setId(id);
		preUpdate(vo,SecurityHelper.getCurrentUserId());
		vo.setAuditUser(SecurityHelper.getCurrentUser());
		String delFlag = (isPass!=null && isPass.booleanValue())? Constants.DEL_FLAG_NORMAL:Constants.DEL_FLAG_DELETE;
		vo.setDelFlag(delFlag);
		commentService.update(vo);
		
		return Result.newSuccResult();
	}
	
	
	@RequiresPermissions("cms:comment:view")
	@RequestMapping(value = {"list", ""})
	public String list(CommentQo commentQo, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CommentVo> page = commentService.findPage(getPage(request, response), commentQo); 
        model.addAttribute("page", page);
		return "modules/cms/commentList";
	}

	@RequiresPermissions("cms:comment:edit")
	@RequestMapping(value = "save")
	public String save(CommentVo commentVo, RedirectAttributes redirectAttributes) {
		if (beanValidator(redirectAttributes, commentVo)){
			if(commentVo.getAuditUser()==null){
				commentVo.setAuditUser(SecurityHelper.getCurrentUser());
			}
			commentVo.setDelFlag(Constants.DEL_FLAG_NORMAL);
			
			if(StringUtils.isNotBlank(commentVo.getId())){
				preUpdate(commentVo, SecurityHelper.getCurrentUserId());
				commentService.update(commentVo);
			}else{
				preInsert(commentVo, SecurityHelper.getCurrentUserId());
				commentService.add(commentVo);
			}
			
//			commentService.save(comment);
//			addMessage(redirectAttributes, DictUtils.getDictLabel(comment.getDelFlag(), "cms_del_flag", "保存")
//					+"评论'" + StringUtils.abbr(StringUtils.replaceHtml(comment.getContent()),50) + "'成功");
			addMessage(redirectAttributes,  "评论" + StringUtils.abbr(StringUtils.replaceHtml(commentVo.getContent()),50) + "'成功");
			
			
		}
		return "redirect:" + adminPath + "/cms/comment/?repage&delFlag=2";
	}
	
	
	@RequiresPermissions("cms:comment:edit")
	@RequestMapping(value = "audit")
	public String audit(@RequestParam(required=true)String id,  @RequestParam(required=false) Boolean isPass, RedirectAttributes redirectAttributes) {
		CommentVo vo = new CommentVo();
		vo.setId(id);
		preUpdate(vo,SecurityHelper.getCurrentUserId());
		vo.setAuditUser(SecurityHelper.getCurrentUser());
		String delFlag = (isPass!=null && isPass.booleanValue())? Constants.DEL_FLAG_NORMAL:Constants.DEL_FLAG_DELETE;
		vo.setDelFlag(delFlag);
		commentService.update(vo);
		
		addMessage(redirectAttributes, "操作成功");
		return "redirect:" + adminPath + "/cms/comment/?repage&delFlag="+delFlag;
		
	}
	
	
	@RequiresPermissions("cms:comment:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, 
			@RequestParam(required=false) Boolean isRe, RedirectAttributes redirectAttributes) {
//		commentService.delete(commentVo, isRe);
//		commentService.delete(commentVo);
		
		CommentVo delVo = new CommentVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		
		String delFlag = (isRe!=null && isRe.booleanValue())? Constants.DEL_FLAG_AUDIT:Constants.DEL_FLAG_DELETE;
		delVo.setDelFlag(delFlag);
		delVo.setAuditUser(SecurityHelper.getCurrentUser());
		commentService.update(delVo);
		
//		addMessage(redirectAttributes, (isRe!=null&&isRe?"恢复审核":"删除")+"评论成功");
		addMessage(redirectAttributes, "操作成功");
		return "redirect:" + adminPath + "/cms/comment/?repage&delFlag="+delFlag;
	}

}
