package com.jad.web.cms.util;

import org.apache.commons.lang3.StringUtils;

import com.jad.core.cms.utils.SiteUtil;

public class SiteHelper {

	/**
	 * 获取当前编辑的站点编号
	 */
	public static String getCurrentSiteId(){
//		String siteId = (String)UserUtils.getCache("siteId");
//		SiteService siteService=SpringContextHolder.getBean("siteService");
		String siteId = SiteUtil.getCurrentSiteId();
		
		return StringUtils.isNotBlank(siteId)?siteId:SiteUtil.getDefaultSiteId();
	}
	
	/**
	 * 获取默认站点ID
	 */
	public static String defaultSiteId(){
		return SiteUtil.getDefaultSiteId();
	}
	
	/**
	 * 判断是否为默认（主站）站点
	 */
	public static boolean isDefault(String id){
		return id != null && id.equals(defaultSiteId());
	}

}
