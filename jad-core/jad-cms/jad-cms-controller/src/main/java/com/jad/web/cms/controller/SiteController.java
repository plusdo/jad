/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.commons.web.utils.CookieUtils;
import com.jad.core.cms.qo.SiteQo;
import com.jad.core.cms.service.SiteService;
import com.jad.core.cms.vo.SiteVo;
import com.jad.web.cms.util.SiteHelper;
import com.jad.web.mvc.BaseController;

/**
 * 站点Controller
 * @author ThinkGem
 * @version 2013-3-23
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/site")
@Api(description = "站点管理")
public class SiteController extends BaseController {

	@Autowired
	private SiteService siteService;
	
//	@ModelAttribute
//	public SiteVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return siteService.findById(id);
//		}else{
//			return new SiteVo();
//		}
//	}
	
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:site:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看站点信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<SiteVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(siteService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:site:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询站点列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<SiteVo>> findPage(SiteQo siteQo,PageQo pageQo){
		Page<SiteVo> page = siteService.findPage(pageQo, siteQo);
		return Result.newSuccResult(page);
	}
	
	/**
	 * 修改
	 * @param siteVo
	 * @return
	 */
	@RequiresPermissions("sys:site:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改站点信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(SiteVo siteVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, siteVo)){
			return result;
		}

		if(StringUtils.isBlank(siteVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(siteVo,SecurityHelper.getCurrentUserId());
		siteService.update(siteVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:site:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增站点信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(SiteVo siteVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		
		if (!beanValidator(result, siteVo)){
			return result;
		}
		preInsert(siteVo,SecurityHelper.getCurrentUserId());
		siteService.add(siteVo);
		return result;
	}
	
	/**
	 * 删除或退回审核
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:site:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除站点信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id,@RequestParam(required=false) Boolean isRe){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		SiteVo delVo = new SiteVo();
		delVo.setId(id);
		String delFlag = (isRe!=null && isRe.booleanValue())? Constants.DEL_FLAG_AUDIT:Constants.DEL_FLAG_DELETE;
		delVo.setDelFlag(delFlag);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		siteService.delete(delVo);
		return result;
	}
	
	@RequiresPermissions("cms:site:view")
	@RequestMapping(value = {"list", ""})
	public String list(SiteQo siteQo, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<SiteVo> page = siteService.findPage(getPage(request, response), siteQo); 
        model.addAttribute("page", page);
		return "modules/cms/siteList";
	}

	@RequiresPermissions("cms:site:view")
	@RequestMapping(value = "form")
	public String form(SiteQo siteQo, Model model) {
		SiteVo siteVo =new SiteVo();
		if(StringUtils.isNotBlank(siteQo.getId())){
			siteVo = siteService.findById(siteQo.getId());
		}
		return toForm(siteVo,model);
	}
	
	private String toForm(SiteVo siteVo,Model model){
		model.addAttribute("siteVo", siteVo);
		return "modules/cms/siteForm";
	}

	@RequiresPermissions("cms:site:edit")
	@RequestMapping(value = "save")
	public String save(SiteVo siteVo, Model model, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/cms/site/?repage";
		}
		if (!beanValidator(model, siteVo)){
			return toForm(siteVo, model);
		}
		if(StringUtils.isNotBlank(siteVo.getId())){
			preUpdate(siteVo, SecurityHelper.getCurrentUserId());
			siteService.update(siteVo);
		}else{
			preInsert(siteVo, SecurityHelper.getCurrentUserId());
			siteService.add(siteVo);
		}
		addMessage(redirectAttributes, "保存站点'" + siteVo.getName() + "'成功");
		return "redirect:" + adminPath + "/cms/site/?repage";
	}
	
	/**
	 * 删除或恢复
	 * @param id
	 * @param isRe
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("cms:site:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id,  @RequestParam(required=false) Boolean isRe, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/cms/site/?repage";
		}
		if (SiteHelper.isDefault(id)){
			addMessage(redirectAttributes, "删除站点失败, 不允许删除默认站点");
		}else{
//			siteService.delete(siteVo, isRe);
			SiteVo delVo = new SiteVo();
			delVo.setId(id);
			preUpdate(delVo,SecurityHelper.getCurrentUserId());
			
			//恢复站点
			if(isRe!=null && isRe.booleanValue()){
				delVo.setDelFlag(Constants.DEL_FLAG_NORMAL);
				siteService.update(delVo);
			}else{
				siteService.delete(delVo);
			}
			
			addMessage(redirectAttributes, (isRe!=null&&isRe?"恢复":"")+"删除站点成功");
		}
		return "redirect:" + adminPath + "/cms/site/?repage";
	}
	
	/**
	 * 选择站点
	 * @param id
	 * @return
	 */
	@RequiresPermissions("cms:site:select")
	@RequestMapping(value = "select")
	public String select(@RequestParam(required=false)String id, boolean flag, HttpServletResponse response){
		if (id!=null){
//			UserUtils.putCache("siteId", id);//20161213
			// 保存到Cookie中，下次登录后自动切换到该站点
			CookieUtils.setCookie(response, "siteId", id);
		}
		if (flag){
			return "redirect:" + adminPath;
		}
		return "modules/cms/siteSelect";
	}
}
