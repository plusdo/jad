package com.jad.core.cms.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.jad.commons.context.Constants;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.utils.Validate;
import com.jad.core.cms.dao.ArticleDao;
import com.jad.core.cms.entity.ArticleEo;
import com.jad.core.cms.qo.CategoryQo;
import com.jad.core.cms.vo.CategoryVo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("articleDao")
public class ArticleDaoImpl extends AbstractJadEntityDao<ArticleEo,String>implements ArticleDao{

//	@Override
//	public List<ArticleEo> findByIdIn(String[] ids) {
//		
//		Assert.notEmpty(ids);
//		
//		List<Object>params=new ArrayList();
//		
//		StringBuffer sb=new StringBuffer();
//		sb.append(SqlHelper.getSelectSql(ArticleEo.class, "a", params));
//		
//		sb.append(" where id in ").append(SqlHelper.getValsInSql(Arrays.asList(ids), params));
//		
//		
//		return super.findBySql(sb.toString(), params, ArticleEo.class);
//	}

	@Override
	public int updateHitsAddOne(String id) {
		
		Assert.hasText(id);
		
		String sql="update cms_article set hits = hits+1 WHERE id = ? ";
		List<Object>params=new ArrayList();
		params.add(id);
		return super.executeSql(sql, params);
		
	}

	@Override
	public int updateExpiredWeight() {
		
		String sql="update cms_article SET weight = 0 WHERE weight > 0 "
				+ "AND weight_date < CURDATE()  ";
		
		List<Object>params=new ArrayList();
		
		return super.executeSql(sql, params);
		
	}

	@Override
	public List<CategoryVo> findStats(CategoryQo category) {
		
		List<Object>params=new ArrayList();
		
		Validate.notBlank(category.getSiteId());
		
		StringBuffer sb=new StringBuffer();
		
		sb.append(" select max(c.id) AS \"id\", ");
		sb.append(" max(c.name) AS \"name\", ");
		sb.append(" max(cp.id) AS \"parent.id\", ");
		sb.append("  max(cp.name) AS \"parent.name\", ");
		sb.append(" count(*) AS \"cnt\", ");
		sb.append(" sum(a.hits) AS \"hits\", ");
		sb.append(" max(a.update_date) as \"updateDate\", ");
		sb.append(" max(o.id) AS \"office.id\", ");
		sb.append(" max(o.name) AS \"office.name\" ");
		sb.append(" FROM cms_article a ");
		sb.append(" RIGHT JOIN cms_category c ON c.id = a.category_id ");
		sb.append(" JOIN cms_category cp ON c.parent_id = cp.id ");
		sb.append(" JOIN sys_office o ON o.id = c.office_id ");
		
		sb.append(" where a.del_flag = ? and c.site_id = ? ");
		params.add(Constants.DEL_FLAG_NORMAL);
		params.add(category.getSiteId());
		
//		if(category.getOffice()!=null && StringUtils.isNotBlank(category.getOffice().getId())){
//			sb.append(" AND (c.office_id = ? OR o.parent_ids like CONCAT('%', ?, '%') ");
//			params.add(category.getOffice().getId());
//			params.add(category.getOffice().getId());
//		}
		if(category.getUpdateDateBegin()!=null){
			sb.append(" AND a.update_date >=  str_to_date(?,'%Y-%m-%d %H:%i:%s') ");
			params.add(category.getUpdateDateBegin());
		}
		if(category.getUpdateDateEnd()!=null){
			sb.append(" AND a.update_date <=  str_to_date(?,'%Y-%m-%d %H:%i:%s') ");
			params.add(category.getUpdateDateEnd());
		}
		if(StringUtils.isNotBlank(category.getId())){
			sb.append(" AND (c.id = ? OR c.parent_ids LIKE CONCAT('%', ?, '%') ");
			params.add(category.getId());
			params.add(category.getId());
		}
		sb.append(" group by cp.sort, cp.id, c.sort, c.id");
		sb.append(" order by cp.sort, cp.id, c.sort, c.id");
		
		List<CategoryVo>voList=super.findBySql(sb.toString(), params, CategoryVo.class);
		
		
		return voList;
	}


}
