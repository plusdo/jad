/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.BaseEo;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;

/**
 * 文章Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
@Entity
@Table(name = "cms_article")
public class ArticleEo extends BaseEo<String> {

    public static final String DEFAULT_TEMPLATE = "frontViewArticle";
	
	private static final long serialVersionUID = 1L;
	
	private CategoryEo category;// 分类编号
	
	private String title;	// 标题
    private String link;	// 外部链接
	private String color;	// 标题颜色（red：红色；green：绿色；blue：蓝色；yellow：黄色；orange：橙色）
	private String image;	// 文章图片
	private String keywords;// 关键字
	private String description;// 描述、摘要
	private Integer weight;	// 权重，越大越靠前
	private Date weightDate;// 权重期限，超过期限，将weight设置为0
	private Integer hits;	// 点击数
	private String posid;	// 推荐位，多选（1：首页焦点图；2：栏目页文章推荐；）
    private String customContentView;	// 自定义内容视图
   	private String viewConfig;	// 视图参数
   	
	private ArticleDataEo articleData;	//文章副表
    
	public ArticleEo() {
		super();

	}

	public ArticleEo(String id){
		this();
		this.id = id;
	}
	
	
	@ManyToOne
	@RelateColumn(name="category_id",loadMethod = RelateLoadMethod.LEFT_JOIN)
	public CategoryEo getCategory() {
		return category;
	}

	public void setCategory(CategoryEo category) {
		this.category = category;
	}

	@Column(name="title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="link")
    @Length(min=0, max=255)
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Column(name="color")
	@Length(min=0, max=50)
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Column(name="image")
	@Length(min=0, max=255)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
        this.image = image;//CmsUtils.formatImageSrcToDb(image);
	}

	@Column(name="keywords")
	@Length(min=0, max=255)
	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	@Column(name="description")
	@Length(min=0, max=255)
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="weight")
	@NotNull
	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	@Column(name="weight_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getWeightDate() {
		return weightDate;
	}

	public void setWeightDate(Date weightDate) {
		this.weightDate = weightDate;
	}

	@Column(name="hits")
	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}

	@Column(name="posid")
	@Length(min=0, max=10)
	public String getPosid() {
		return posid;
	}

	public void setPosid(String posid) {
		this.posid = posid;
	}

	@Column(name="custom_content_view")
    public String getCustomContentView() {
        return customContentView;
    }

    public void setCustomContentView(String customContentView) {
        this.customContentView = customContentView;
    }
    
    @Column(name="view_config")
    public String getViewConfig() {
        return viewConfig;
    }

    public void setViewConfig(String viewConfig) {
        this.viewConfig = viewConfig;
    }

    @ManyToOne
	@RelateColumn(name="id")
    public ArticleDataEo getArticleData() {
		return articleData;
	}

	public void setArticleData(ArticleDataEo articleData) {
		this.articleData = articleData;
	}

//	public List<String> getPosidList() {
//		List<String> list = Lists.newArrayList();
//		if (posid != null){
//			for (String s : StringUtils.split(posid, ",")) {
//				list.add(s);
//			}
//		}
//		return list;
//	}
//
//
//	public void setPosidList(List<String> list) {
//		posid = ","+StringUtils.join(list, ",")+",";
//	}
//
//   	public String getUrl(String contextPath,String frontPath,String urlSuffix) {
//        return getUrlDynamic(this,contextPath,frontPath,urlSuffix);
//   	}
//
//   	public String getImageSrc(String contextPath) {
//        return formatImageSrcToWeb(this.image,contextPath);
//   	}
//   	
//    /**
//     * 从图片地址中加入ContextPath地址
//   	 * @param src
//   	 * @return src
//   	 */
//    public static String formatImageSrcToWeb(String src,String contextPath) {
//        if(StringUtils.isBlank(src)) return src;
//        if(src.startsWith(contextPath + "/userfiles")){
//            return src;
//        }else{
//            return contextPath+src;
//        }
//    }
//	
//   	
//    /**
//     * 获得文章动态URL地址
//   	 * @param article
//   	 * @return url
//   	 */
//    public static String getUrlDynamic(ArticleEo article,String contextPath,String frontPath,String urlSuffix) {
//        if(StringUtils.isNotBlank(article.getLink())){
//            return article.getLink();
//        }
//        StringBuilder str = new StringBuilder();
////        str.append(context.getContextPath()).append(Global.getFrontPath());
////        str.append("/view-").append(article.getCategory().getId()).append("-").append(article.getId()).append(Global.getUrlSuffix());
//        str.append(contextPath).append(frontPath);
//        str.append("/view-").append(article.getCategory().getId()).append("-").append(article.getId()).append(urlSuffix);
//        return str.toString();
//    }
    
    
}


