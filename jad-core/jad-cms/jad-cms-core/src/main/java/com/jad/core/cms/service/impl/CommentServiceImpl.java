/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.core.cms.entity.CommentEo;
import com.jad.core.cms.service.CommentService;
import com.jad.core.cms.vo.CommentVo;

/**
 * 评论Service
 * @author ThinkGem
 * @version 2013-01-15
 */
@Service("commentService")
@Transactional(readOnly = true)
public class CommentServiceImpl extends AbstractServiceImpl<CommentEo, String,CommentVo> implements CommentService{

//	public Page<CommentVo> findPage(Page<CommentVo> page, CommentQo comment) {
////		DetachedCriteria dc = commentDao.createDetachedCriteria();
////		if (StringUtils.isNotBlank(comment.getContentId())){
////			dc.add(Restrictions.eq("contentId", comment.getContentId()));
////		}
////		if (StringUtils.isNotEmpty(comment.getTitle())){
////			dc.add(Restrictions.like("title", "%"+comment.getTitle()+"%"));
////		}
////		dc.add(Restrictions.eq(Comment.FIELD_DEL_FLAG, comment.getDelFlag()));
////		dc.addOrder(Order.desc("id"));
////		return commentDao.find(page, dc);
////		UserVo user=UserUtils.getUser();
////		comment.getSqlMap().put("dsf", DataFilters.dataScopeFilter(comment.getCurrentUser(), "o", "u"));
////		comment.getSqlMap().put("dsf", DataFilters.dataScopeFilter(user, "o", "u"));
//		
//		return super.findPage(page, comment);
//	}
	
//	@Transactional(readOnly = false)
//	public void delete(CommentVo entity, Boolean isRe) {
//		super.delete(entity);
//	}
}
