package com.jad.core.cms.dao.impl;

import com.jad.core.cms.dao.GuestbookDao;
import com.jad.core.cms.entity.GuestbookEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("guestbookDao")
public class GuestbookDaoImpl extends AbstractJadEntityDao<GuestbookEo,String>implements GuestbookDao{


}
