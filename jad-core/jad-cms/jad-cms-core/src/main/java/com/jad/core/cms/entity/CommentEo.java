/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.BaseEo;
import com.jad.core.sys.entity.UserEo;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;

/**
 * 评论Entity
 * @author ThinkGem
 * @version 2013-05-15
 */

@Entity
@Table(name = "cms_comment")
public class CommentEo extends BaseEo<String> {

	private static final long serialVersionUID = 1L;
	private CategoryEo category;// 分类编号
	private String contentId;	// 归属分类内容的编号（Article.id、Photo.id、Download.id）
	private String title;	// 归属分类内容的标题（Article.title、Photo.title、Download.title）
	private String content; // 评论内容
	private String name; 	// 评论姓名
	private String ip; 		// 评论IP
	private UserEo auditUser; // 审核人
	private Date auditDate;	// 审核时间

	public CommentEo() {
		super();
	}
	
	public CommentEo(String id){
		this();
		this.id = id;
	}
	
//	public CommentEo(CategoryEo category){
//		this();
//		this.category = category;
//	}
	


	@ManyToOne
	@RelateColumn(name="category_id")
	@NotNull
	public CategoryEo getCategory() {
		return category;
	}

	public void setCategory(CategoryEo category) {
		this.category = category;
	}

	@Column(name="content_id")
	@NotNull
	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	@Column(name="content")
	@Length(min=1, max=255)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Column(name="title")
	@Length(min=1, max=255)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name="name")
	@Length(min=1, max=100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne
	@RelateColumn(name="audit_user_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	public UserEo getAuditUser() {
		return auditUser;
	}

	public void setAuditUser(UserEo auditUser) {
		this.auditUser = auditUser;
	}

	@Column(name="audit_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	@Column(name="ip")
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}


}