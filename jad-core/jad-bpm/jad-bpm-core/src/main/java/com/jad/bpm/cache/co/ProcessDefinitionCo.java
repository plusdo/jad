package com.jad.bpm.cache.co;

import com.jad.bpm.dto.ActProcessDefinition;

public class ProcessDefinitionCo {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 流程id
	 */
	private String procDefId;
	
	/**
	 * 流程定义
	 */
	private ActProcessDefinition processDefinition;

	public String getProcDefId() {
		return procDefId;
	}

	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}

	public ActProcessDefinition getProcessDefinition() {
		return processDefinition;
	}

	public void setProcessDefinition(ActProcessDefinition processDefinition) {
		this.processDefinition = processDefinition;
	}
	
}
