/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.service.audit;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.bpm.dao.audit.TestAuditDao;
import com.jad.bpm.entity.TestAuditEo;
import com.jad.bpm.service.ActTaskManager;
import com.jad.bpm.utils.ActUtils;
import com.jad.bpm.vo.TestAuditVo;
import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.commons.vo.Page;
import com.jad.dao.utils.EntityUtils;

/**
 * 审批Service
 * @author thinkgem
 * @version 2014-05-16
 */
@Service
@Transactional(readOnly = true)
public class TestAuditServiceImpl extends AbstractServiceImpl<TestAuditEo, String,TestAuditVo> 
	implements TestAuditService {

	@Autowired
	private ActTaskManager actTaskManager;
	@Autowired
	private TestAuditDao testAuditDao;
	
	public TestAuditVo getByProcInsId(String procInsId) {
		return EntityUtils.copyToVo(testAuditDao.getByProcInsId(procInsId), TestAuditVo.class);
	}
	
	public Page<TestAuditVo> findPage(Page<TestAuditVo> page, TestAuditVo testAudit) {
//		testAudit.setPage(page);
//		page.setList(testAuditDao.findList(testAudit));
//		return page;
		
//		return EntityUtils.copyToVoPage(testAuditDao.findPageByVo(page, testAudit), TestAuditVo.class);
		
		return page;
	}
	
	/**
	 * 审核新增或编辑
	 * @param testAudit
	 */
	@Transactional(readOnly = false)
	public void save(TestAuditVo testAudit) {
		
		// 申请发起
		if (StringUtils.isBlank(testAudit.getId().toString())){
//			testAudit.preInsert();
//			testAuditDao.insert(testAudit);
			super.preInsert(testAudit,null);
			testAuditDao.add(EntityUtils.copyToEo(testAudit, TestAuditEo.class));
			// 启动流程
			actTaskManager.startProcess(ActUtils.PD_TEST_AUDIT[0], ActUtils.PD_TEST_AUDIT[1], testAudit.getId().toString(), testAudit.getContent());
			
		}
		
		// 重新编辑申请		
		else{
//			testAudit.preUpdate();
//			testAuditDao.update(testAudit);
			super.preUpdate(testAudit);
			testAuditDao.updateById(EntityUtils.copyToEo(testAudit, TestAuditEo.class), testAudit.getId().toString());

			testAudit.getAct().setComment(("yes".equals(testAudit.getAct().getFlag())?"[重申] ":"[销毁] ")+testAudit.getAct().getComment());
			
			// 完成流程任务
			Map<String, Object> vars =  new HashMap<String, Object>();
			vars.put("pass", "yes".equals(testAudit.getAct().getFlag())? "1" : "0");
			actTaskManager.complete(testAudit.getAct().getTaskId(), testAudit.getAct().getProcInsId(), testAudit.getAct().getComment(), testAudit.getContent(), vars);
		}
	}

	/**
	 * 审核审批保存
	 * @param testAudit
	 */
	@Transactional(readOnly = false)
	public void auditSave(TestAuditVo testAudit) {
		
		// 设置意见
		testAudit.getAct().setComment(("yes".equals(testAudit.getAct().getFlag())?"[同意] ":"[驳回] ")+testAudit.getAct().getComment());
		
//		testAudit.preUpdate();
		super.preUpdate(testAudit);
		// 对不同环节的业务逻辑进行操作
		String taskDefKey = testAudit.getAct().getTaskDefKey();

		// 审核环节
		if ("audit".equals(taskDefKey)){
			
		}
		else if ("audit2".equals(taskDefKey)){
			testAudit.setHrText(testAudit.getAct().getComment());
			testAuditDao.updateHrText(testAudit);
		}
		else if ("audit3".equals(taskDefKey)){
			testAudit.setLeadText(testAudit.getAct().getComment());
			testAuditDao.updateLeadText(testAudit);
		}
		else if ("audit4".equals(taskDefKey)){
			testAudit.setMainLeadText(testAudit.getAct().getComment());
			testAuditDao.updateMainLeadText(testAudit);
		}
		else if ("apply_end".equals(taskDefKey)){
			
		}
		
		// 未知环节，直接返回
		else{
			return;
		}
		
		// 提交流程任务
		Map<String, Object> vars =  new HashMap<String, Object>();
		vars.put("pass", "yes".equals(testAudit.getAct().getFlag())? "1" : "0");
		actTaskManager.complete(testAudit.getAct().getTaskId(), testAudit.getAct().getProcInsId(), testAudit.getAct().getComment(), vars);
		
	}


	
}
