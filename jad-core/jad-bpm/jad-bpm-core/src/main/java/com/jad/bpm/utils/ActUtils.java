/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.DelegationState;
import org.activiti.engine.task.Task;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.bpm.annotation.FieldName;
import com.jad.bpm.dto.ActActivity;
import com.jad.bpm.dto.ActDeployment;
import com.jad.bpm.dto.ActGroup;
import com.jad.bpm.dto.ActHistoricActivityInstance;
import com.jad.bpm.dto.ActHistoricProcessInstance;
import com.jad.bpm.dto.ActHistoricTaskInstance;
import com.jad.bpm.dto.ActModel;
import com.jad.bpm.dto.ActProcessDefinition;
import com.jad.bpm.dto.ActProcessInstance;
import com.jad.bpm.dto.ActTask;
import com.jad.bpm.dto.ActVo;
import com.jad.commons.context.Global;
import com.jad.commons.encrypt.Encodes;
import com.jad.commons.utils.ObjectUtils;
import com.jad.commons.utils.StringUtils;
import com.jad.core.sys.vo.RoleVo;
import com.jad.core.sys.vo.UserVo;

/**
 * 流程工具
 * @author ThinkGem
 * @version 2013-11-03
 */
public class ActUtils {

//	private static Logger logger = LoggerFactory.getLogger(ActUtils.class);
	
	/**
	 * 定义流程定义KEY，必须以“PD_”开头
	 * 组成结构：string[]{"流程标识","业务主表表名"}
	 */
	public static final String[] PD_LEAVE = new String[]{"leave", "oa_leave"};
	public static final String[] PD_TEST_AUDIT = new String[]{"test_audit", "oa_test_audit"};
	
//	/**
//	 * 流程定义Map（自动初始化）
//	 */
//	private static Map<String, String> procDefMap = new HashMap<String, String>() {
//		private static final long serialVersionUID = 1L;
//		{
//			for (Field field : ActUtils.class.getFields()){
//				if(StringUtils.startsWith(field.getName(), "PD_")){
//					try{
//						String[] ss = (String[])field.get(null);
//						put(ss[0], ss[1]);
//					}catch (Exception e) {
//						logger.debug("load pd error: {}", field.getName());
//					}
//				}
//			}
//		}
//	};
//	
//	/**
//	 * 获取流程执行（办理）URL
//	 * @param procId
//	 * @return
//	 */
//	public static String getProcExeUrl(String procId) {
//		String url = procDefMap.get(StringUtils.split(procId, ":")[0]);
//		if (StringUtils.isBlank(url)){
//			return "404";
//		}
//		return url;
//	}
	
	@SuppressWarnings({ "unused" })
	public static Map<String, Object> getMobileEntity(Object entity,String spiltType){
		if(spiltType==null){
			spiltType="@";
		}
		Map<String, Object> map = new HashMap();

		List<String> field = new ArrayList();
		List<String> value = new ArrayList();
		List<String> chinesName =new ArrayList();
		
		try{
			for (Method m : entity.getClass().getMethods()){
				if (m.getAnnotation(JsonIgnore.class) == null && m.getAnnotation(JsonBackReference.class) == null && m.getName().startsWith("get")){
					if (m.isAnnotationPresent(FieldName.class)) {
						Annotation p = m.getAnnotation(FieldName.class);
						FieldName fieldName=(FieldName) p;
						chinesName.add(fieldName.value());
					}else{
						chinesName.add("");
					}
					if (m.getName().equals("getAct")){
						Object act = m.invoke(entity, new Object[]{});
						Method actMet = act.getClass().getMethod("getTaskId");
						map.put("taskId", ObjectUtils.toString(m.invoke(act, new Object[]{}), ""));
					}else{
						field.add(StringUtils.uncapitalize(m.getName().substring(3)));
						value.add(ObjectUtils.toString(m.invoke(entity, new Object[]{}), ""));
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		map.put("beanTitles", StringUtils.join(field, spiltType));
		map.put("beanInfos", StringUtils.join(value, spiltType));
		map.put("chineseNames", StringUtils.join(chinesName, spiltType));
		
		return map;
	}
	
	/**
	 * 获取流程表单URL
	 * @param formKey
	 * @param act 表单传递参数
	 * @return
	 */
	public static String getFormUrl(String formKey, ActVo act){
		
		StringBuilder formUrl = new StringBuilder();
		
		String formServerUrl = Global.getConfig("activiti.form.server.url");
		if (StringUtils.isBlank(formServerUrl)){
			formUrl.append(Global.getConfig("adminPath"));
		}else{
			formUrl.append(formServerUrl);
		}
		
		formUrl.append(formKey).append(formUrl.indexOf("?") == -1 ? "?" : "&");
		formUrl.append("act.taskId=").append(act.getTaskId() != null ? act.getTaskId() : "");
		formUrl.append("&act.taskName=").append(act.getTaskName() != null ? Encodes.urlEncode(act.getTaskName()) : "");
		formUrl.append("&act.taskDefKey=").append(act.getTaskDefKey() != null ? act.getTaskDefKey() : "");
		formUrl.append("&act.procInsId=").append(act.getProcInsId() != null ? act.getProcInsId() : "");
		formUrl.append("&act.procDefId=").append(act.getProcDefId() != null ? act.getProcDefId() : "");
		formUrl.append("&act.status=").append(act.getStatus() != null ? act.getStatus() : "");
		formUrl.append("&id=").append(act.getBusinessId() != null ? act.getBusinessId() : "");
		
		return formUrl.toString();
	}
	
	/**
	 * 转换流程节点类型为中文说明
	 * @param type 英文名称
	 * @return 翻译后的中文名称
	 */
	public static String parseToZhType(String type) {
		Map<String, String> types = new HashMap<String, String>();
		types.put("userTask", "用户任务");
		types.put("serviceTask", "系统任务");
		types.put("startEvent", "开始节点");
		types.put("endEvent", "结束节点");
		types.put("exclusiveGateway", "条件判断节点(系统自动根据条件处理)");
		types.put("inclusiveGateway", "并行处理任务");
		types.put("callActivity", "子流程");
		return types.get(type) == null ? type : types.get(type);
	}
	
	public static ActGroup toActGroup(Group group){
		if(group==null)return null;
		ActGroup g=new ActGroup();
		g.setId(group.getId());
		g.setName(group.getName());
		g.setType(group.getType());
		return g;
	}

	
	public static ActTask toActTask(Task task){
		if(task==null)return null;
		ActTask at=new ActTask();
		at.setId(task.getId());
		at.setName(task.getName());
		at.setDescription(task.getDescription());
		at.setPriority(task.getPriority());
		at.setOwner(task.getOwner());
		at.setAssignee(task.getAssignee());
		at.setDelegationState(DelegationState.PENDING.equals(task.getDelegationState())?ActTask.PENDING:ActTask.RESOLVED);
		at.setProcessInstanceId(task.getProcessInstanceId());
		at.setExecutionId(task.getExecutionId());
		at.setProcessDefinitionId(task.getProcessDefinitionId());
		at.setCreateTime(task.getCreateTime());
		at.setTaskDefinitionKey(task.getTaskDefinitionKey());
		at.setDueDate(task.getDueDate());
		at.setCategory(task.getCategory());
		at.setParentTaskId(task.getParentTaskId());
		at.setSuspended(task.isSuspended());
		at.setTenantId(task.getTenantId());
		at.setTaskLocalVariables(task.getTaskLocalVariables());
		at.setProcessVariables(task.getProcessVariables());
		return at;
		
	}
	
	public static ActHistoricProcessInstance toActHistoricProcessInstance(HistoricProcessInstance his){
		
		if(his==null)return null;
		
		ActHistoricProcessInstance ins=new ActHistoricProcessInstance();
		
		ins.setId(his.getId());

		ins.setBusinessKey(his.getBusinessKey());
		
		ins.setProcessDefinitionId(his.getProcessDefinitionId());
		

		ins.setStartTime(his.getStartTime());

		ins.setEndTime(his.getEndTime());

		ins.setDurationInMillis(his.getDurationInMillis());

		ins.setEndActivityId(his.getEndActivityId());
		
		ins.setStartUserId(his.getStartUserId());
		

		ins.setStartActivityId(his.getStartActivityId());
		
		ins.setDeleteReason(his.getDeleteReason());

		ins.setSuperProcessInstanceId(his.getSuperProcessInstanceId());

		ins.setTenantId(his.getTenantId());

		ins.setProcessVariables(his.getProcessVariables());
		
		return ins;
	}
	
	public static ActDeployment toActDeployment(Deployment deploy){
		if(deploy==null)return null;
		ActDeployment actDeploy=new ActDeployment();
		
		actDeploy.setId(deploy.getId());
		actDeploy.setName(deploy.getName());
		actDeploy.setDeploymentTime(deploy.getDeploymentTime());
		actDeploy.setCategory(deploy.getCategory());
		actDeploy.setTenantId(deploy.getTenantId());
		return actDeploy;
		
	}
	
	public static ActProcessDefinition toActProcessDefinition(ProcessDefinition pd){
		if(pd==null)return null;
		ActProcessDefinition apd=new ActProcessDefinition();
		apd.setId(pd.getId());
		apd.setCategory(pd.getCategory());  
		apd.setName(pd.getName());
		apd.setKey(pd.getKey());
		apd.setDescription(pd.getDescription());  
		apd.setVersion(pd.getVersion());
		apd.setResourceName(pd.getResourceName());
		apd.setDeploymentId(pd.getDeploymentId());
		apd.setDiagramResourceName(pd.getDiagramResourceName());
		apd.setHasStartFormKey(pd.hasStartFormKey());
		apd.setSuspended(pd.isSuspended());
		apd.setTenantId(pd.getTenantId());
		
		if(pd instanceof ProcessDefinitionEntity){
			List<ActActivity>aList=new ArrayList<ActActivity>();
			List<ActivityImpl>actList=((ProcessDefinitionEntity) pd).getActivities();
			if(actList!=null && !actList.isEmpty()){
				for(ActivityImpl ai:actList){
					aList.add(toActActivity(ai));
				}
			}
			apd.setActivityList(aList);
		}
		
		return apd;
	}
	
	
	
	public static ActActivity toActActivity(ActivityImpl ai){
		if(ai==null)return null;
		ActActivity a=new ActActivity();
		
		a.setId(ai.getId());
		a.setProperties(ai.getProperties());
		
		return a;
		
	}
	
	public static ActProcessInstance toActProcessInstance(ProcessInstance instance){
		
		if(instance==null)return null;
		
		ActProcessInstance actInstance=new ActProcessInstance();
		
		actInstance.setId(instance.getId());

		actInstance.setSuspended(instance.isSuspended());
		actInstance.setEnded(instance.isEnded());
		actInstance.setActivityId(instance.getActivityId());

		actInstance.setProcessInstanceId(instance.getProcessInstanceId());
		actInstance.setParentId(instance.getParentId());

		actInstance.setTenantId(instance.getTenantId());

		actInstance.setProcessDefinitionId(instance.getProcessDefinitionId());

		actInstance.setBusinessKey(instance.getBusinessKey());

		actInstance.setProcessVariables(instance.getProcessVariables());
		
		return actInstance;
	}
	
	public static ActModel toActModel(Model mod){
		if(mod==null)return null;
		ActModel m=new ActModel();
		
		m.setId(mod.getId());
		m.setName(mod.getName());
		m.setKey(mod.getKey());
		m.setCategory(mod.getCategory());
		m.setCreateTime(mod.getCreateTime());
		m.setLastUpdateTime(mod.getLastUpdateTime());
		m.setVersion(mod.getVersion());
		m.setMetaInfo(mod.getMetaInfo());
		m.setDeploymentId(mod.getDeploymentId());
		m.setTenantId(mod.getTenantId());
		
		return m;
		
	}
	
	public static ActHistoricActivityInstance toActHistoricActivityInstance(HistoricActivityInstance his){
		if(his==null)return null;
		ActHistoricActivityInstance act=new ActHistoricActivityInstance();
		act.setId(his.getId());

		act.setActivityId(his.getActivityId());

		act.setActivityName(his.getActivityName());

		act.setActivityType(his.getActivityType());

		act.setProcessDefinitionId(his.getProcessDefinitionId());

		act.setProcessInstanceId(his.getProcessInstanceId());

		act.setExecutionId(his.getExecutionId());

		act.setTaskId(his.getTaskId());

		act.setCalledProcessInstanceId(his.getCalledProcessInstanceId());

		act.setAssignee(his.getAssignee());

		act.setStartTime(his.getStartTime());

		act.setEndTime(his.getEndTime());

		act.setDurationInMillis(his.getDurationInMillis());

		act.setTenantId(his.getTenantId());
		
		return act;
	}
	
	public static ActHistoricTaskInstance toActHistoricTaskInstance(HistoricTaskInstance his){
		if(his==null)return null;
		
		ActHistoricTaskInstance act=new ActHistoricTaskInstance();
		act.setId(his.getId());
		act.setProcessDefinitionId(his.getProcessDefinitionId());
		act.setProcessInstanceId(his.getProcessInstanceId());

		act.setExecutionId(his.getExecutionId());

		act.setName(his.getName());

		act.setDescription(his.getDescription());

		act.setDeleteReason(his.getDeleteReason());

		act.setOwner(his.getOwner());

		act.setAssignee(his.getAssignee());

		act.setStartTime(his.getStartTime());

		act.setEndTime(his.getEndTime());

		act.setDurationInMillis(his.getDurationInMillis());

		act.setWorkTimeInMillis(his.getWorkTimeInMillis());

		act.setClaimTime(his.getClaimTime());

		act.setTaskDefinitionKey(his.getTaskDefinitionKey());

		act.setFormKey(his.getFormKey());

		act.setPriority(his.getPriority());

		act.setDueDate(his.getDueDate());

		act.setCategory(his.getCategory());
		
		act.setParentTaskId(his.getParentTaskId());

		act.setTenantId(his.getTenantId());

		act.setTaskLocalVariables(his.getTaskLocalVariables());

		act.setProcessVariables(his.getProcessVariables());
		
		return act;
	}
	
	
	
	public static UserEntity toActivitiUser(UserVo user){
		if (user == null){
			return null;
		}
		UserEntity userEntity = new UserEntity();
		userEntity.setId(user.getLoginName());
		userEntity.setFirstName(user.getName());
		userEntity.setLastName(StringUtils.EMPTY);
		userEntity.setPassword(user.getPassword());
		userEntity.setEmail(user.getEmail());
		userEntity.setRevision(1);
		return userEntity;
	}
	
	public static GroupEntity toActivitiGroup(RoleVo role){
		if (role == null){
			return null;
		}
		GroupEntity groupEntity = new GroupEntity();
		groupEntity.setId(role.getEnname());
		groupEntity.setName(role.getName());
		groupEntity.setType(role.getRoleType());
		groupEntity.setRevision(1);
		return groupEntity;
	}
	
	public static void main(String[] args) {
		 UserVo user = new UserVo();
		 System.out.println(getMobileEntity(user, "@"));
	}
}
