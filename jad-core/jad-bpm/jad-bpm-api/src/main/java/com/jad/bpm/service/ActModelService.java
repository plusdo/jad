package com.jad.bpm.service;

import java.io.UnsupportedEncodingException;

import com.jad.bpm.dto.ActModel;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;

public interface ActModelService {

	/**
	 * 查询流程模型列表
	 * @param page
	 * @param category
	 * @return
	 */
	public Page<ActModel> queryActModelList(PageQo page, String category);
	
	/**
	 * 创建模型
	 * @param name
	 * @param key
	 * @param description
	 * @param category
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public ActModel createActModel(String name, String key, String description, String category) throws UnsupportedEncodingException ;
	
	
	/**
	 * 根据Model部署流程
	 */
	public String deploy(String id);
	
	/**
	 * 更新Model分类
	 */
	public void updateCategory(String id, String category);
	
	
	/**
	 * 删除模型
	 * @param id
	 * @return
	 */
	public void delete(String id);
	
}
