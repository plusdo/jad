package com.jad.bpm.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jad.commons.utils.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.vo.BaseVo;


/**
 * 
 * 流程对象
 * @author Administrator
 *
 */
public class ActVo extends BaseVo {
	

	
	private static final long serialVersionUID = 1L;
	
//	protected UserVo currentUser;

	private String taskId; 		// 任务编号
	private String taskName; 	// 任务名称
	private String taskDefKey; 	// 任务定义Key（任务环节标识）

	private String procInsId; 	// 流程实例ID
	private String procDefId; 	// 流程定义ID
	private String procDefKey; 	// 流程定义Key（流程定义标识）

	private String businessTable;	// 业务绑定Table
	private String businessId;		// 业务绑定ID
	
	private String title; 		// 任务标题

	private String status; 		// 任务状态（todo/claim/finish）

	private String comment; 	// 任务意见
	private String flag; 		// 意见状态
	
	private ActTask task; 			// 任务对象
	private ActProcessDefinition procDef; 	// 流程定义对象
	private ActProcessInstance procIns;	// 流程实例对象
	private ActHistoricTaskInstance histTask; // 历史任务
	private ActHistoricActivityInstance histIns;	//历史活动任务

	private String assignee; // 任务执行人编号
	private String assigneeName; // 任务执行人名称

	private ActVariable vars; 		// 流程变量
	
	private Date beginDate;	// 开始查询日期
	private Date endDate;	// 结束查询日期

	private List<ActVo> list; // 任务列表

	public ActVo() {
		super();
	}

	public String getTaskId() {
		if (taskId == null && task != null){
			taskId = task.getId();
		}
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		if (taskName == null && task != null){
			taskName = task.getName();
		}
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskDefKey() {
		if (taskDefKey == null && task != null){
			taskDefKey = task.getTaskDefinitionKey();
		}
		return taskDefKey;
	}

	public void setTaskDefKey(String taskDefKey) {
		this.taskDefKey = taskDefKey;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getTaskCreateDate() {
		if (task != null){
			return task.getCreateTime();
		}
		return null;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getTaskEndDate() {
		if (histTask != null){
			return histTask.getEndTime();
		}
		return null;
	}
	
	@JsonIgnore
	public ActTask getTask() {
		return task;
	}

	public void setTask(ActTask task) {
		this.task = task;
	}

	@JsonIgnore
	public ActProcessDefinition getProcDef() {
		return procDef;
	}

	public void setProcDef(ActProcessDefinition procDef) {
		this.procDef = procDef;
	}
	
	public String getProcDefName() {
		return procDef.getName();
	}

	@JsonIgnore
	public ActProcessInstance getProcIns() {
		return procIns;
	}

	public void setProcIns(ActProcessInstance procIns) {
		this.procIns = procIns;
		if (procIns != null && procIns.getBusinessKey() != null){
			String[] ss = procIns.getBusinessKey().split(":");
			setBusinessTable(ss[0]);
			setBusinessId(ss[1]);
		}
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@JsonIgnore
	public ActHistoricTaskInstance getHistTask() {
		return histTask;
	}

	public void setHistTask(ActHistoricTaskInstance histTask) {
		this.histTask = histTask;
	}

	@JsonIgnore
	public ActHistoricActivityInstance getHistIns() {
		return histIns;
	}

	public void setHistIns(ActHistoricActivityInstance histIns) {
		this.histIns = histIns;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getProcDefId() {
		if (procDefId == null && task != null){
			procDefId = task.getProcessDefinitionId();
		}
		return procDefId;
	}

	public void setProcDefId(String procDefId) {
		this.procDefId = procDefId;
	}

	public String getProcInsId() {
		if (procInsId == null && task != null){
			procInsId = task.getProcessInstanceId();
		}
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getBusinessTable() {
		return businessTable;
	}

	public void setBusinessTable(String businessTable) {
		this.businessTable = businessTable;
	}

	public String getAssigneeName() {
		return assigneeName;
	}

	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}

	public String getAssignee() {
		if (assignee == null && task != null){
			assignee = task.getAssignee();
		}
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public List<ActVo> getList() {
		return list;
	}

	public void setList(List<ActVo> list) {
		this.list = list;
	}

	public ActVariable getVars() {
		return vars;
	}

	public void setVars(ActVariable vars) {
		this.vars = vars;
	}
	
	/**
	 * 通过Map设置流程变量值
	 * @param map
	 */
	public void setVars(Map<String, Object> map) {
		this.vars = new ActVariable(map);
	}


	/**
	 * 获取流程定义KEY
	 * @return
	 */
	public String getProcDefKey() {
		if (!StringUtils.isNotBlank(procDefKey) && StringUtils.isNotBlank(procDefId)){
			procDefKey = StringUtils.split(procDefId, ":")[0];
		}
		return procDefKey;
	}

	public void setProcDefKey(String procDefKey) {
		this.procDefKey = procDefKey;
	}
	
	/**
	 * 获取过去的任务历时
	 * @return
	 */
	public String getDurationTime(){
		if (histIns!=null && histIns.getDurationInMillis() != null){
//			return TimeUtils.toTimeString(histIns.getDurationInMillis());
//			TODO
			return histIns.getDurationInMillis()+"";
		}
		return "";
	}
	
	/**
	 * 是否是一个待办任务
	 * @return
	 */
	public boolean isTodoTask(){
		return "todo".equals(status) || "claim".equals(status);
	}
	
	/**
	 * 是否是已完成任务
	 * @return
	 */
	public boolean isFinishTask(){
		return "finish".equals(status) || !StringUtils.isNotBlank(taskId);
	}


//	public UserVo getCurrentUser() {
//		return currentUser;
//	}
//
//	public void setCurrentUser(UserVo currentUser) {
//		this.currentUser = currentUser;
//	}


	
}
