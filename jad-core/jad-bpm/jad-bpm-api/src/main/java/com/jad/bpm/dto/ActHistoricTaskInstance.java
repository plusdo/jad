package com.jad.bpm.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 历史任务实例
 * 
 * @author Administrator
 *
 */
public class ActHistoricTaskInstance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The unique identifier of this historic task instance. This is the same
	 * identifier as the runtime Task instance.
	 */
	private String id;

	/** Process definition reference. */
	private String processDefinitionId;

	/** Process instance reference. */
	private String processInstanceId;

	/** Execution reference. */
	private String executionId;

	/** The latest name given to this task. */
	private String name;

	/** The latest description given to this task. */
	private String description;

	/**
	 * The reason why this task was deleted {'completed' | 'deleted' | any other
	 * user defined string }.
	 */
	private String deleteReason;

	/** Task owner */
	private String owner;

	/** The latest assignee given to this task. */
	private String assignee;

	/** Time when the task started. */
	private Date startTime;

	/** Time when the task was deleted or completed. */
	private Date endTime;

	/**
	 * Difference between {@link #getEndTime()} and {@link #getStartTime()} in
	 * milliseconds.
	 */
	private Long durationInMillis;

	/**
	 * Difference between {@link #getEndTime()} and {@link #getClaimTime()} in
	 * milliseconds.
	 */
	private Long workTimeInMillis;

	/** Time when the task was claimed. */
	private Date claimTime;

	/** Task definition key. */
	private String taskDefinitionKey;

	/** Task form key. */
	private String formKey;

	/** Task priority */
	private int priority;

	/** Task due date */
	private Date dueDate;

	/** Task category */
	private String category;

	/** The parent task of this task, in case this task was a subtask */
	private String parentTaskId;

	/** Returns the tenant identifier for this historic task */
	private String tenantId;

	/** Returns the local task variables if requested in the task query */
	private Map<String, Object> taskLocalVariables;

	/** Returns the process variables if requested in the task query */
	private Map<String, Object> processVariables;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getDurationInMillis() {
		return durationInMillis;
	}

	public void setDurationInMillis(Long durationInMillis) {
		this.durationInMillis = durationInMillis;
	}

	public Long getWorkTimeInMillis() {
		return workTimeInMillis;
	}

	public void setWorkTimeInMillis(Long workTimeInMillis) {
		this.workTimeInMillis = workTimeInMillis;
	}

	public Date getClaimTime() {
		return claimTime;
	}

	public void setClaimTime(Date claimTime) {
		this.claimTime = claimTime;
	}

	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}

	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}

	public String getFormKey() {
		return formKey;
	}

	public void setFormKey(String formKey) {
		this.formKey = formKey;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getParentTaskId() {
		return parentTaskId;
	}

	public void setParentTaskId(String parentTaskId) {
		this.parentTaskId = parentTaskId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Map<String, Object> getTaskLocalVariables() {
		return taskLocalVariables;
	}

	public void setTaskLocalVariables(Map<String, Object> taskLocalVariables) {
		this.taskLocalVariables = taskLocalVariables;
	}

	public Map<String, Object> getProcessVariables() {
		return processVariables;
	}

	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}

}
