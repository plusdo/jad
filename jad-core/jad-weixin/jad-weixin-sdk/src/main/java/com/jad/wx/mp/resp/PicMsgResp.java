package com.jad.wx.mp.resp;

/**
 * 回复图片消息
 * @author hechuan
 *
 */
public class PicMsgResp  extends MsgReplyResp{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String mediaId 	;//是 	通过素材管理接口上传多媒体文件，得到的id。 

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

}
