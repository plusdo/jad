package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.AddAttachmentReq;
import com.jad.wx.mp.req.GetMaterialListReq;
import com.jad.wx.mp.req.UpdateAttachmentReq;
import com.jad.wx.mp.resp.AddAttachmentResp;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.GetAttachmentResp;
import com.jad.wx.mp.resp.GetMaterialCountResp;
import com.jad.wx.mp.resp.GetMaterialListResp;
import com.jad.wx.mp.resp.UploadAttachmentResp;

/**
 * 素材接口
 * @author hechuan
 *
 */
public interface AttachmentService {
	
	/**
	 * 上传临时素材
	 * 
	 * 1、对于临时素材，每个素材（media_id）会在开发者上传或粉丝发送到微信服务器3天后自动删除（所以用户发送给开发者的素材，若开发者需要，应尽快下载到本地），以节省服务器资源。
		2、media_id是可复用的。
		3、素材的格式大小等要求与公众平台官网一致。具体是，图片大小不超过2M，支持bmp/png/jpeg/jpg/gif格式，语音大小不超过5M，长度不超过60秒，支持mp3/wma/wav/amr格式
		4、需使用https调用本接口。
	 * 
	 * http请求方式: POST/FORM,需使用https
		https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE
		调用示例（使用curl命令，用FORM表单方式上传一个多媒体文件）：
		curl -F media=@test.jpg "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE"
	 * @param type  	是 	媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb） 
	 * @param media 是 	form-data中媒体文件标识，有filename、filelength、content-type等信息 
	 * @param access_token  	是 	调用接口凭证 
	 * @return
	 * @throws WeixinMpException
	 */
	UploadAttachmentResp updateMedia(String type,String media,String access_token )throws WeixinMpException;
	
	/**
	 * 
	 * 获得临时素才
	 * http请求方式: GET,https调用
		https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID
		请求示例（示例为通过curl命令获取多媒体文件）
		curl -I -G "https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID"
	 * @param media_id
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp getMedia(String media_id ,String access_token )throws WeixinMpException;
	
	/**
	 * 新增永久图文素材
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN
	 * @param req
	 * @param access_token
	 * @return
	 */
	AddAttachmentResp addMaterial(AddAttachmentReq req,String access_token)throws WeixinMpException;
	
	
	/**
	 * 获得永久图文素材
	 * http请求方式: POST,https调用
		https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=ACCESS_TOKEN
	 * @param req
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	GetAttachmentResp getMaterial(String media_id ,String access_token)throws WeixinMpException;
	
	/**
	 * 删除永久素材
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=ACCESS_TOKEN
	 * @param media_id
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp delMaterial(String media_id ,String access_token)throws WeixinMpException;
	
	/**
	 * 修改永久素材
	 * http请求方式: POST
		https://api.weixin.qq.com/cgi-bin/material/update_news?access_token=ACCESS_TOKEN
	 * @param media_id
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp updateMaterial(UpdateAttachmentReq req ,String access_token)throws WeixinMpException;
	
	/**
	 * 获取素材总数
	 * http请求方式: GET
		https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=ACCESS_TOKEN
	 * @param access_token
	 * @return
	 */
	GetMaterialCountResp getMaterialCount(String access_token)throws WeixinMpException;
	
	/**
	 * 获得素材列表
	 * @param req
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	GetMaterialListResp GetMaterialList(GetMaterialListReq req,String access_token)throws WeixinMpException;
	
	
}
