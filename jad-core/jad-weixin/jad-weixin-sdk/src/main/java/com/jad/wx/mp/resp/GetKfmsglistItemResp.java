package com.jad.wx.mp.resp;

import java.io.Serializable;

/**
 * 获取客服聊天记录
 * @author hechuan
 *
 */
public class GetKfmsglistItemResp implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String worker	;//完整客服帐号，格式为：帐号前缀@公众号微信号
	private String openid		;//用户标识
	private String opercode		;//操作码，2002（客服发送信息），2003（客服接收消息）
	private String text		;//聊天记录
	private String time		;//操作时间，unix时间戳
	
	public String getWorker() {
		return worker;
	}
	public void setWorker(String worker) {
		this.worker = worker;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getOpercode() {
		return opercode;
	}
	public void setOpercode(String opercode) {
		this.opercode = opercode;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
	

}
