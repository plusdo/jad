package com.jad.wx.mp.resp;

import java.util.List;

/**
 * 获取客服列表
 * @author hechuan
 *
 */
public class GetKfOnlineListResp extends CommonResp {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<KfOnlineInfo> kf_online_list;

	public List<KfOnlineInfo> getKf_online_list() {
		return kf_online_list;
	}

	public void setKf_online_list(List<KfOnlineInfo> kf_online_list) {
		this.kf_online_list = kf_online_list;
	}


}
