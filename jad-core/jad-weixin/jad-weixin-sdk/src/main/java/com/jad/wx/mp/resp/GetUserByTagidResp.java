package com.jad.wx.mp.resp;


/**
 * 跟据id获得标签
 * @author hechuan
 *
 */
public class GetUserByTagidResp extends CommonResp {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int count;//这次获取的粉丝数量
	private String next_openid;
	private GetUserByTagidDataResp data;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getNext_openid() {
		return next_openid;
	}
	public void setNext_openid(String next_openid) {
		this.next_openid = next_openid;
	}
	public GetUserByTagidDataResp getData() {
		return data;
	}
	public void setData(GetUserByTagidDataResp data) {
		this.data = data;
	}

}
