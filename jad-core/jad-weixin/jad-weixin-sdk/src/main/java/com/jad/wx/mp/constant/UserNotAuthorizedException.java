package com.jad.wx.mp.constant;

/**
 * 用户没有授权访问
 * @author hechuan
 *
 */
public class UserNotAuthorizedException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String openid;
	
	public UserNotAuthorizedException(String openid) {
		this.openid=openid;
	}

	public UserNotAuthorizedException(String openid,String message) {
		super(message);
		this.openid=openid;
	}

	public String getOpenid() {
		return openid;
	}


}
