package com.jad.wx.mp.resp;

import java.io.Serializable;

/**
 * 获得模板id
 * @author hechuan
 *
 */
public class GetTemplateListItemResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String access_token;//	是	接口调用凭证
	private String template_id;//	是	模板ID
	private String title;//	是	模板标题
	private String primary_industry;//	是	模板所属行业的一级行业
	private String deputy_industry;//	是	模板所属行业的二级行业
	private String content;//	是	模板内容
	private String example	;//是	模板示例
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public String getTemplate_id() {
		return template_id;
	}
	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPrimary_industry() {
		return primary_industry;
	}
	public void setPrimary_industry(String primary_industry) {
		this.primary_industry = primary_industry;
	}
	public String getDeputy_industry() {
		return deputy_industry;
	}
	public void setDeputy_industry(String deputy_industry) {
		this.deputy_industry = deputy_industry;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getExample() {
		return example;
	}
	public void setExample(String example) {
		this.example = example;
	}
	
	
	

}
