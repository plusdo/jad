package com.jad.wx.mp.constant;

/**
 * 用户没有关注公众号
 * @author hechuan
 *
 */
public class UserNotSubscribeException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String openid;
	
	public UserNotSubscribeException(String openid) {
		this.openid=openid;
	}

	public UserNotSubscribeException(String openid,String message) {
		super(message);
		this.openid=openid;
	}

	public String getOpenid() {
		return openid;
	}


}
