package com.jad.wx.mp.req;


/**
 * 发送音乐消息
 * @author hechuan
 *
 */
public class SendMusicMsgReq extends SendMsgReq{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SendMusicContentMsgReq music;
	public SendMusicContentMsgReq getMusic() {
		return music;
	}
	public void setMusic(SendMusicContentMsgReq music) {
		this.music = music;
	}
	
	

}
