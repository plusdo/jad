package com.jad.wx.mp.resp;

import java.util.List;

/**
 * 获得模板列表
 * @author hechuan
 *
 */
public class GetTemplateListResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<GetTemplateListItemResp>template_list;

	public List<GetTemplateListItemResp> getTemplate_list() {
		return template_list;
	}

	public void setTemplate_list(List<GetTemplateListItemResp> template_list) {
		this.template_list = template_list;
	}
	

}
