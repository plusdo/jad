package com.jad.wx.mp.req;

public class RecTemplatesendjobfinishEventReq  extends RecEventMsgReq{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String msgId ;//	消息id
	private String status ;//	发送状态为用户拒绝接收 ;
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
