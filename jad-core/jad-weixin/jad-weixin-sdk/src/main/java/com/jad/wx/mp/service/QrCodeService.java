package com.jad.wx.mp.service;

import java.io.File;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.req.CreateQrTicketReq;
import com.jad.wx.mp.resp.CreateQrTicketResp;
import com.jad.wx.mp.resp.GetShortUrlResp;

/**
 * 二维码
 * @author hechuan
 *
 */
public interface QrCodeService {
	
	/**
	 * 创建二维码 ticket
	 * URL: https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKENPOST
	 * @param req
	 * @param access_token
	 * @return
	 * @throws WeixinMpException
	 */
	CreateQrTicketResp createQrTicket(CreateQrTicketReq req,String access_token)throws WeixinMpException;
	
	/**
	 * 通过ticket获得二维码
	 * https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET
	 * @param ticket 提醒：TICKET记得进行UrlEncode
	 * @return
	 * @throws WeixinMpException
	 */
	byte[] showqrcode(String ticket)throws WeixinMpException;
	
	/**
	 * 通过ticket获得二维码图片文件
	 * @param ticket
	 * @param filePath
	 * @return
	 * @throws WeixinMpException
	 */
	File getQrcode(String ticket,String filePath)throws WeixinMpException;
	
	GetShortUrlResp toShorturl(String long_url,String access_token) throws WeixinMpException;

}
