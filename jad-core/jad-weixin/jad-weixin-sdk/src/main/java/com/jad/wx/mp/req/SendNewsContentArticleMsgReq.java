package com.jad.wx.mp.req;

import java.io.Serializable;


/**
 * 发送图文消息内容条目
 * @author hechuan
 *
 */
public class SendNewsContentArticleMsgReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String title;// 否 	图文消息/视频消息/音乐消息的标题 
	private String description ;// 否 	图文消息/视频消息/音乐消息的描述 
	private String url;//  否 	图文消息被点击后跳转的链接 
	private String picurl;//  否 	图文消息的图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80 
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPicurl() {
		return picurl;
	}
	public void setPicurl(String picurl) {
		this.picurl = picurl;
	}
	
	

}
