package com.jad.wx.mp.resp;

/**
 * 长链接转短链接
 * @author hechuan
 *
 */
public class GetShortUrlResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String short_url;//短链接。

	public String getShort_url() {
		return short_url;
	}

	public void setShort_url(String short_url) {
		this.short_url = short_url;
	}

}
