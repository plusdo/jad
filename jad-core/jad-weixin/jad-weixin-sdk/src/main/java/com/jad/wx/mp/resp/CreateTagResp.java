package com.jad.wx.mp.resp;


/**
 * 创建用户标签
 * @author hechuan
 * 
 *
 */
public class CreateTagResp extends CommonResp{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UserTagItem tag;

	public UserTagItem getTag() {
		return tag;
	}

	public void setTag(UserTagItem tag) {
		this.tag = tag;
	}
	
	
}
