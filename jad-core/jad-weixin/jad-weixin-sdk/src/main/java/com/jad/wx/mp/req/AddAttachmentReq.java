package com.jad.wx.mp.req;

import java.io.Serializable;
import java.util.List;

/**
 * 上传永久素材
 * @author hechuan
 *
 */
public class AddAttachmentReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<AddAttachmentArticleReq>articles;

	public List<AddAttachmentArticleReq> getArticles() {
		return articles;
	}

	public void setArticles(List<AddAttachmentArticleReq> articles) {
		this.articles = articles;
	}
	
}
