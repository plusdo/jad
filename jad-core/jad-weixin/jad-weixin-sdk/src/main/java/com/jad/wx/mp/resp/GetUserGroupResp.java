package com.jad.wx.mp.resp;


/**
 * 查询用户所在的分组
 * @author hechuan
 *
 */
public class GetUserGroupResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String groupid;

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}


}
