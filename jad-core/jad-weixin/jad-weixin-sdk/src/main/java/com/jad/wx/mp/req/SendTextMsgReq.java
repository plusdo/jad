package com.jad.wx.mp.req;


/**
 * 发送文本消息
 * @author hechuan
 *
 */
public class SendTextMsgReq extends SendMsgReq{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SendTextContentMsgReq text;
	public SendTextContentMsgReq getText() {
		return text;
	}
	public void setText(SendTextContentMsgReq text) {
		this.text = text;
	}

}
