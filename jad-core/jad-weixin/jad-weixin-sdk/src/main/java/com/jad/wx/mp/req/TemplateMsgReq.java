package com.jad.wx.mp.req;

import java.io.Serializable;
import java.util.Map;

/**
 * 模板消息
 * @author hechuan
 *
 */
public class TemplateMsgReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String touser;//"touser":"OPENID",
	private String template_id;//"template_id":"ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY",
	private String url;//"url":"http://weixin.qq.com/download",            
	private String topcolor;
	
	
	private Map<String, TemplateMsgItemReq> data;
	
	public String getTouser() {
		return touser;
	}
	public void setTouser(String touser) {
		this.touser = touser;
	}
	public String getTemplate_id() {
		return template_id;
	}
	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTopcolor() {
		return topcolor;
	}
	public void setTopcolor(String topcolor) {
		this.topcolor = topcolor;
	}
	public Map<String, TemplateMsgItemReq> getData() {
		return data;
	}
	public void setData(Map<String, TemplateMsgItemReq> data) {
		this.data = data;
	}

}
