package com.jad.wx.mp.req;


/**
 * 发送语音消息 
 * @author hechuan
 *
 */
public class SendVoiceMsgReq extends SendMsgReq{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SendVoiceContentMsgReq voice;
	
	public SendVoiceContentMsgReq getVoice() {
		return voice;
	}
	public void setVoice(SendVoiceContentMsgReq voice) {
		this.voice = voice;
	}

}
