package com.jad.wx.mp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.http.HttpResp;
import com.jad.commons.http.HttpUtil;
import com.jad.commons.http.MyHttpException;
import com.jad.commons.json.JsonMapper;
import com.jad.wx.mp.constant.MpConstant;
import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.resp.GetAccessTokenResp;
import com.jad.wx.mp.service.WxmpApiService;

public class WxmpApiServiceImpl extends AbstractWxServiceImpl implements WxmpApiService {

	private static Logger logger=LoggerFactory.getLogger(WxmpApiServiceImpl.class);
	
//	avggWUJQdjYzrWbruPktZ-9Ydob1I4fXt1TkEqBaCtiAqXx1Nez6TyQWYe9mVglty27c7jI_yXsol-mubgxJJ9oXB5jyCYCH002O3wAyjjw2skgPh7c0NP1sQ-CDVAPJHXWiAAASIH
	
	public static void main(String[] args) {
		
		//测试号
//		String appid="wx40659e5cfc5bea25";
//		String secret="5e7acbb8068b4a9b9dde6881311c64fe";
		
//		wx496e5fb817c905fc
//		da30a2020d2516af1377b6c18e9d07bb
		
		// 寻梦树
		String appid="wx496e5fb817c905fc";
		String secret="da30a2020d2516af1377b6c18e9d07bb";
		
		WxmpApiServiceImpl service=new WxmpApiServiceImpl();
		
		try {
			GetAccessTokenResp resp=service.getAccessToken(appid, secret);
			System.out.println("token:"+resp.getAccess_token());
		} catch (WeixinMpException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public GetAccessTokenResp getAccessToken(String appid, String secret)throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/token?grant_type=client_credential&appid="+appid+"&secret="+secret;
		
		try {
			
			HttpResp httpResp=HttpUtil.get(url);
			
			GetAccessTokenResp resp=(GetAccessTokenResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetAccessTokenResp.class);
			
			checkSuccess(resp);
			
			System.out.println("收到结果:"+JsonMapper.toJsonString(resp));
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
	}

}
