package com.jad.wx.mp.req;


/**
 * 发送图片消息
 * @author hechuan
 *
 */
public class SendImageMsgReq extends SendMsgReq{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SendImageContentMsgReq image;
	public SendImageContentMsgReq getImage() {
		return image;
	}
	public void setImage(SendImageContentMsgReq image) {
		this.image = image;
	}

}
