package com.jad.wx.mp.req;

import java.io.Serializable;
import java.util.List;


/**
 * 发送图文消息内容
 * @author hechuan
 *
 */
public class SendNewsContentMsgReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 图文消息条数限制在10条以内，注意，如果图文数超过10，则将会无响应。 
	 */
	private List<SendNewsContentArticleMsgReq>articles;

	public List<SendNewsContentArticleMsgReq> getArticles() {
		return articles;
	}

	public void setArticles(List<SendNewsContentArticleMsgReq> articles) {
		this.articles = articles;
	}
	
	
	

}
