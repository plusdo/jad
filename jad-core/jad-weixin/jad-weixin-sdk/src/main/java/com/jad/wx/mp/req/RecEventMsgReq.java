package com.jad.wx.mp.req;

/**
 * 接收事件
 * @author hechuan
 *
 */
public class RecEventMsgReq extends RecMsgReq {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected String toUserName;// 	开发者微信号
	protected String fromUserName;// 	发送方帐号（一个OpenID）
	protected String createTime ;//	消息创建时间 （整型）
	protected String msgType;// 	消息类型
	protected String event ;//	事件类型
	
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getFromUserName() {
		return fromUserName;
	}
	public void setFromUserName(String fromUserName) {
		this.fromUserName = fromUserName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	

}
