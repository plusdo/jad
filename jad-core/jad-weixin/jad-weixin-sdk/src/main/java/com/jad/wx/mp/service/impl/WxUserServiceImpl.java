package com.jad.wx.mp.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.http.HttpResp;
import com.jad.commons.http.HttpUtil;
import com.jad.commons.http.MyHttpException;
import com.jad.commons.json.JsonMapper;
import com.jad.wx.mp.constant.MpConstant;
import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.CreateGroupResp;
import com.jad.wx.mp.resp.CreateTagResp;
import com.jad.wx.mp.resp.GetBlackListResp;
import com.jad.wx.mp.resp.GetGroupResp;
import com.jad.wx.mp.resp.GetTagsResp;
import com.jad.wx.mp.resp.GetUserByTagidResp;
import com.jad.wx.mp.resp.GetUserGroupResp;
import com.jad.wx.mp.resp.GetUserInfoResp;
import com.jad.wx.mp.resp.GetUserListResp;
import com.jad.wx.mp.resp.GetidlistResp;
import com.jad.wx.mp.resp.WxmpGroup;
import com.jad.wx.mp.service.WxUserService;

public class WxUserServiceImpl extends AbstractWxServiceImpl implements WxUserService {
	
	private static Logger logger=LoggerFactory.getLogger(WxUserServiceImpl.class);
	
	public static void main(String[] args) {
		
		WxUserServiceImpl service=new WxUserServiceImpl();
		
		CommonResp resp=null;
		
		try {
			String openid="o2zDCt7_YbDAUXBY-fINZ1W4VVkk";
			
			String access_token=MpConstant.TOKEN;
			
//			resp=service.getUserList(null, MpConstant.TOKEN);
			
			resp=service.batchblacklist(new String[]{openid}, access_token);
			
			resp=service.getBlackList(null, MpConstant.TOKEN);
			System.out.println("黑名单列表:"+JsonMapper.toJsonString(resp));
			
			resp=service.batchunblacklist(new String[]{openid}, access_token);
			System.out.println("取消拉黑:"+JsonMapper.toJsonString(resp));
			
			resp=service.getBlackList(null, MpConstant.TOKEN);
			System.out.println("黑名单列表:"+JsonMapper.toJsonString(resp));
			
			
//			resp=service.getTags(MpConstant.TOKEN);
//			System.out.println("查看标签测试结果:"+JsonMapper.toJsonString(resp));
			
//			resp=service.moveUserToGroup(openid, "101", MpConstant.TOKEN);
//			System.out.println("查看组测试结果:"+JsonMapper.toJsonString(resp));
			
//			resp=service.batchtagging("103", new String[]{openid}, MpConstant.TOKEN);
//			System.out.println("查看batchtagging试结果:"+JsonMapper.toJsonString(resp));
//			
////			resp=service.batchuntagging("101",new String[]{openid}, MpConstant.TOKEN);
////			System.out.println("查看batchuntagging试结果:"+JsonMapper.toJsonString(resp));
//			
//			resp=service.getidlist(openid, MpConstant.TOKEN);
//			System.out.println("查看getidlist试结果:"+JsonMapper.toJsonString(resp));
//			
//			resp=service.getWxmpUserInfo(openid, MpConstant.TOKEN);
//			System.out.println("查看试结果:"+JsonMapper.toJsonString(resp));
//			
//			resp=service.getUserByTagid("101", null, MpConstant.TOKEN);
//			System.out.println("查看结果:"+JsonMapper.toJsonString(resp));
			
//			resp=service.createGroup("测试组", MpConstant.TOKEN);
			
//			resp=service.getGroup(MpConstant.TOKEN);
//			System.out.println("查看组测试结果:"+JsonMapper.toJsonString(resp));
			

			
//			resp=service.updateTag("104", "修改了的标签w", MpConstant.TOKEN);
//			System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
		
//			resp=service.delTag("102",  MpConstant.TOKEN);
//			System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
			
			
//			resp=service.modifyGroup("100", "哈哈笑组2", MpConstant.TOKEN);
//			resp=service.getGroup(MpConstant.TOKEN);
			
//			resp=service.queryUserGroup(openid, MpConstant.TOKEN);
			
//			resp=service.updateRemark(openid,"测试备注",MpConstant.TOKEN);
			
//			resp=service.createTag("测试创建标签", MpConstant.TOKEN);
//			System.out.println("测试结果:"+JsonMapper.toJsonString(resp));
			
//			resp=service.getWxmpUserInfo(openid, MpConstant.TOKEN);
//			System.out.println("查看测试结果:"+JsonMapper.toJsonString(resp));
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
//	o2zDCt7_YbDAUXBY-fINZ1W4VVkk
	
	@Override
	public GetUserListResp getUserList(String next_openid, String access_token)throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/user/get?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			GetUserListResp resp=(GetUserListResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetUserListResp.class);
			
			checkSuccess(resp);
			
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}
	@Override
	public GetUserInfoResp getWxmpUserInfo( String openid,String access_token) throws WeixinMpException {
//		TODO
		String url=MpConstant.API_URL+"/user/info?access_token="+access_token+"&openid="+openid+"&lang=zh_CN";
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			GetUserInfoResp resp=(GetUserInfoResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetUserInfoResp.class);
			
			checkSuccess(resp);
			
			return resp;
		} catch (MyHttpException e){
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
	}
	
	
	@Override
	public CreateGroupResp createGroup(String name, String access_token)throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/groups/create?access_token="+access_token;
		
		WxmpGroup group=new WxmpGroup();
		group.setName(name);
		Map<String,WxmpGroup>map=new HashMap<String,WxmpGroup>();
		
		map.put("group", group);
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(map));
			
			CreateGroupResp resp=(CreateGroupResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CreateGroupResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e){
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
	}

	@Override
	public GetGroupResp getGroup(String access_token) throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/groups/get?access_token="+access_token;
		
		try {
			
			HttpResp httpResp=HttpUtil.get(url);
			
			GetGroupResp resp=(GetGroupResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetGroupResp.class);
			
			checkSuccess(resp);
			
			
			return resp;
			
		} catch (MyHttpException e){
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public GetUserGroupResp queryUserGroup(String openid, String access_token)throws WeixinMpException {
		
		
		String url=MpConstant.API_URL+"/groups/getid?access_token="+access_token;
		Map<String,String> params=new HashMap<String,String>();
		params.put("openid", openid);
		
		try {
			
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(params));
			
			GetUserGroupResp resp=(GetUserGroupResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetUserGroupResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
	}

	@Override
	public CommonResp modifyGroup(String id, String name, String access_token)throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/groups/update?access_token="+access_token;
		
		Map<String,Map<String,String>> reqParam=new HashMap<String,Map<String,String>>();
		
		Map<String,String> params=new HashMap<String,String>();
		reqParam.put("group", params);
		params.put("id", id);
		params.put("name", name);
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(reqParam));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
	}

	@Override
	public CommonResp moveUserToGroup(String openid, String to_groupid,String access_token) throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/groups/members/update?access_token="+access_token;
		
		Map<String,String> params=new HashMap<String,String>();
		params.put("openid", openid);
		params.put("to_groupid", to_groupid);
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public CommonResp moveUserToGroup(String[] openid_list, String to_groupid,String access_token) throws WeixinMpException {
	
		String url=MpConstant.API_URL+"/groups/members/batchupdate?access_token="+access_token;
		
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("openid_list", openid_list);
		params.put("to_groupid", to_groupid);
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public CommonResp delGroup(String id, String access_token)throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/groups/delete?access_token="+access_token;
		
		Map<String,Map<String,String>> params=new HashMap<String,Map<String,String>>();
		
		Map<String,String> idMap=new HashMap<String,String>();
		params.put("group", idMap);
		idMap.put("id", id);
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public CommonResp updateRemark(String openid, String remark,String access_token) throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/user/info/updateremark?access_token="+access_token;
		
		Map<String,String> params=new HashMap<String,String>();
		params.put("openid", openid);
		params.put("remark", remark);
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}



	@Override
	public CreateTagResp createTag(String name, String access_token)throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/tags/create?access_token="+access_token;
		
		Map<String,Map<String,String>> params=new HashMap<String,Map<String,String>>();
		
		Map<String,String> tagMap=new HashMap<String,String>();
		params.put("tag", tagMap);
		tagMap.put("name", name);
		
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url, JsonMapper.toJsonString(params));
			
			CreateTagResp resp=(CreateTagResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CreateTagResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
	}

	@Override
	public GetTagsResp getTags(String access_token) throws WeixinMpException {
		
		String url=MpConstant.API_URL+"/tags/get?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.get(url);
			
			GetTagsResp resp=(GetTagsResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetTagsResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public CommonResp updateTag(String id, String name, String access_token)throws WeixinMpException {
		
		Map<String,Map<String,String>>params=new HashMap<String,Map<String,String>>();
		Map<String,String>tabMap=new HashMap<String,String>();
		params.put("tag", tabMap);
		tabMap.put("id", id);
		tabMap.put("name", name);
		
		String url=MpConstant.API_URL+"/tags/update?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public CommonResp delTag(String id, String access_token)throws WeixinMpException {
		
		Map<String,Map<String,String>>params=new HashMap<String,Map<String,String>>();
		Map<String,String>tagMap=new HashMap<String,String>();
		params.put("tag", tagMap);
		tagMap.put("id", id);
		
		String url=MpConstant.API_URL+"/tags/delete?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public GetUserByTagidResp getUserByTagid(String tagid, String next_openid,String access_token) throws WeixinMpException {
		
		Map<String,String>params=new HashMap<String,String>();
		params.put("tagid", tagid);
		if(StringUtils.isNotBlank(next_openid)){
			params.put("next_openid", next_openid);
		}
		
		String url=MpConstant.API_URL+"/user/tag/get?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			GetUserByTagidResp resp=(GetUserByTagidResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetUserByTagidResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		

	
	
	}

	@Override
	public CommonResp batchtagging(String tagid, String[] openid_list,String access_token) throws WeixinMpException {
		
		Map<String,Object>params=new HashMap<String,Object>();
		params.put("tagid", tagid);
		params.put("openid_list", openid_list);
		
		String url=MpConstant.API_URL+"/tags/members/batchtagging?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
	}

	@Override
	public CommonResp batchuntagging(String tagid, String[] openid_list, String access_token) throws WeixinMpException {
		
		Map<String,Object>params=new HashMap<String,Object>();
		params.put("tagid", tagid);
		params.put("openid_list", openid_list);
		
		String url=MpConstant.API_URL+"/tags/members/batchuntagging?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
	}

	@Override
	public GetidlistResp getidlist(String openid, String access_token) throws WeixinMpException {
	
		Map<String,String>params=new HashMap<String,String>();
		params.put("openid", openid);
		
		String url=MpConstant.API_URL+"/tags/getidlist?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			GetidlistResp resp=(GetidlistResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetidlistResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
	}



	@Override
	public GetBlackListResp getBlackList(String begin_openid, String access_token)throws WeixinMpException {
		
		Map<String,String>params=new HashMap<String,String>();
		if(StringUtils.isNotBlank(begin_openid)){
			params.put("begin_openid", begin_openid);
		}
		
		String url=MpConstant.API_URL+"/tags/members/getblacklist?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			GetBlackListResp resp=(GetBlackListResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), GetBlackListResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
		
		
	}

	@Override
	public CommonResp batchblacklist(String[] openid_list, String access_token) throws WeixinMpException {
		
		Map<String,Object>params=new HashMap<String,Object>();
		params.put("openid_list", openid_list);
		String url=MpConstant.API_URL+"/tags/members/batchblacklist?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

	@Override
	public CommonResp batchunblacklist(String[] openid_list, String access_token) throws WeixinMpException {
			
		Map<String,Object>params=new HashMap<String,Object>();
		params.put("openid_list", openid_list);
		String url=MpConstant.API_URL+"/tags/members/batchunblacklist?access_token="+access_token;
		
		try {
			HttpResp httpResp=HttpUtil.postMsg(url,JsonMapper.toJsonString(params));
			
			CommonResp resp=(CommonResp)JsonMapper.fromJsonString(httpResp.getRespMsg(), CommonResp.class);
			
			checkSuccess(resp);
			
			return resp;
			
		} catch (MyHttpException e) {
			logger.error("通讯异常,"+e.getMessage(),e);
			throw new WeixinMpException("抱歉，服务器通讯异常,请联系管理员。");
		}
		
	}

}
