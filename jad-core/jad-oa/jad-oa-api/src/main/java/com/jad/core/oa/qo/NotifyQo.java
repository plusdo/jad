package com.jad.core.oa.qo;

import com.jad.commons.qo.BaseQo;

public class NotifyQo extends BaseQo<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String type;		// 类型
	private String title;		// 标题
	private String status;		// 状态
	

	public NotifyQo() {
	}
	
	public NotifyQo(String id) {
		super(id);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
