/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.oa.service;

import java.util.List;

import com.jad.commons.service.CurdService;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.core.oa.qo.NotifyQo;
import com.jad.core.oa.vo.NotifyRecordVo;
import com.jad.core.oa.vo.NotifyVo;



public interface NotifyService extends CurdService<NotifyVo,String>  {

	/**
	 * 获取通知发送记录
	 * @param notifyQo
	 * @return
	 */
	public List<NotifyRecordVo> getRecordList(String notifyId) ;
	
	/**
	 * 查看通知列表
	 * @param page
	 * @param notifyVo
	 * @return
	 */
	public Page<NotifyVo> findUserNodify(PageQo page, NotifyQo notifyQo,String userId) ;
	
	/**
	 * 获取通知数目
	 * @param notifyQo
	 * @return
	 */
	public Long findUserNodifyCount(NotifyQo notifyQo ,String userId) ;
	
	
	/**
	 * 更新阅读状态
	 */
	public void updateReadFlag(String notifyId) ;
	
}


