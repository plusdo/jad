/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.oa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.BaseEo;

/**
 * 通知通告Entity
 * @author ThinkGem
 * @version 2014-05-16
 */
@Entity
@Table(name = "oa_notify")
public class NotifyEo extends BaseEo<String> {	
	
	private static final long serialVersionUID = 1L;
	
	private String type;		// 类型
	private String title;		// 标题
	private String content;		// 类型
	private String files;		// 附件
	private String status;		// 状态
	
//	private String readNum;		// 已读
//	private String unReadNum;	// 未读
//	private String readFlag;	// 本人阅读状态
//	private Boolean isSelf;		// 是否只查询自己的通知
//	private List<OaNotifyRecord> oaNotifyRecordList = Lists.newArrayList();
	
	public NotifyEo() {
		super();
	}


	@Column(name="title")
	@Length(min=0, max=200, message="标题长度必须介于 0 和 200 之间")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name="type")
	@Length(min=0, max=1, message="类型长度必须介于 0 和 1 之间")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name="status")
	@Length(min=0, max=1, message="状态长度必须介于 0 和 1 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="files")
	@Length(min=0, max=2000, message="附件长度必须介于 0 和 2000 之间")
	public String getFiles() {
		return files;
	}

	public void setFiles(String files) {
		this.files = files;
	}

	@Column(name="content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}



	


}


