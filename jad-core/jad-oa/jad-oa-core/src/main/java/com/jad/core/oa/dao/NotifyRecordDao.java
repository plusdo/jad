/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.oa.dao;

import com.jad.core.oa.entity.NotifyRecordEo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 通知通告记录DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@JadDao
public interface NotifyRecordDao extends JadEntityDao<NotifyRecordEo,String> {

//	/**
//	 * 插入通知记录
//	 * @param notifyRecordList
//	 * @return
//	 */
//	public int insertAll(List<NotifyRecordEo> notifyRecordList);
//	
//	/**
//	 * 根据通知ID删除通知记录
//	 * @param notifyId 通知ID
//	 * @return
//	 */
//	public int deleteByNotifyId(String notifyId);
//	
//	public List<NotifyRecordEo> findList(String notifyId);
	
}