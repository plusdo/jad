/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.oa.dao;

import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.core.oa.entity.NotifyEo;
import com.jad.core.oa.qo.NotifyQo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 通知通告DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@JadDao
public interface NotifyDao extends JadEntityDao<NotifyEo,String> {
	
	/**
	 * 获取用户通知数目
	 * @param notifyQo
	 * @return
	 */
	public Long findUserNodifyCount(NotifyQo notifyQo ,String userId);
	
	public Page<NotifyEo> findUserNodify(PageQo page, NotifyQo notifyQo,String userId);
	
}