package com.jad.dao.mybatis.mapper;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

import org.apache.ibatis.reflection.ExceptionUtil;
import org.apache.ibatis.session.SqlSession;

import com.jad.dao.JadRootDao;

/**
 * @author Clinton Begin
 * @author Eduardo Macarron
 */
public class JadMapperProxy<T> implements InvocationHandler, Serializable {

  private static final long serialVersionUID = -6424540398559729838L;
  private final SqlSession sqlSession;
  private final Class<T> mapperInterface;
  private final Map<Method, JadMapperMethod> methodCache;

  public JadMapperProxy(SqlSession sqlSession, Class<T> mapperInterface, Map<Method, JadMapperMethod> methodCache) {
    this.sqlSession = sqlSession;
    this.mapperInterface = mapperInterface;
    this.methodCache = methodCache;
  }
  

	@Override
	  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Class declarClass=method.getDeclaringClass();
		
//		System.out.println("declarClass:"+declarClass.getName()
//				+",methodName:"+method.getName()+",args.length:"+args.length);
	    
		if (Object.class.equals(declarClass)) {
	      try {
	        return method.invoke(this, args);
	      } catch (Throwable t) {
	        throw ExceptionUtil.unwrapThrowable(t);
	      }
	    }
		
		if(JadRootDao.class.equals(declarClass)){
			System.out.println("来这里鸟....");
//			return invokeJadDynamicMthod(method,args);
		}
	    
	    if("flush".equals(method.getName())){
	    	return sqlSession.flushStatements();
	    }
	    else if("exists".equals(method.getName()) 
	    		&& args!=null && args.length==1 
	    		){
	    	String name=mapperInterface.getName() + ".findOne" ;
	    	Object result = sqlSession.selectOne(name, args[0]);
	    	return result!=null;
	    }
	    
	    final JadMapperMethod mapperMethod = cachedMapperMethod(method);
	    
	    if("save".equals(method.getName()) 
	    		&& args!=null && args.length==1 
	    		){
	    	return (T)mapperMethod.save((T)args[0]);
	    	
	    }else if("saveAndFlush".equals(method.getName()) 
	    		&& args!=null && args.length==1 
	    		){
	    	
	    	T t=(T)mapperMethod.save((T)args[0]);
	    	flush();
	    	return t;
	    }
	    
	    
	    else{
	    	return mapperMethod.execute(sqlSession, args);
	    }
	    
	    
	  }
	
	
	public void flush(){
		sqlSession.flushStatements();
	}


	  private JadMapperMethod cachedMapperMethod(Method method) {
		 JadMapperMethod mapperMethod = (JadMapperMethod)methodCache.get(method);
	    if (mapperMethod == null) {
	      mapperMethod = new JadMapperMethod(mapperInterface, method, sqlSession);
	      methodCache.put(method, mapperMethod);
	    }
	    return mapperMethod;
	  }
	

}