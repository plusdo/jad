package com.jad.dao.mybatis.conf;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.mapper.ClassPathMapperScanner;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.scope.ScopedProxyUtils;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.annotation.AnnotatedGenericBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.AnnotationConfigUtils;
import org.springframework.context.annotation.AnnotationScopeMetadataResolver;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ScopeMetadata;
import org.springframework.context.annotation.ScopeMetadataResolver;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.enums.RespositoryType;

public class JadClassPathMapperScanner extends ClassPathMapperScanner{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JadClassPathMapperScanner.class);
	
	private String implPostfix;//实现类后缀
	
	private static final String COMPONENT_ANNOTATION_CLASSNAME = "org.springframework.stereotype.Component";
	
	private static final String CUSTOM_IMPLEMENTATION_RESOURCE_PATTERN = "**/*%s.class";
	
	private BeanNameGenerator beanNameGenerator = new AnnotationBeanNameGenerator();

	private ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
	private MetadataReaderFactory metadataReaderFactory =
			new CachingMetadataReaderFactory(this.resourcePatternResolver);
	
	private ScopeMetadataResolver scopeMetadataResolver = new AnnotationScopeMetadataResolver();
	private final BeanDefinitionRegistry registry;
	
	  private boolean addToConfig = true;
	  

	  private SqlSessionFactory sqlSessionFactory;

	  private SqlSessionTemplate sqlSessionTemplate;

	  private String sqlSessionTemplateBeanName;

	  private String sqlSessionFactoryBeanName;

	  private ApplicationContext applicationContext;
	  
	  private ResourceLoader resourceLoader;
	  
	  private Class<? extends Annotation> annotationClass;

	  private Class<?> markerInterface;

	  public JadClassPathMapperScanner(BeanDefinitionRegistry registry) {
	    super(registry);
	    this.registry=registry;
	  }

	  public void setAddToConfig(boolean addToConfig) {
	    this.addToConfig = addToConfig;
	  }

	  public void setAnnotationClass(Class<? extends Annotation> annotationClass) {
	    this.annotationClass = annotationClass;
	  }

	  public void setMarkerInterface(Class<?> markerInterface) {
	    this.markerInterface = markerInterface;
	  }

	  public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
	    this.sqlSessionFactory = sqlSessionFactory;
	  }

	  public void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
	    this.sqlSessionTemplate = sqlSessionTemplate;
	  }

	  public void setSqlSessionTemplateBeanName(String sqlSessionTemplateBeanName) {
	    this.sqlSessionTemplateBeanName = sqlSessionTemplateBeanName;
	  }

	  public void setSqlSessionFactoryBeanName(String sqlSessionFactoryBeanName) {
	    this.sqlSessionFactoryBeanName = sqlSessionFactoryBeanName;
	  }
	  
	  public void setResourceLoader(ResourceLoader resourceLoader) {
		  super.setResourceLoader(resourceLoader);
		  this.resourceLoader=resourceLoader;
	  }

	  /**
	   * Configures parent scanner to search for the right interfaces. It can search
	   * for all interfaces or just for those that extends a markerInterface or/and
	   * those annotated with the annotationClass
	   */
	  public void registerFilters() {
	    boolean acceptAllInterfaces = true;

	    // if specified, use the given annotation and / or marker interface
	    if (this.annotationClass != null) {
	      addIncludeFilter(new AnnotationTypeFilter(this.annotationClass));
	      acceptAllInterfaces = false;
	    }

	    // override AssignableTypeFilter to ignore matches on the actual marker interface
	    if (this.markerInterface != null) {
	      addIncludeFilter(new AssignableTypeFilter(this.markerInterface) {
	        @Override
	        protected boolean matchClassName(String className) {
	          return false;
	        }
	      });
	      acceptAllInterfaces = false;
	    }

	    if (acceptAllInterfaces) {
	      // default include filter that accepts all classes
	      addIncludeFilter(new TypeFilter() {
	        public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
	          return true;
	        }
	      });
	    }

	    // exclude package-info.java
	    addExcludeFilter(new TypeFilter() {
	      public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
	        String className = metadataReader.getClassMetadata().getClassName();
	        return className.endsWith("package-info");
	      }
	    });
	  }
	  
	  public Set<BeanDefinitionHolder> superDoScan(String... basePackages) {
		  
			Assert.notEmpty(basePackages, "At least one base package must be specified");
			Set<BeanDefinitionHolder> beanDefinitions = new LinkedHashSet<BeanDefinitionHolder>();
			for (String basePackage : basePackages) {
				Set<BeanDefinition> candidates = findCandidateComponents(basePackage);
				for (BeanDefinition candidate : candidates) {
					ScopeMetadata scopeMetadata = this.scopeMetadataResolver.resolveScopeMetadata(candidate);
					candidate.setScope(scopeMetadata.getScopeName());
					
					String beanName=null;
					
					String implBeanName=registerCustomImplementation(candidate,basePackages);//注册实现类
					if(implBeanName!=null){
						beanName=implBeanName+AbstractJadEntityDao.DELEGATE_BEAN_NAME_SUFFIX;
						logger.debug("把baenName的名字改为:"+beanName);
					}else{
						beanName = this.beanNameGenerator.generateBeanName(candidate, this.registry);
					}
					
					if (candidate instanceof AbstractBeanDefinition) {
						postProcessBeanDefinition((AbstractBeanDefinition) candidate, beanName);
					}
					if (candidate instanceof AnnotatedBeanDefinition) {
						AnnotationConfigUtils.processCommonDefinitionAnnotations((AnnotatedBeanDefinition) candidate);
					}
					if (checkCandidate(beanName, candidate)) {
						BeanDefinitionHolder definitionHolder = new BeanDefinitionHolder(candidate, beanName);
						definitionHolder = applyScopedProxyMode(scopeMetadata, definitionHolder, this.registry);
						beanDefinitions.add(definitionHolder);
						registerBeanDefinition(definitionHolder, this.registry);
					}
				}
			}
			return beanDefinitions;
		  
	  }
	  
	  private synchronized String registerCustomImplementation(BeanDefinition candidate, String[] basePackages) {
			
			
//			CachingMetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourceLoader);
//			CustomRepositoryImplementationDetector implementationDetector 
//					= new CustomRepositoryImplementationDetector(metadataReaderFactory, applicationContext.getEnvironment(),resourceLoader);
			
			String beanAliasName = getImplementationBeanName(candidate);
	//

			// Already a bean configured?
			if (registry.isAlias(beanAliasName) || registry.containsBeanDefinition(beanAliasName)  ) {
				return beanAliasName;
			}
			
//			AbstractBeanDefinition beanDefinition = implementationDetector.detectCustomImplementation(
//					getImplementationClassName(candidate), basePackages);
			
			AbstractBeanDefinition beanDefinition = detectCustomImplementation(getImplementationClassName(candidate),basePackages);
			
			if (null == beanDefinition) {
				return null;
			}
			
			String implClassName=beanDefinition.getBeanClassName();
			Class implClass=null;
			try {
				implClass=ClassUtils.forName(implClassName, candidate.getClass().getClassLoader());
			} catch (Exception e) {
				LOGGER.warn("无法加载实现类"+implClassName+",跳过,"+e.getMessage(),e);
				return null;
			}
			
			if(!AbstractJadEntityDao.class.isAssignableFrom(implClass)){
				LOGGER.warn("实现类不是"+implClassName+"AbstractJadEntityDao的子类,跳过");
				return null;
			}
			
			AnnotatedBeanDefinition annotatedBeanDefinition = new AnnotatedGenericBeanDefinition(implClass);
			
			String realBeanName=determineBeanNameFromAnnotation(annotatedBeanDefinition);//查看注解上的名称
			if(realBeanName==null || "".equals(realBeanName.trim())){
				realBeanName=beanAliasName;
			}
			
			beanDefinition.getPropertyValues().add("repositoryType", RespositoryType.MYBATIS.getRespositoryType());
			
			
			if(!realBeanName.equals(beanAliasName)){
				if(registry.containsBeanDefinition(realBeanName)){
					
					registry.getBeanDefinition(realBeanName).getPropertyValues().add("repositoryType", RespositoryType.MYBATIS.getRespositoryType());
					
					registry.registerAlias(realBeanName, beanAliasName);//注册实现类别名，以防止下次重复判断，提升性能
					return realBeanName;
				}
			}
				
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Registering custom repository implementation: " + getImplementationBeanName(candidate) + " "
						+ beanDefinition.getBeanClassName());
			}

//			beanDefinition.setSource(configuration.getSource());//
			
			
			registry.registerBeanDefinition(realBeanName, beanDefinition);//注册实现类
			
			
			if(!realBeanName.equals(beanAliasName)){
				registry.registerAlias(realBeanName, beanAliasName);
			}
			
			return realBeanName;
		}
	  
	  
	  public String getImplementationClassName(BeanDefinition definition){
		  Assert.notNull(implPostfix, "implPostfix must be specified");
		  String shortClassName = ClassUtils.getShortName(definition.getBeanClassName());
		  return shortClassName+implPostfix;
	  }
	  
	  public String getImplementationBeanName(BeanDefinition definition) {
			return StringUtils.uncapitalize(getImplementationClassName(definition));
		}
	  protected String determineBeanNameFromAnnotation(AnnotatedBeanDefinition annotatedDef) {
			if(annotatedDef==null)return null;
			AnnotationMetadata amd = annotatedDef.getMetadata();
			Set<String> types = amd.getAnnotationTypes();
			String beanName = null;
			for (String type : types) {
				AnnotationAttributes attributes = AnnotationAttributes.fromMap(amd.getAnnotationAttributes(type, false));
				if (isStereotypeWithNameValue(type, amd.getMetaAnnotationTypes(type), attributes)) {
					Object value = attributes.get("value");
					if (value instanceof String) {
						String strVal = (String) value;
						if (StringUtils.hasText(strVal)) {
							if (beanName != null && !strVal.equals(beanName)) {
								throw new IllegalStateException("Stereotype annotations suggest inconsistent " +
										"component names: '" + beanName + "' versus '" + strVal + "'");
							}
							beanName = strVal;
						}
					}
				}
			}
			return beanName;
		}
	  
	  protected boolean isStereotypeWithNameValue(String annotationType,
				Set<String> metaAnnotationTypes, Map<String, Object> attributes) {

			boolean isStereotype = annotationType.equals(COMPONENT_ANNOTATION_CLASSNAME) ||
					(metaAnnotationTypes != null && metaAnnotationTypes.contains(COMPONENT_ANNOTATION_CLASSNAME)) ||
					annotationType.equals("javax.annotation.ManagedBean") ||
					annotationType.equals("javax.inject.Named");

			return (isStereotype && attributes != null && attributes.containsKey("value"));
		}
		
	  
	  
	  public AbstractBeanDefinition detectCustomImplementation(String className, String[] basePackages) {

			Assert.notNull(className, "ClassName must not be null!");
			Assert.notNull(basePackages, "BasePackages must not be null!");

			// Build pattern to lookup implementation class
			Pattern pattern = Pattern.compile(".*\\." + className);

			// Build classpath scanner and lookup bean definition
			ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
			
//			provider.setEnvironment(environment);
//			provider.setResourceLoader(resourceLoader);
			
			provider.setEnvironment(applicationContext.getEnvironment());
			provider.setResourceLoader(resourceLoader);
			
			provider.setResourcePattern(String.format(CUSTOM_IMPLEMENTATION_RESOURCE_PATTERN, className));
			provider.setMetadataReaderFactory(metadataReaderFactory);
			provider.addIncludeFilter(new RegexPatternTypeFilter(pattern));

			Set<BeanDefinition> definitions = new HashSet<BeanDefinition>();

			for (String basePackage : basePackages) {
				definitions.addAll(provider.findCandidateComponents(basePackage));
			}

			if (definitions.isEmpty()) {
				return null;
			}

			if (definitions.size() == 1) {
				return (AbstractBeanDefinition) definitions.iterator().next();
			}

			List<String> implementationClassNames = new ArrayList<String>();
			for (BeanDefinition bean : definitions) {
				implementationClassNames.add(bean.getBeanClassName());
			}

			throw new IllegalStateException(String.format(
					"Ambiguous custom implementations detected! Found %s but expected a single implementation!",
					StringUtils.collectionToCommaDelimitedString(implementationClassNames)));
		}
	  
	  
	  
	  
	  
	  public String getImplementationClassName(String parentBeanName) {
			return ClassUtils.getShortName(parentBeanName) + implPostfix;
		}
		/* 
		 * (non-Javadoc)
		 * @see org.springframework.data.repository.config.RepositoryConfiguration#getImplementationBeanName()
		 */
		public String getImplementationBeanName(String parentBeanName) {
			return StringUtils.uncapitalize(getImplementationClassName(parentBeanName));
		}
		

		
		
	  /**
	   * Calls the parent search that will search and register all the candidates.
	   * Then the registered objects are post processed to set them as
	   * MapperFactoryBeans
	   */
	  public Set<BeanDefinitionHolder> doScan(String... basePackages) {
		  
		    Set<BeanDefinitionHolder> beanDefinitions = superDoScan(basePackages);

		    if (beanDefinitions.isEmpty()) {
		      logger.warn("No MyBatis mapper was found in '" + Arrays.toString(basePackages) + "' package. Please check your configuration.");
		    } else {
		      for (BeanDefinitionHolder holder : beanDefinitions) {
		    	  
		        GenericBeanDefinition definition = (GenericBeanDefinition) holder.getBeanDefinition();

		        if (logger.isDebugEnabled()) {
		          logger.debug("Creating MapperFactoryBean with name '" + holder.getBeanName() 
		              + "' and '" + definition.getBeanClassName() + "' mapperInterface");
		        }

		        // the mapper interface is the original class of the bean
		        // but, the actual class of the bean is MapperFactoryBean
		        definition.getPropertyValues().add("mapperInterface", definition.getBeanClassName());
		        
		        definition.setBeanClass(MapperFactoryBean.class);
		        
		        definition.getPropertyValues().add("addToConfig", this.addToConfig);

		        boolean explicitFactoryUsed = false;
		        if (StringUtils.hasText(this.sqlSessionFactoryBeanName)) {
		          definition.getPropertyValues().add("sqlSessionFactory", new RuntimeBeanReference(this.sqlSessionFactoryBeanName));
		          explicitFactoryUsed = true;
		        } else if (this.sqlSessionFactory != null) {
		          definition.getPropertyValues().add("sqlSessionFactory", this.sqlSessionFactory);
		          explicitFactoryUsed = true;
		        }

		        if (StringUtils.hasText(this.sqlSessionTemplateBeanName)) {
		          if (explicitFactoryUsed) {
		            logger.warn("Cannot use both: sqlSessionTemplate and sqlSessionFactory together. sqlSessionFactory is ignored.");
		          }
		          definition.getPropertyValues().add("sqlSessionTemplate", new RuntimeBeanReference(this.sqlSessionTemplateBeanName));
		          explicitFactoryUsed = true;
		        } else if (this.sqlSessionTemplate != null) {
		          if (explicitFactoryUsed) {
		            logger.warn("Cannot use both: sqlSessionTemplate and sqlSessionFactory together. sqlSessionFactory is ignored.");
		          }
		          definition.getPropertyValues().add("sqlSessionTemplate", this.sqlSessionTemplate);
		          explicitFactoryUsed = true;
		        }

		        if (!explicitFactoryUsed) {
		          if (logger.isDebugEnabled()) {
		            logger.debug("Enabling autowire by type for MapperFactoryBean with name '" + holder.getBeanName() + "'.");
		          }
		          definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
		        }
		      }
		    }

		    return beanDefinitions;
		  
		    
		    
	  }
	  
		static BeanDefinitionHolder applyScopedProxyMode(
				ScopeMetadata metadata, BeanDefinitionHolder definition, BeanDefinitionRegistry registry) {

			ScopedProxyMode scopedProxyMode = metadata.getScopedProxyMode();
			if (scopedProxyMode.equals(ScopedProxyMode.NO)) {
				return definition;
			}
			boolean proxyTargetClass = scopedProxyMode.equals(ScopedProxyMode.TARGET_CLASS);
			return ScopedProxyCreator.createScopedProxy(definition, registry, proxyTargetClass);
		}
		
		static class ScopedProxyCreator {

			public static BeanDefinitionHolder createScopedProxy(
					BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry, boolean proxyTargetClass) {

				return ScopedProxyUtils.createScopedProxy(definitionHolder, registry, proxyTargetClass);
			}

			public static String getTargetBeanName(String originalBeanName) {
				return ScopedProxyUtils.getTargetBeanName(originalBeanName);
			}

		}
	  
//	  public void registerDelegate(BeanDefinitionHolder holder){
//
//	        GenericBeanDefinition definitionSrc = (GenericBeanDefinition) holder.getBeanDefinition();
//	        
//	        GenericBeanDefinition definition =(GenericBeanDefinition)definitionSrc.clone();
//
//	        if (logger.isDebugEnabled()) {
//	          logger.debug("Creating MapperFactoryBean with name '" + holder.getBeanName() 
//	              + "' and '" + definition.getBeanClassName() + "' mapperInterface");
//	        }
//
//	        // the mapper interface is the original class of the bean
//	        // but, the actual class of the bean is MapperFactoryBean
//	        definition.getPropertyValues().add("mapperInterface", definition.getBeanClassName());
//	        definition.setBeanClass(MapperFactoryBean.class);
//
//	        definition.getPropertyValues().add("addToConfig", this.addToConfig);
//
//	        boolean explicitFactoryUsed = false;
//	        if (StringUtils.hasText(this.sqlSessionFactoryBeanName)) {
//	          definition.getPropertyValues().add("sqlSessionFactory", new RuntimeBeanReference(this.sqlSessionFactoryBeanName));
//	          explicitFactoryUsed = true;
//	        } else if (this.sqlSessionFactory != null) {
//	          definition.getPropertyValues().add("sqlSessionFactory", this.sqlSessionFactory);
//	          explicitFactoryUsed = true;
//	        }
//
//	        if (StringUtils.hasText(this.sqlSessionTemplateBeanName)) {
//	          if (explicitFactoryUsed) {
//	            logger.warn("Cannot use both: sqlSessionTemplate and sqlSessionFactory together. sqlSessionFactory is ignored.");
//	          }
//	          definition.getPropertyValues().add("sqlSessionTemplate", new RuntimeBeanReference(this.sqlSessionTemplateBeanName));
//	          explicitFactoryUsed = true;
//	        } else if (this.sqlSessionTemplate != null) {
//	          if (explicitFactoryUsed) {
//	            logger.warn("Cannot use both: sqlSessionTemplate and sqlSessionFactory together. sqlSessionFactory is ignored.");
//	          }
//	          definition.getPropertyValues().add("sqlSessionTemplate", this.sqlSessionTemplate);
//	          explicitFactoryUsed = true;
//	        }
//
//	        if (!explicitFactoryUsed) {
//	          if (logger.isDebugEnabled()) {
//	            logger.debug("Enabling autowire by type for MapperFactoryBean with name '" + holder.getBeanName() + "'.");
//	          }
//	          definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);
//	        }
//	      
//	  }

	  /**
	   * {@inheritDoc}
	   */
	  @Override
	  protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
	    return (beanDefinition.getMetadata().isInterface() && beanDefinition.getMetadata().isIndependent());
	  }

	  /**
	   * {@inheritDoc}
	   */
	  @Override
	  protected boolean checkCandidate(String beanName, BeanDefinition beanDefinition) throws IllegalStateException {
	    if (super.checkCandidate(beanName, beanDefinition)) {
	      return true;
	    } else {
	      logger.warn("Skipping MapperFactoryBean with name '" + beanName 
	          + "' and '" + beanDefinition.getBeanClassName() + "' mapperInterface"
	          + ". Bean already defined with the same name!");
	      return false;
	    }
	  }

	public String getImplPostfix() {
		return implPostfix;
	}

	public void setImplPostfix(String implPostfix) {
		this.implPostfix = implPostfix;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	
}
