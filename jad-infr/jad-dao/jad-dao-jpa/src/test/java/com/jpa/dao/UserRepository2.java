package com.jpa.dao;

import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jpa.entity.User;
import com.jpa.vo.UserVo;

@JadDao
public interface UserRepository2 extends JadEntityDao<User, Integer>{
	
	User findUserByNameAndPassword(String name, String password);
//	User findUserByAdddddd(String name, String password);
}

