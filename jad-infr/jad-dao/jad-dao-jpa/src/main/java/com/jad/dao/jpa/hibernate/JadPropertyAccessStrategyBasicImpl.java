package com.jad.dao.jpa.hibernate;

import org.hibernate.property.access.spi.PropertyAccess;
import org.hibernate.property.access.spi.PropertyAccessStrategy;

public class JadPropertyAccessStrategyBasicImpl implements PropertyAccessStrategy{
	/**
	 * Singleton access
	 */
	public static final JadPropertyAccessStrategyBasicImpl INSTANCE = new JadPropertyAccessStrategyBasicImpl();

	@Override
	public PropertyAccess buildPropertyAccess(Class containerJavaType, final String propertyName) {
		return new JadPropertyAccessBasicImpl( this, containerJavaType, propertyName );
	}

}
