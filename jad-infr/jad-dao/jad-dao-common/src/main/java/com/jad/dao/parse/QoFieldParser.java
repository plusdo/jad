package com.jad.dao.parse;

import java.lang.reflect.Field;

import com.jad.commons.vo.QueryObject;
import com.jad.dao.entity.QoFieldInfo;


/**
 * Qo 字段解析
 * @author Administrator
 *
 */
public interface QoFieldParser {
	
	/**
	 * 字段解析
	 * @param entityFieldInfo
	 * @param field
	 */
	<QO extends QueryObject> boolean parseQoField(QoFieldInfo<QO> qoFieldInfo,Field field);
	


}
