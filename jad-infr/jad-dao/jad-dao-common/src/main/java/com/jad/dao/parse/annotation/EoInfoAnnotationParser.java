package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.dao.annotation.KeyGenStrategy;
import com.jad.dao.entity.EoFieldInfo;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.exception.JadEntityParseException;
import com.jad.dao.parse.EoInfoParser;
import com.jad.dao.utils.DaoReflectionUtil;

/**
 * 通过注解解析实体信息
 * @author hechuan
 *
 */
public class EoInfoAnnotationParser implements EoInfoParser{
	
	private static final Logger logger = LoggerFactory.getLogger(EoInfoAnnotationParser.class);
	
	/*
	 * 默认表主键
	 */
	private static final String DEFAULT_ID_NAME = "id";
	
	private ColumnParser columnParser=new ColumnParser();
	
	private ForceUpdateNullParser forceUpdateNullParser = new ForceUpdateNullParser();
	
	private IdParser idParser=new IdParser();
	
	private OrderByParser orderByParser=new OrderByParser();
	
	private TemporalParser temporalParser=new TemporalParser();
	
	private RelateColumnParser relateColumnParser=new RelateColumnParser();
	
	private ManyToOneParser manyToOneParser=new ManyToOneParser();
	
	@Override
	public <EO extends EntityObject> EoMetaInfo<EO> parseEo(Class<EO> clazz){
		
		if(clazz.getAnnotation(Entity.class)==null){
			throw new JadEntityParseException(String.format("实体类:%s没有被@Entity标注", clazz.getName()));
		}
		Table table = clazz.getAnnotation(Table.class);
		String tableName = null;
		if (table != null && StringUtils.isNotBlank(table.name())) {
			tableName = table.name();
		}
		if(tableName==null){
			tableName=clazz.getSimpleName();
			logger.warn(String.format("实体类:%s没有通过@Table注解指定表名,使用类名%s作为表名",clazz.getName(), tableName));
		}
		
		EoMetaInfo<EO> eoInfo=new EoMetaInfo<EO>(clazz);
		eoInfo.setTableName(tableName);
		
		KeyGenStrategy keyGenStrategy=clazz.getAnnotation(KeyGenStrategy.class);
		if(keyGenStrategy!=null){
			eoInfo.setIdType(keyGenStrategy.idType());
			eoInfo.setSequenceName(keyGenStrategy.sequenceName());
		}
		
		//实体字段
		Map<String,EoFieldInfo<EO>>fieldInfoMap=getEoFieldInfos(clazz, eoInfo);
		eoInfo.setFieldInfoMap(fieldInfoMap);
		
		
		//设置orderBy
		Iterator<EoFieldInfo<EO>> iterator = fieldInfoMap.values().iterator();
		boolean isFirst=true;
		StringBuffer sb=new StringBuffer();
		while (iterator.hasNext()) {
			EoFieldInfo<EO> fieldInfo = iterator.next();
			String orderBy=fieldInfo.getOrderBy();
			if(StringUtils.isNotBlank(orderBy)){
				if(isFirst){
					isFirst=false;
				}else{
					sb.append(",");
				}
				sb.append(orderBy);
			}
		}
		if(sb.length()>0){
			eoInfo.setOrderBy(sb.toString());
		}
		
		// 设置默认主键
		if(eoInfo.getKeyFieldInfo()==null){
			
			iterator = fieldInfoMap.values().iterator();
			while (iterator.hasNext()) {
				EoFieldInfo<EO> fieldInfo = iterator.next();
				String column = fieldInfo.getColumn();
				// 如果没有主键,数据库字段为ID则设置为默认主键
				if (DEFAULT_ID_NAME.equals(column)) {
					eoInfo.setKeyFieldInfo(fieldInfo);
                    break;
				}
			}
			if (eoInfo.getKeyFieldInfo()==null) {
				logger.warn(String.format("实体类:%s没有通过@Id注解指定主键", clazz.getSimpleName()));
			}
		}
		
		return eoInfo;
		
	}
	
	/**
	 * 获取实体类所有字段
	 * 
	 * @param clazz
	 * @param eoInfo
	 */
	private <EO extends EntityObject> Map<String,EoFieldInfo<EO>> getEoFieldInfos(Class<EO> clazz, EoMetaInfo<EO> eoInfo) {
		
		Map<String,EoFieldInfo<EO>> fieldInfoMap= new TreeMap<String,EoFieldInfo<EO>>();
		
		Set<Field> fields = DaoReflectionUtil.getClassFields(clazz);//获得所有属性
		for (Field field : fields) {
			
			EoFieldInfo<EO> eoFieldInfo = new EoFieldInfo<EO>(eoInfo,field);
			
			boolean parseColumnRes=columnParser.parseEoField(eoFieldInfo, field);
			boolean parseIdRes=idParser.parseEoField(eoFieldInfo, field);
			
			forceUpdateNullParser.parseEoField(eoFieldInfo, field);//20170617
			
			if(!parseColumnRes && !parseIdRes 
					&& field.getType().getAnnotation(Entity.class)!=null 
					&& 	manyToOneParser.parseEoField(eoFieldInfo, field) ){
				
				relateColumnParser.parseEoField(eoFieldInfo, field);
				
			}
			
			temporalParser.parseEoField(eoFieldInfo, field);
			orderByParser.parseEoField(eoFieldInfo, field);
			
			//主键
			if(eoFieldInfo.isId()){
				String fieldName=field.getName();
				
				if(parseColumnRes){
				}else{
					eoFieldInfo.setColumn(fieldName);
					eoFieldInfo.setFieldName(fieldName);
				}
				eoInfo.setKeyFieldInfo(eoFieldInfo);
			}
			fieldInfoMap.put(field.getName(), eoFieldInfo);
		}
		
		return fieldInfoMap;
	}
	
	

	
	


}
