package com.jad.dao.entity;

import com.jad.commons.vo.QueryObject;

public class QoFieldVal<QO extends QueryObject> {
	
	private QoFieldInfo<QO> info;
	
	private Object value;

	public QoFieldVal(QoFieldInfo<QO> info, Object value) {
		super();
		this.info = info;
		this.value = value;
	}

	public QoFieldInfo<QO> getInfo() {
		return info;
	}

	public void setInfo(QoFieldInfo<QO> info) {
		this.info = info;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}


}
