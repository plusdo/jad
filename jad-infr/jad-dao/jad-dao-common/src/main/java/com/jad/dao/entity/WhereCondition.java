package com.jad.dao.entity;

import com.jad.commons.enums.QueryOperateType;
import com.jad.commons.vo.EntityObject;


/**
 * 全部用 and 连接起来的where条件
 *
 */
public class WhereCondition<E extends EntityObject> {
	
	/**
	 * 条件
	 */
	private String conditionKey;
	
	/**
	 * 字段信息
	 */
	private EoFieldInfo<E> fieldInfo;
	
	/**
	 * 实体信息
	 */
	private EoMetaInfo<E> ei;
	
	/**
	 * 操作类型
	 */
	private QueryOperateType operateType;
	
	/**
	 * 是否有效
	 */
	private boolean isValid=true;
	
	
	
	public WhereCondition(EoMetaInfo<E> ei,EoFieldInfo<E> fieldInfo){
		this.ei=ei;
		this.fieldInfo=fieldInfo;
	}

	public String getConditionKey() {
		return conditionKey;
	}

	public void setConditionKey(String conditionKey) {
		this.conditionKey = conditionKey;
	}

	public EoFieldInfo<E> getFieldInfo() {
		return fieldInfo;
	}

	public void setFieldInfo(EoFieldInfo<E> fieldInfo) {
		this.fieldInfo = fieldInfo;
	}

	public QueryOperateType getOperateType() {
		return operateType;
	}

	public void setOperateType(QueryOperateType operateType) {
		this.operateType = operateType;
	}

	public EoMetaInfo<E> getEi() {
		return ei;
	}

	public void setEi(EoMetaInfo<E> ei) {
		this.ei = ei;
	}

	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	
	


}
