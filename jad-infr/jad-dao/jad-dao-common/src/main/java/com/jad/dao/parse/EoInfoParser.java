package com.jad.dao.parse;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoMetaInfo;

/**
 * 实体信息解析
 * @author hechuan
 *	
 */
public interface EoInfoParser {


	public <EO extends EntityObject> EoMetaInfo<EO> parseEo(Class<EO> clazz);
	
	
	
	
}
