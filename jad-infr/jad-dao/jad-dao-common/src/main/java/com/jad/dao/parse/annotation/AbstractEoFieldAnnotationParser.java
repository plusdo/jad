package com.jad.dao.parse.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoFieldInfo;
import com.jad.dao.parse.EoFieldParser;

public abstract class AbstractEoFieldAnnotationParser<A extends Annotation> implements EoFieldParser{

	private Class<A> annotationClass;
	
	@SuppressWarnings("unchecked")
	public AbstractEoFieldAnnotationParser(){
		this.annotationClass=ReflectionUtils.getSuperClassGenricType(this.getClass(),0);
	}
	
	public abstract <EO extends EntityObject> boolean parseAnnotationEoField(EoFieldInfo<EO>eoFieldInfo, Field field,A annotation);
	
	
	@Override
	public <EO extends EntityObject> boolean parseEoField(EoFieldInfo<EO> eoFieldInfo,Field field) {

		Method getterMethod=ReflectionUtils.findGetterMethod(
				eoFieldInfo.getEoInfo().getMetaClass(),field.getName());
		
		boolean parseRes=parseEoField(eoFieldInfo,field,getterMethod);//先解析getter方法上的注解
		
		if(!parseRes){//getter方法上没有注解，再从字段上解析
			
			A annotation=field.getAnnotation(annotationClass);
			
			if(annotation!=null){
				
				parseRes = parseAnnotationEoField(eoFieldInfo,field,annotation);
				
			}
		}
		
		return parseRes;
	}
	
	private <EO extends EntityObject> boolean parseEoField(EoFieldInfo<EO> eoFieldInfo, Field field, Method getterMethod) {
		
		A annotation=getterMethod.getAnnotation(annotationClass);
		
		if(annotation!=null){
			
			return parseAnnotationEoField(eoFieldInfo,field,annotation);
			
		}else{
			
			return false;
			
		}
	}

	
	


}
