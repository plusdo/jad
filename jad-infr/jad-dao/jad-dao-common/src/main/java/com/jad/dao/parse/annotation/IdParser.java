package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;

import javax.persistence.Id;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoFieldInfo;

/**
 * 解析Id注解
 * @author hechuan
 *
 */
public class IdParser extends AbstractEoFieldAnnotationParser<Id>{
	

	@Override
	public <EO extends EntityObject> boolean parseAnnotationEoField(
			EoFieldInfo<EO> eoFieldInfo, Field field, Id annotation) {
		eoFieldInfo.setColumn(true);
		eoFieldInfo.setId(true);
		return true;
	}

}
