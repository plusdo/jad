package com.jad.dao.entity;

import java.util.Map;

import com.jad.commons.vo.ValueObject;

public class VoMetaInfo <V extends ValueObject> extends ObjectMetaInfo<V>{

	public VoMetaInfo(Class<V> metaClass) {
		super(metaClass);
	}
	
	/**
	 * 字段信息
	 * key:field name,val:VoFieldInfo
	 */
	private Map<String,VoFieldInfo<V>>fieldInfoMap;
//	private Set<VoFieldInfo<V>> fieldInfos;
	
	public Map<String, VoFieldInfo<V>> getFieldInfoMap() {
		return fieldInfoMap;
	}

	public void setFieldInfoMap(Map<String, VoFieldInfo<V>> fieldInfoMap) {
		this.fieldInfoMap = fieldInfoMap;
	}



	

}
