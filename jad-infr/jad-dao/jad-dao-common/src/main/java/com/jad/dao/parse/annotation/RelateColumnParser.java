package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.entity.EoFieldInfo;

public class RelateColumnParser extends AbstractEoFieldAnnotationParser<RelateColumn>{


	@Override
	public <EO extends EntityObject> boolean parseAnnotationEoField(
			EoFieldInfo<EO> eoFieldInfo, Field field, RelateColumn relateColumn) {

		eoFieldInfo.setColumn(true);
		
		if(EntityObject.class.isAssignableFrom(field.getType())){
			eoFieldInfo.setRelateColumn(true);
		}
		
		eoFieldInfo.setRelateAlias(relateColumn.alia());
		
		eoFieldInfo.setRelateLoadMethod(relateColumn.loadMethod());
		
		eoFieldInfo.setFieldName(field.getName());
		
		eoFieldInfo.setFieldType(field.getType());
		
		eoFieldInfo.setColumn(relateColumn.name());
		
		eoFieldInfo.setNullable(relateColumn.nullable());
		
		eoFieldInfo.setInsertable(relateColumn.insertable());
		
		eoFieldInfo.setUpdatable(relateColumn.updatable());
		
		eoFieldInfo.setLength(relateColumn.length());
		
		eoFieldInfo.setPrecision(relateColumn.precision());
		
		eoFieldInfo.setScale(relateColumn.scale());
		
		return true;
	}


}


