package com.jad.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.vo.QueryObject;

@NoRepositoryBean
public interface JadEntityDao<EO extends EntityObject, ID extends Serializable> extends Repository<EO,ID>, JadRootDao<EO,ID>{
	
	Class<EO>getEntityClass();

	EO add(EO entity);
	
	int updateById(EO entity,ID id);
	
	int updateId(ID newId,ID oldId);
	
//	int updateByMap(EO entity,Map<String,?>conditionParams);
	
	<QO extends QueryObject>int updateByQo(EO entity,QO qo);//
	
	int updateByIdList(EO entity,List<ID> idList);
	
	int deleteById(ID id);
	
	int deleteByIdList(List<ID> idList);
	
//	int deleteByMap(Map<String,?>params);
	
	<QO extends QueryObject>int deleteByQo(QO qo);
	
	EO findById(ID id);
	
	List<EO> findByIdList(List<ID> idList);
	List<EO> findByIdList(List<ID> idList,String orderBy);
	
//	List<EO> findByMap(Map<String,?>conditionParams);
	
	<QO extends QueryObject>List<EO> findByQo(QO qo);
	
//	List<EO> findByMap(Map<String,?>conditionParams,String orderBy);
	
	<QO extends QueryObject>List<EO> findByQo(QO qo,String orderBy);
	
	<QO extends QueryObject> Long findCountByQo(QO qo);
	
//	Page<EO> findPageByMap(Page<?> page,Map<String,?>conditionParams);
	
	<QO extends QueryObject>Page<EO> findPageByQo(PageQo page,QO qo);
	
//	Page<EO> findPageByMap(Page<?> page,Map<String,?>conditionParams,String orderBy);
	
	<QO extends QueryObject>Page<EO> findPageByQo(PageQo page,QO qo,String orderBy);
	
	List<EO> findBySql(String sql,List<?>params);
	
	Page<EO> findPageBySql(PageQo page,String sql,List<?>params);
	

}



