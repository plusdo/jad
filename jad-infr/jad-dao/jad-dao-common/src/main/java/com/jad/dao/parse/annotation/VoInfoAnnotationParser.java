package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.jad.commons.vo.ValueObject;
import com.jad.dao.entity.VoFieldInfo;
import com.jad.dao.entity.VoMetaInfo;
import com.jad.dao.parse.VoInfoParser;
import com.jad.dao.utils.DaoReflectionUtil;

public class VoInfoAnnotationParser implements VoInfoParser{
	
	@Override
	public <VO extends ValueObject> VoMetaInfo<VO> parseVo(Class<VO> clazz) {
		
		VoMetaInfo<VO> voInfo=new VoMetaInfo<VO>(clazz);
		
		voInfo.setFieldInfoMap(getVoFieldInfos(clazz, voInfo));
		
		return voInfo;
	}
	
	
	/**
	 * 获取实体类所有字段
	 * 
	 * @param clazz
	 * @param eoInfo
	 */
	private <VO extends ValueObject> Map<String,VoFieldInfo<VO>> getVoFieldInfos(Class<VO> clazz, VoMetaInfo<VO> voInfo) {
		
		Map<String,VoFieldInfo<VO>> fieldInfoMap= new TreeMap<String,VoFieldInfo<VO>>();
		
		Set<Field> fields = DaoReflectionUtil.getClassFields(clazz);//获得所有属性
		
		for (Field field : fields) {
			VoFieldInfo<VO> voFieldInfo = new VoFieldInfo<VO>(voInfo,field);
			fieldInfoMap.put(field.getName(), voFieldInfo);
		}
		return fieldInfoMap;
	}
	
	

}
