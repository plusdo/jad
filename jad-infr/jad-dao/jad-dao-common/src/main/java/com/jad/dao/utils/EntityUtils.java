package com.jad.dao.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.jad.commons.enums.QueryOperateType;
import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PojoObject;
import com.jad.commons.vo.QueryObject;
import com.jad.commons.vo.ValueObject;
import com.jad.dao.entity.EoFieldInfo;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.QoMetaInfo;
import com.jad.dao.entity.VoFieldInfo;
import com.jad.dao.entity.VoMetaInfo;
import com.jad.dao.entity.WhereCondition;
import com.jad.dao.exception.JadEntityParseException;
import com.jad.dao.parse.annotation.EoInfoAnnotationParser;
import com.jad.dao.parse.annotation.QoInfoAnnotationParser;
import com.jad.dao.parse.annotation.VoInfoAnnotationParser;

public class EntityUtils {
	
	
	public static final String DOT=".";
	public static final String DOT_REGEX="\\.";
	
	public static final int MAX_RELATE_DEPTH=2;
	
	/**
	 * 默认表主键
	 */
	public static final String DEFAULT_ID_NAME = "id";
	
	/**
	 * 缓存实体信息
	 * key:classname,val:EoMetaInfo
	 */
	@SuppressWarnings("rawtypes")
	private static final Map<String, EoMetaInfo> EO_INFO_CACHE = new ConcurrentHashMap<String, EoMetaInfo>();
	
	/**
	 * 缓存Vo信息
	 * key:classname,val:VoMetaInfo
	 */
	@SuppressWarnings("rawtypes")
	private static final Map<String, VoMetaInfo> VO_INFO_CACHE = new ConcurrentHashMap<String, VoMetaInfo>();
	
	/**
	 * 缓存QO信息
	 * key:classname,val:QoMetaInfo
	 */
	@SuppressWarnings("rawtypes")
	private static final Map<String, QoMetaInfo> QO_INFO_CACHE = new ConcurrentHashMap<String, QoMetaInfo>();
	
	
	/**
	 * <p>
	 * 获取实体映射信息
	 * <p>
	 *
	 * @param clazz
	 *            反射实体类
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <E extends EntityObject> EoMetaInfo<E> getEoInfo(Class<E> clazz) {
		EoMetaInfo<E> ei=EO_INFO_CACHE.get(clazz.getName());
		if(ei==null){
			ei=initEoInfo(clazz);
		}
		return ei;
	}
	
	
	
	/**
	 * <p>
	 * 实体类反射获取表信息【初始化】
	 * <p>
	 *
	 * @param clazz
	 *            反射实体类
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <E extends EntityObject> EoMetaInfo<E> initEoInfo(Class<E> clazz) {
		EoMetaInfo<E> entityInfo=null;
		synchronized(clazz){
			EoMetaInfo<E> ei=EO_INFO_CACHE.get(clazz.getName());
			if(ei!=null){
				return ei;
			}
			EoInfoAnnotationParser parser=new EoInfoAnnotationParser();
			entityInfo=parser.parseEo(clazz);
			EO_INFO_CACHE.put(clazz.getName(), entityInfo);
		}
		if(entityInfo!=null){
			validateRelateEntity(entityInfo);
		}
		return entityInfo;
	}
	
	
	
	/**
	 * <p>
	 * 获取Vo映射信息
	 * <p>
	 *
	 * @param clazz
	 *            反射实体类
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <V extends ValueObject> VoMetaInfo<V> getVoInfo(Class<V> clazz) {
		VoMetaInfo<V> ei=VO_INFO_CACHE.get(clazz.getName());
		if(ei==null){
			ei=initVoInfo(clazz);
		}
		return ei;
	}
	
	/**
	 * <p>
	 * 实体类反射获取表信息【初始化】
	 * <p>
	 *
	 * @param clazz
	 *            反射实体类
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <V extends ValueObject> VoMetaInfo<V> initVoInfo(Class<V> clazz) {
		
		synchronized(clazz){
			VoMetaInfo<V> ei=VO_INFO_CACHE.get(clazz.getName());
			if(ei!=null){
				return ei;
			}
			VoInfoAnnotationParser parser=new VoInfoAnnotationParser();
			VoMetaInfo<V> entityInfo=parser.parseVo(clazz);
			VO_INFO_CACHE.put(clazz.getName(), entityInfo);
			return entityInfo;
		}
		
	}
	
	
	/**
	 * <p>
	 * 获取QO映射信息
	 * <p>
	 *
	 * @param clazz
	 *            反射实体类
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <QO extends QueryObject> QoMetaInfo<QO> getQoInfo(Class<QO> clazz) {
		QoMetaInfo<QO> ei=QO_INFO_CACHE.get(clazz.getName());
		if(ei==null){
			ei=initQoInfo(clazz);
		}
		return ei;
	}
	
	/**
	 * <p>
	 * 实体类反射获取表信息【初始化】
	 * <p>
	 *
	 * @param clazz
	 *            反射实体类
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <QO extends QueryObject> QoMetaInfo<QO> initQoInfo(Class<QO> clazz) {
		
		synchronized(clazz){
			QoMetaInfo<QO> ei=QO_INFO_CACHE.get(clazz.getName());
			if(ei!=null){
				return ei;
			}
			QoInfoAnnotationParser parser=new QoInfoAnnotationParser();
			QoMetaInfo<QO> qoInfo=parser.parseQo(clazz);
			QO_INFO_CACHE.put(clazz.getName(), qoInfo);
			return qoInfo;
		}
		
	}
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static <E extends EntityObject> void validateRelateEntity(EoMetaInfo<E> entityInfo){
		
		for(EoFieldInfo<E> fieldInfo:entityInfo.getFieldInfoMap().values()){
			if(!fieldInfo.isRelateColumn()){
				continue;
			}
			EoMetaInfo rEi=getEoInfo(fieldInfo.getFieldType());
			if(rEi==null || rEi.getKeyFieldInfo()==null ){
				throw new JadEntityParseException(String.format("实体%s关联的属性%s必须为实体，而且存在主键", 
						entityInfo.getObjName(),fieldInfo.getFieldName()));
			}
			
		}
		
	}
	
	public static <E extends EntityObject> WhereCondition<E> findWhereCondition(EoMetaInfo<E> ei,String key,QueryOperateType specialType){
		
		Collection<EoFieldInfo<E>>fieldInfos=ei.getFieldInfoMap().values();
		
		boolean isRelateKey=isRelateProperty(key,MAX_RELATE_DEPTH);
		
		for(EoFieldInfo<E> fi:fieldInfos){
			if(!fi.isColumn()){
				continue;
			}
			if(fi.isRelateColumn()  && (!PojoObject.class.isAssignableFrom(fi.getFieldType())) ){
				continue;
			}
			String property=fi.getFieldName();
			if(property.equals(key)){
				WhereCondition<E> wc=new WhereCondition<E>(ei,fi);
				wc.setConditionKey(key);
				if(specialType!=null){
					wc.setOperateType(specialType);
				}else{
					wc.setOperateType(QueryOperateType.eq);
				}
				return wc;
			}
			
			if(isRelateKey && fi.isRelateColumn()){
				String relateName=getRelateEntityName(key);
				if(property.equals(relateName)){
					WhereCondition<E> wc=new WhereCondition<E>(ei,fi);
					wc.setConditionKey(key);
					if(specialType!=null){
						wc.setOperateType(specialType);
					}else{
						wc.setOperateType(QueryOperateType.eq);
					}
					return wc;
				}
			}
			
		}
		
		if(isRelateKey){
			WhereCondition<E> rwc=new WhereCondition<E>(ei,null);
			rwc.setConditionKey(key);
			rwc.setValid(false);//无效
			return rwc;
		}
		
		WhereCondition<E> wc=match(ei,fieldInfos,key,QueryOperateType.eq,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.gt,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.ge,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.lt,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.le,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.notLike,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.like,specialType);
		if(wc!=null){
			return wc;
		}
		
		
		wc=match(ei,fieldInfos,key,QueryOperateType.notLikeLeft,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.likeLeft,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.notLikeRight,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.likeRight,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.isNull,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.isNotNull,specialType);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.notIn,null);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.in,null);
		if(wc!=null){
			return wc;
		}
		
		wc=match(ei,fieldInfos,key,QueryOperateType.between,null);
		if(wc!=null){
			return wc;
			
		}else{
			wc=new WhereCondition<E>(ei,null);
			wc.setConditionKey(key);
			wc.setValid(false);//无效
//			logger.warn("在实体:"+ei.getObjName()+"中无法匹配到key为"+key+"的相应的条件");
			return wc;
		}
		
	}
	
	public static <E extends EntityObject> WhereCondition<E> match(
			EoMetaInfo<E> ei,Collection<EoFieldInfo<E>>fieldInfos,String key,QueryOperateType defType,QueryOperateType specialType){
		
		if(key.endsWith(defType.getSuffix()) ){
			String matchPro=key.substring(0,key.length()-defType.getSuffix().length());
			
			for(EoFieldInfo<E> fi:fieldInfos){
				if(!fi.isColumn()){
					continue;
				}
				if(fi.getFieldName().equals(matchPro)){
					WhereCondition<E> wc=new WhereCondition<E>(ei,fi);
					wc.setConditionKey(key);
					
					if(specialType!=null){
						wc.setOperateType(specialType);
					}else{
						wc.setOperateType(defType);
					}
					return wc;
				}
				
			}
			
			
		}
		return null;
	}
	
	
	public static <E extends EntityObject>WhereCondition<E> getWhereCondition(EoMetaInfo<E> ei,String key,QueryOperateType specialType){

		
		
		String cacheKey=key;
		if(specialType!=null){
			cacheKey=cacheKey+"_"+specialType.getType();
		}
		
		WhereCondition<E> wc=ei.getWhereConditionMap().get(cacheKey);
		
		if(wc!=null){

			return wc;
			
		}else{
			wc=findWhereCondition(ei,key,specialType);
			if(wc!=null){
				ei.getWhereConditionMap().put(cacheKey, wc);//缓存起来
			}else{
//				logger.warn("在实体:"+ei.getObjName()+"中无法匹配到key为"+key+"的相应的条件");
			}
			return wc;
		}
		
	
		
	}
	
//	private static <E extends EntityObject>WhereCondition<E> getWhereCondition(EoMetaInfo<E> ei,String key){
//		
//		return getWhereCondition(ei,key,null);
//		
//	}
	
	
	

	
	/**
	 * 跟据属性名称判断是否为关联属性
	 * @param propertyName
	 * @return
	 */
	public static boolean isRelateProperty(String propertyName,int maxDepth){
//		TODO 待完善
		return propertyName.contains(DOT) && propertyName.split(DOT_REGEX).length <= maxDepth;//目前只支持关联一级
	}
	
	public static boolean isRelateProperty(String propertyName){
//		TODO 待完善
		return propertyName.contains(DOT);
		
	}
	
	/**
	 * 获得关联属性名
	 * @param propertyName
	 * @return
	 */
	public static String getRelateEntityName(String propertyName){
//		TODO 待完善
		if(!isRelateProperty(propertyName)){
			return propertyName;
		}else{
			return propertyName.split(DOT_REGEX)[0];
		}
	}
	
	/**
	 * 
	 */
	public static String getRelatePropertyName(String propertyName){
//		TODO 待完善
		if(!isRelateProperty(propertyName)){
			return propertyName;
		}else{
//			return propertyName.split(DOT_REGEX)[1];
			return propertyName.substring(propertyName.indexOf(DOT)+1);
		}
	}
	
	
	/**
	 * 将对像转换成map
	 * @return
	 */
//	public static <QO extends QueryObject> Map<String,Object>copyQoToMap(QO qo){
//		return copyQoToMap(null,qo);
//	}
	
	/**
	 * 将对像转换成map
	 * 
	 * 这个方面还有问题，待完善
	 * 
	 * @return
	 */
//	public static <QO extends QueryObject> Map<String,Object>copyQoToMap(String keyPrefix,QO qo){
//
////		TODO 待完善
//		
//		Map<String,Object> map=new LinkedHashMap<String,Object>();
//		if(qo==null){
//			return new LinkedHashMap<String,Object>();
//		}
//		
//		QoMetaInfo<QO> qoMetaInfo=EntityUtils.getQoInfo((Class<QO>)qo.getClass());
//		
//		
//		for(Map.Entry<String, QoFieldInfo<QO>>ent:qoMetaInfo.getFieldInfoMap().entrySet()){
//			
//			Field field=ent.getValue().getField();
//			
//			String fieldName=field.getName();
//			
//			Object val=ReflectionUtils.getFieldValue(qo, fieldName);
//			
//			if(val==null){
//				continue;
//			}
//			
//			Class<?> type = field.getType();
//			
//			if(!JavaType.Object.getType().equals(JavaType.getJavaType(type.getName()))){
//					
//				if(StringUtils.isNotBlank(keyPrefix)){
//					map.put(keyPrefix+"."+fieldName, val);
//				}else{
//					map.put(fieldName, val);
//				}
//				
//			}else if(QueryObject.class.isAssignableFrom(type)){
//				
//				map.putAll(copyQoToMap(fieldName,(QueryObject)val));
//			}
//			
//		}
//		return map;
//	
//	}
	
	
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <EO extends EntityObject,VO extends ValueObject> VO copyToVo(EO eo,Class<VO>voClass){
		
		if(eo==null){
			return null;
		}
		
		EoMetaInfo<EO> eoMetaInfo=getEoInfo((Class<EO>)eo.getClass());
		VoMetaInfo<VO> voMetaInfo=getVoInfo(voClass);
		
		try {
			VO vo=voClass.newInstance();
			
//			BeanUtils.copyProperties( vo,eo);//复制普通属性
			
			for(Map.Entry<String, EoFieldInfo<EO>>ent:eoMetaInfo.getFieldInfoMap().entrySet()){
				
				String fieldName=ent.getKey();
				EoFieldInfo eoi=ent.getValue();
				
				Object obj=ReflectionUtils.getFieldValue(eo, fieldName);
				if(obj==null){
					continue;
				}
				
				VoFieldInfo voi=voMetaInfo.getFieldInfoMap().get(fieldName);
				if(voi==null){
					continue;
				}
				
				if(!eoi.isRelateColumn()){
					ReflectionUtils.setFieldValue(vo,fieldName,obj);
				}else{
					ReflectionUtils.setFieldValue(vo, fieldName,copyToVo((EO)obj,voi.getFieldType() ) );//递归复制关联属性	
				}
				
				
				
			}
			return vo;
		} catch (Exception e) {
			throw new JadEntityParseException(String.format("属性复制失败,eoMetaInfo:%s,voMetaInfo:%s"
					, eoMetaInfo.getObjName(),voMetaInfo.getObjName()),e);
		}
		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <EO extends EntityObject,VO extends ValueObject> EO copyToEo(VO vo,Class<EO> eoClass){
		
		if(vo==null){
			return null;
		}
		EoMetaInfo<EO> eoMetaInfo=getEoInfo(eoClass);
		VoMetaInfo<VO> voMetaInfo=getVoInfo((Class<VO>)vo.getClass());
			
		try {
			EO eo=eoClass.newInstance();
			
//			BeanUtils.copyProperties( eo,vo);//复制普通属性
			
			//复制关联属性
			for(Map.Entry<String, EoFieldInfo<EO>>ent:eoMetaInfo.getFieldInfoMap().entrySet()){
				
				String fieldName=ent.getKey();
				EoFieldInfo eoi=ent.getValue();
				
//				if(!eoi.isRelateColumn()){//不是关联列
//					continue;
//				}
				
				VoFieldInfo voi=voMetaInfo.getFieldInfoMap().get(fieldName);
				if(voi==null){
					continue;
				}
				
				Object obj=ReflectionUtils.getFieldValue(vo, fieldName);
				if(obj==null){
					continue;
				}
				
				if(!eoi.isRelateColumn()){
					
					ReflectionUtils.setFieldValue(eo,fieldName,obj);
					
				}else{
					ReflectionUtils.setFieldValue(eo, fieldName, copyToEo((VO)obj,eoi.getFieldType() ));//递归复制关联属性并返射回去	
				}
				
				
				
			}
			return eo;
		} catch (Exception e) {
			throw new JadEntityParseException(String.format("属性复制失败,eoMetaInfo:%s,voMetaInfo:%s"
					, eoMetaInfo.getObjName(),voMetaInfo.getObjName()),e);
		}
		
	}
	
	
	public static <EO extends EntityObject,VO extends ValueObject> List<VO> copyToVoList(
			List<EO> eoList,Class<VO>voClass){
		
		List<VO>voList=new ArrayList<VO>();
		if(eoList==null || eoList.isEmpty()){
			return voList;
		}
		
		for(EO eo:eoList){
			voList.add(copyToVo(eo,voClass));
		}
		return voList;
	}
	
	public static <EO extends EntityObject,VO extends ValueObject> Page<VO> copyToVoPage(
			Page<EO> eoPage,Class<VO>voClass){
		
		
		Page<VO>voPage=new Page<VO>(eoPage.getPageNo(),eoPage.getPageSize(),eoPage.getCount());
		
		if(eoPage==null ||eoPage.getList()==null || eoPage.getList().isEmpty()){
			return voPage;
		}
		
		for(EO eo:eoPage.getList()){
			voPage.getList().add(copyToVo(eo,voClass));
		}
		return voPage;
	}
	
	
	
	
	

}
