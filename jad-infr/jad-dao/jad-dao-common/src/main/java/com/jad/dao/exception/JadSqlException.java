package com.jad.dao.exception;

public class JadSqlException extends RuntimeException {

	public JadSqlException() {
		// TODO Auto-generated constructor stub
	}

	public JadSqlException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JadSqlException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public JadSqlException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public JadSqlException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
