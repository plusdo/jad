package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;

import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.QueryObject;
import com.jad.dao.entity.QoFieldInfo;

public class CurdParser extends AbstractQoFieldAnnotationParser<CURD>{

	@Override
	public <QO extends QueryObject> boolean parseAnnotationQoField(
			QoFieldInfo<QO> qoFieldInfo, Field field, CURD curd) {
		
		qoFieldInfo.setCurd(curd);
		
		return true;
	}

}
