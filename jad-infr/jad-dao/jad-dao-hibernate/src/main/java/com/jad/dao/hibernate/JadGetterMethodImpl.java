package com.jad.dao.hibernate;

import java.lang.reflect.Method;

import org.hibernate.property.access.spi.GetterMethodImpl;

public class JadGetterMethodImpl extends GetterMethodImpl {
	
	/**
	 * 该getter方法的返回值类型
	 */
	private final Class<?> resultClass;
	

	public JadGetterMethodImpl(Class containerClass, String propertyName,
			Method getterMethod) {
		
		super(containerClass, propertyName, getterMethod);
		
		resultClass=getterMethod.getReturnType();
		
	}


	public Class<?> getResultClass() {
		return resultClass;
	}
	

}
