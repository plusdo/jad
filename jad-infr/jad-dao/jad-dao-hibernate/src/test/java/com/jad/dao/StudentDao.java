package com.jad.dao;

import com.jad.dao.annotation.JadDao;
import com.jad.entity.Student;

@JadDao
public interface StudentDao extends JadEntityDao<Student, String> {


}
