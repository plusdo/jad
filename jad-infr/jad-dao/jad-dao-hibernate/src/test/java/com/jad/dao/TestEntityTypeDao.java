package com.jad.dao;

import com.jad.dao.annotation.JadDao;
import com.jad.entity.TestEntityType;
import com.jad.vo.TestEntityTypeVo;

@JadDao
public interface TestEntityTypeDao extends JadEntityDao<TestEntityType, String>{


}
