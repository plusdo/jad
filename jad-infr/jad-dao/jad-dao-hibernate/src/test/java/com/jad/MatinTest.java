package com.jad;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.Test;

import com.jad.commons.vo.Page;
import com.jad.entity.User;
import com.jad.service.UserService;

public class MatinTest {
	
	@Test
    public void baseTest() throws Exception {
    	
    	ApplicationContext context = new ClassPathXmlApplicationContext("spring-hibernate.xml");
    	String[]names=context.getBeanDefinitionNames();
    	
         for(String name:names){
         	System.out.println("beanName:"+name);
         }
         
         Map<String,Object>conditionParams=new HashMap<String,Object>();
//         conditionParams.put("name", "jad");
//         conditionParams.put("password", "ssffd");
         
         UserService userService=(UserService)context.getBean("userService2Impl");
//         
//         User vo =userService.findById(1203);
//         UserVo vo = userService.selectOne(SelectWrapper.instance().eq("id", 1203));
//         UserVo vo = userService.get(1203);
         
//         List<User>userList=userService.findBy(conditionParams,"id");
         
         Page<User>page=new Page<User>(2,3);
         
         Page<Map<String,?>>resPage=userService.findPageMapBy(page, conditionParams, "id");
         
//         Page<User>resPage=userService.findPageBy(page, conditionParams, "id");
         
         List<Map<String,?>>list=resPage.getList();
         for(Map user:list){
//        	 System.out.println(JsonMapper.toJsonString(user));
         }
         
         System.out.println("totalSize:"+page.getCount()+",listSize:"+resPage.getList().size());
         
         
         
	}

}
