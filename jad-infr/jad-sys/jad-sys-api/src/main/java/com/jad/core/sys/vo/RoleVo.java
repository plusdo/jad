package com.jad.core.sys.vo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;

public class RoleVo extends BaseVo<String>{

	
	private static final long serialVersionUID = 1L;
	private OfficeVo office;	// 归属机构
	
	@CURD(label="角色名称")
	private String name; 	// 角色名称
	
	@CURD(label="英文名称")
	private String enname;	// 英文名称
	
	@CURD(label="权限类型")
	private String roleType;// 权限类型
	
	@CURD(label="数据范围")
	private String dataScope;// 数据范围
	
	@CURD(label="是否系统数据")
	private String sysData; 		//是否系统数据
	
	@CURD(label="是否可用")
	private String useable; 		//是否可用

	private List<MenuVo> menuList = null; // 拥有菜单列表
	private List<OfficeVo> officeList = null; // 按明细设置数据范围
	
	public RoleVo() {
		super();
	}
	
	public RoleVo(String id){
		super(id);
	}
	

	public String getUseable() {
		return useable;
	}

	public void setUseable(String useable) {
		this.useable = useable;
	}

	public String getSysData() {
		return sysData;
	}

	public void setSysData(String sysData) {
		this.sysData = sysData;
	}

	@JsonIgnore
	public OfficeVo getOffice() {
		return office;
	}

	public void setOffice(OfficeVo office) {
		this.office = office;
	}

	@Length(min=1, max=100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Length(min=1, max=100)
	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}
	
	@Length(min=1, max=100)
	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getDataScope() {
		return dataScope;
	}

	public void setDataScope(String dataScope) {
		this.dataScope = dataScope;
	}

//	public String getOldName() {
//		return oldName;
//	}
//
//	public void setOldName(String oldName) {
//		this.oldName = oldName;
//	}
//
//	public String getOldEnname() {
//		return oldEnname;
//	}
//
//	public void setOldEnname(String oldEnname) {
//		this.oldEnname = oldEnname;
//	}

//	public List<User> getUserList() {
//		return userList;
//	}
//
//	public void setUserList(List<User> userList) {
//		this.userList = userList;
//	}
//	
//	public List<String> getUserIdList() {
//		List<String> nameIdList = new ArrayList();
//		for (User user : userList) {
//			nameIdList.add(user.getId());
//		}
//		return nameIdList;
//	}
//
//	public String getUserIds() {
//		return StringUtils.join(getUserIdList(), ",");
//	}
	@JsonIgnore
	public List<MenuVo> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<MenuVo> menuList) {
		this.menuList = menuList;
	}
	@JsonIgnore
	public List<String> getMenuIdList() {
		List<String> menuIdList = new ArrayList();
		if(menuList==null)return menuIdList;
		for (MenuVo menu : menuList) {
			menuIdList.add(menu.getId());
		}
		return menuIdList;
	}

	public void setMenuIdList(List<String> menuIdList) {
		menuList = new ArrayList();
		for (String menuId : menuIdList) {
			MenuVo menu = new MenuVo();
			menu.setId(menuId);
			menuList.add(menu);
		}
	}

	public String getMenuIds() {
		return StringUtils.join(getMenuIdList(), ",");
	}
	
	public void setMenuIds(String menuIds) {
		menuList = new ArrayList();
		if (menuIds != null){
			String[] ids = StringUtils.split(menuIds, ",");
			setMenuIdList(Arrays.asList(ids));
		}
	}
	
	@JsonIgnore
	public List<OfficeVo> getOfficeList() {
		return officeList;
	}

	public void setOfficeList(List<OfficeVo> officeList) {
		this.officeList = officeList;
	}

	@JsonIgnore
	public List<String> getOfficeIdList() {
		List<String> officeIdList = new ArrayList();
		if(officeList==null)return officeIdList;
		for (OfficeVo office : officeList) {
			officeIdList.add(office.getId());
		}
		return officeIdList;
	}

	public void setOfficeIdList(List<String> officeIdList) {
		officeList = new ArrayList();
		for (String officeId : officeIdList) {
			OfficeVo office = new OfficeVo();
			office.setId(officeId);
			officeList.add(office);
		}
	}

	public String getOfficeIds() {
		return StringUtils.join(getOfficeIdList(), ",");
	}
	
	public void setOfficeIds(String officeIds) {
		officeList = new ArrayList();
		if (officeIds != null){
			String[] ids = StringUtils.split(officeIds, ",");
			setOfficeIdList(Arrays.asList(ids));
		}
	}
	
	/**
	 * 获取权限字符串列表
	 */
	@JsonIgnore
	public List<String> getPermissions() {
		List<String> permissions = new ArrayList();
		if(menuList==null)return permissions;
		for (MenuVo menu : menuList) {
			if (menu.getPermission()!=null && !"".equals(menu.getPermission())){
				permissions.add(menu.getPermission());
			}
		}
		return permissions;
	}



}
