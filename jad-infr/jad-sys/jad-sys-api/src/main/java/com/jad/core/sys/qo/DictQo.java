package com.jad.core.sys.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.qo.BaseQo;

public class DictQo extends BaseQo<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="数据值")
	private String value;	// 数据值
	
	@CURD(label="标签名")
	private String label;	// 标签名
	
	@CURD(label="类型")
	private String type;	// 类型
	
	@CURD(label="描述")
	private String description;// 描述
	
	@CURD(label="排序")
	private Integer sort;	// 排序
	
	@CURD(label="父Id")
	private String parentId;//父Id

	public DictQo() {
	}

	public DictQo(String id) {
		super(id);
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

}
