package com.jad.core.sys.qo;

import java.util.Date;

import com.jad.commons.annotation.CURD;
import com.jad.commons.qo.BaseQo;

public class LogQo extends BaseQo<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="开始日期")
	private Date createDateBegin;		// 开始日期
	
	@CURD(label="结束日期")
	private Date createDateEnd;		// 结束日期
	
	@CURD(label="日志类型")
	private String type; 		// 日志类型（1：接入日志；2：错误日志）
	
	@CURD(label="标题")
	private String title;
	
	@CURD(label="请求地址")
	private String requestUri;
//	private String createDateBegin;
//	private String createDateEnd;
	
	@CURD(label="异常信息")
	private String exception;
	
	/**
	 * 用户id
	 */
	@CURD(label="用户id")
	private String createBy;
	
	
	public LogQo() {
	}
	public LogQo(String id) {
		super(id);
	}
	
	
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDateBegin() {
		return createDateBegin;
	}
	public void setCreateDateBegin(Date createDateBegin) {
		this.createDateBegin = createDateBegin;
	}
	public Date getCreateDateEnd() {
		return createDateEnd;
	}
	public void setCreateDateEnd(Date createDateEnd) {
		this.createDateEnd = createDateEnd;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRequestUri() {
		return requestUri;
	}
	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	
	
	
}
