package com.jad.core.sys.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.qo.BaseQo;

public class RoleQo extends BaseQo<String>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@CURD(label="角色名称")
	private String name; 	// 角色名称
	
	
	@CURD(label="英文名称")
	private String enname;	// 英文名称
	
	
	public RoleQo() {
	}
	
	public RoleQo(String id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}
	
}
