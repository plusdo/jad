package com.jad.core.sys.service;

import java.util.List;

import com.jad.commons.service.TreeService;
import com.jad.core.sys.vo.OfficeVo;


/**
 * 机构Service
 */
public interface OfficeService extends TreeService<OfficeVo,String>  {
	
	public List<OfficeVo> findList(Boolean isAll,String type);
	
	/**
	 * 跟据角色id查找部门
	 * @param roleId
	 * @return
	 */
	public List<OfficeVo> findByRoleId(String roleId) ;
	
}
