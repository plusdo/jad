package com.jad.core.sys.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.ServiceException;
import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.commons.util.QoUtil;
import com.jad.commons.utils.Validate;
import com.jad.core.sys.dao.RoleDao;
import com.jad.core.sys.dao.UserDao;
import com.jad.core.sys.entity.RoleEo;
import com.jad.core.sys.entity.UserEo;
import com.jad.core.sys.qo.UserQo;
import com.jad.core.sys.service.UserService;
import com.jad.core.sys.util.PasswordUtil;
import com.jad.core.sys.vo.UserVo;
import com.jad.dao.utils.EntityUtils;

@Service("userService")
@Transactional(readOnly = true)
public class UserServiceImpl extends AbstractServiceImpl<UserEo,String, UserVo> implements UserService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;
	
	
	public String entryptPassword(String plainPassword){
		Validate.notBlank(plainPassword);
		return PasswordUtil.entryptPassword(plainPassword);
	}
	

	public boolean validatePassword(String plainPassword, String password){
		Validate.allNotBlank(plainPassword,password);
		return PasswordUtil.validatePassword(plainPassword, password);
	}
	
	@Override
	public UserVo findByLoginName(String loginName) {
		
		Validate.notBlank(loginName);
		
		UserEo eo = null;
		
		UserQo qo = QoUtil.newNormalDataQo(UserQo.class);
		qo.setLoginName(loginName);
		
		List<UserEo>eoList=userDao.findByQo(qo);
		if(eoList!=null && !eoList.isEmpty()){
			eo = eoList.get(0);
		}
		
		return EntityUtils.copyToVo(eo, UserVo.class);
	}

	@Override
	@Transactional(readOnly = false)
	public int deleteUserRole(String userId,String roleId){
		Validate.allNotBlank(userId,roleId);
		return userDao.deleteUserRole(userId,roleId);
	}
	
	@Transactional(readOnly = false)
	public void delete(UserVo vo){
		Validate.notNull(vo);
		Validate.notBlank(vo.getId());
		super.delete(vo);
		userDao.deleteUserRoleByUserId(vo.getId());//删除用户角色
	}
	
	/**
	 * 分配角色
	 * @param userId
	 * @param roleId
	 * @return
	 */
	@Override
	@Transactional(readOnly = false)
	public UserVo assignUserRole(String userId,String roleId){
		Validate.allNotBlank(userId,roleId);
		
		List<RoleEo> roleList = roleDao.findByUserId(userId);
		
		UserVo userVo=findById(userId);
		
		if(userVo==null){
			throw new ServiceException("找不到用户,userId:"+userId);
		}
		
		if(CollectionUtils.isNotEmpty(roleList)){
			for(RoleEo role:roleList){
				if(roleId.equals(role.getId())){
					String error=String.format("用户[%s]已存在角色[%s],不能再分配",(userVo==null?userId:userVo.getName()),role.getName());
					logger.warn(error);
//					throw new ServiceException(error);
					return userVo;
				}
			}
		}
		
		userDao.insertUserRole(userId, roleId);
		
		return userVo;
		
	}
	
	@Transactional(readOnly = false)
	public void update(UserVo vo){
		super.update(vo);
		if(!vo.getRoleList().isEmpty()){
			updateUserRole(vo.getId(),vo.getRoleIdList());
		}
	}
	
	@Transactional(readOnly = false)
	public void add(UserVo vo){
		super.add(vo);
		if(!vo.getRoleList().isEmpty()){
			updateUserRole(vo.getId(),vo.getRoleIdList());
		}
	}
	
	private void updateUserRole(String userId,List<String>roleIdList){
		userDao.deleteUserRoleByUserId(userId);
		for(String roleId:roleIdList){
			userDao.insertUserRole(userId, roleId);
		}
		
	}
	
	/**
	 * 查询拥有指定角色的用户
	 * @param roleId
	 * @return
	 */
	public List<UserVo>findByRoleId(String roleId){
		Validate.notBlank(roleId);
		return EntityUtils.copyToVoList(userDao.findByRoleId(roleId), UserVo.class);
	}

//	@Override
//	public List<UserVo> findByOfficeId(String officeId) {
//		
//		Validate.notBlank(officeId);
//		UserQo qo = QoUtil.newNormalDataQo(UserQo.class);
//		
//		qo.setOfficeId(officeId);
//		
//		List<UserEo> eoList = userDao.findByQo(qo);
//		
//		return EntityUtils.copyToVoList(eoList, UserVo.class);
//	}

	@Override
	@Transactional(readOnly = false)
	public int updatePasswordById(String id, String password) {
		Validate.allNotBlank(id,password);
		
		return userDao.updatePasswordById(id, PasswordUtil.entryptPassword(password));
	}

}
