package com.jad.core.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.jad.commons.context.Constants;
import com.jad.core.sys.dao.DictDao;
import com.jad.core.sys.entity.DictEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("dictDao")
public class DictDaoImpl extends AbstractJadEntityDao<DictEo,String>implements DictDao{

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<String> findTypeList() {
		
		String sql="SELECT type FROM sys_dict WHERE del_flag = ? GROUP BY type ORDER BY type";
		
		List params=new ArrayList();
		params.add(Constants.DEL_FLAG_NORMAL);
		
		return findBySql(sql, params, String.class);
		
	}
	
	
}
