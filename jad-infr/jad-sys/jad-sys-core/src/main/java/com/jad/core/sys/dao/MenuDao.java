/**
 */
package com.jad.core.sys.dao;

import java.util.List;

import com.jad.commons.dao.TreeDao;
import com.jad.core.sys.entity.MenuEo;
import com.jad.dao.annotation.JadDao;

/**
 * 菜单DAO接口
 */
@JadDao
public interface MenuDao extends TreeDao<MenuEo,String> {
	
	public List<MenuEo> findByUserId(String userId);
	
	/**
	 * 跟据角色id查找有权限的菜单
	 * @param roleId
	 * @return
	 */
	public List<MenuEo> findByRoleId(String roleId) ;
	
	
	
}
