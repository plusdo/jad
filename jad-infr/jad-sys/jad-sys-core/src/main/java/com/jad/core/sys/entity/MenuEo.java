package com.jad.core.sys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.jad.commons.entity.TreeEo;
import com.jad.dao.annotation.ForceUpdateNull;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;

@Entity
@Table(name = "sys_menu")
public class MenuEo extends TreeEo<MenuEo,String>{

	private static final long serialVersionUID = 1L;
	private MenuEo parent;	// 父级菜单
	private String href; 	// 链接
	private String target; 	// 目标（ mainFrame、_blank、_self、_parent、_top）
	private String icon; 	// 图标
	private String isShow; 	// 是否在菜单中显示（1：显示；0：不显示）
	private String permission; // 权限标识
	
	public MenuEo(){
		super();
	}
	
	public MenuEo(String id){
		super(id);
	}
	
	@JsonBackReference
	@NotNull
	@ManyToOne
	@RelateColumn(name="parent_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	public MenuEo getParent() {
		return parent;
	}

	public void setParent(MenuEo parent) {
		this.parent = parent;
	}

	

	@Column(name="href")
	@ForceUpdateNull(true)
	@Length(min=0, max=2000)
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	
	@Column(name="target")
	@ForceUpdateNull(true)
	@Length(min=0, max=20)
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
	
	@Column(name="icon")
	@ForceUpdateNull(true)
	@Length(min=0, max=100)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	
	@Column(name="is_show")
	@OrderBy("sort asc,update_date desc")
	@Length(min=1, max=1)
	public String getIsShow() {
		return isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

	@Column(name="permission")
	@Length(min=0, max=200)
	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}


}
