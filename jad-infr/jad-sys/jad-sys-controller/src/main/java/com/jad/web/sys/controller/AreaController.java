/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.web.api.Result;
import com.jad.core.sys.qo.AreaQo;
import com.jad.core.sys.service.AreaService;
import com.jad.core.sys.service.OfficeService;
import com.jad.core.sys.service.UserService;
import com.jad.core.sys.vo.AreaVo;
import com.jad.core.sys.vo.OfficeVo;
import com.jad.core.sys.vo.UserVo;
import com.jad.web.mvc.BaseController;

/**
 * 区域Controller
 * @author ThinkGem
 * @version 2013-5-15
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/area")
@Api(description = "区域管理")
public class AreaController extends BaseController {

	@Autowired
	private AreaService areaService;
	
	@Autowired
	private OfficeService officeService;
	
	@Autowired
	private UserService userService;
	
//	@ModelAttribute("area")
//	public AreaVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return areaService.findById(id);
//		}else{
//			return new AreaVo();
//		}
//	}
	
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:area:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看区域信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<AreaVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(areaService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:area:view")
	@ResponseBody
	@RequestMapping(value = {"findPage"})
	@ApiOperation(value = "查询区域列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<Page<AreaVo>> findPage(AreaQo areaQo,PageQo pageQo){
		Page<AreaVo> page = areaService.findPage(pageQo, areaQo);
		return Result.newSuccResult(page);
	}
	
	/**
	 * 修改
	 * @param areaVo
	 * @return
	 */
	@RequiresPermissions("sys:area:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改区域信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(AreaVo areaVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, areaVo)){
			return result;
		}
		if(StringUtils.isBlank(areaVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(areaVo,SecurityHelper.getCurrentUserId());
		areaService.update(areaVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:area:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增会员信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(AreaVo areaVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, areaVo)){
			return result;
		}
		preInsert(areaVo,SecurityHelper.getCurrentUserId());
		areaService.add(areaVo);
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:area:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除区域信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		AreaVo delVo = new AreaVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		areaService.delete(delVo);
		return result;
	}
	
	/**
	 * 查询区域树状列表
	 * @param extId
	 * @param response
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	@ApiOperation(value = "查询区域列表",httpMethod = "POST")
	@ApiCurdType(CurdType.FIND_LIST)
	public List<Map<String, Object>> treeData(
			@ApiParam(name="过滤id") @RequestParam(required=false) String extId, 
			HttpServletResponse response) {
		List<Map<String, Object>> mapList =  new ArrayList();
		List<AreaVo> list = areaService.findList(new AreaQo());
		for (int i=0; i<list.size(); i++){
			AreaVo e = list.get(i);
			if (StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map =  new HashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	
	

	@RequiresPermissions("sys:area:view")
	@RequestMapping(value = {"list", ""})
	public String list(AreaQo areaQo, Model model) {
		List<AreaVo>list=areaService.findList(new AreaQo());
		model.addAttribute("list", list);
		return "modules/sys/areaList";
	}

	@RequiresPermissions("sys:area:view")
	@RequestMapping(value = "form")
	public String form(AreaQo areaQo, Model model) {
		
		AreaVo areaVo = new AreaVo();
		
		if(StringUtils.isNotBlank(areaQo.getId())){//修改
			
			areaVo = areaService.findById(areaQo.getId());
			
		}else if(StringUtils.isNotBlank(areaQo.getParentId())){//添加下级
			
			areaVo.setParent(areaService.findById(areaQo.getParentId()));
			
		}else{ //添加新的
			
			UserVo currentUser = SecurityHelper.getCurrentUser();
			if(currentUser!=null && currentUser.getOffice()!=null 
					&& StringUtils.isNotBlank(currentUser.getOffice().getId()) ){
				
				OfficeVo office = officeService.findById(currentUser.getOffice().getId());
				if(office!=null && office.getArea()!=null ){
					areaVo.setParent(office.getArea());
				}
				
			}
			
		}
		
		
		model.addAttribute("areaVo", areaVo);
		return "modules/sys/areaForm";
	}
	
	@RequiresPermissions("sys:area:edit")
	@RequestMapping(value = "save")
	public String save(AreaVo areaVo, Model model, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/area";
		}
		if (!beanValidator(model, areaVo)){
//			return form(areaVo, model);
			model.addAttribute("areaVo", areaVo);
			return "modules/sys/areaForm";
		}
		if(StringUtils.isNotBlank(areaVo.getId())){
			preUpdate(areaVo,SecurityHelper.getCurrentUserId());
			areaService.update(areaVo);
		}else{
			preInsert(areaVo,SecurityHelper.getCurrentUserId());
			areaService.add(areaVo);
		}
		addMessage(redirectAttributes, "保存区域'" + areaVo.getName() + "'成功");
		return "redirect:" + adminPath + "/sys/area/";
	}
	
	@RequiresPermissions("sys:area:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/area";
		}
//		if (Area.isRoot(id)){
//			addMessage(redirectAttributes, "删除区域失败, 不允许删除顶级区域或编号为空");
//		}else{
		
		AreaVo delVo = new AreaVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		
		areaService.delete(delVo);
		addMessage(redirectAttributes, "删除区域成功");
//		}
		return "redirect:" + adminPath + "/sys/area/";
	}

	
}
