/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.utils.Collections3;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.web.api.Result;
import com.jad.core.sys.qo.MenuQo;
import com.jad.core.sys.qo.OfficeQo;
import com.jad.core.sys.qo.RoleQo;
import com.jad.core.sys.qo.UserQo;
import com.jad.core.sys.service.MenuService;
import com.jad.core.sys.service.OfficeService;
import com.jad.core.sys.service.RoleService;
import com.jad.core.sys.service.UserService;
import com.jad.core.sys.vo.RoleVo;
import com.jad.core.sys.vo.UserVo;
import com.jad.web.mvc.BaseController;

/**
 * 角色Controller
 * @author ThinkGem
 * @version 2013-12-05
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/role")
@Api(description = "角色管理")
public class RoleController extends BaseController {

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private OfficeService officeService;
	
//	@ModelAttribute("role")
//	public RoleVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return roleService.findById(id);
//		}else{
//			return new RoleVo();
//		}
//	}
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:role:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看角色信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<RoleVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(roleService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @return
	 */
	@RequiresPermissions("sys:role:view")
	@ResponseBody
	@RequestMapping(value = {"findList"})
	@ApiOperation(value = "查询角色列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<List<RoleVo>> findList(RoleQo roleQo){
		List<RoleVo> list = roleService.findList(roleQo);
		return Result.newSuccResult(list);
	}
	
	/**
	 * 修改
	 * @param roleVo
	 * @return
	 */
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改角色信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(RoleVo roleVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, roleVo)){
			return result;
		}
		if (!"true".equals(checkName(roleVo.getName(),roleVo.getId()))){
			result.setBusiFail("保存角色'" + roleVo.getName() + "'失败, 角色名已存在");
			return result;
		}
		if (!"true".equals(checkEnname( roleVo.getEnname(),roleVo.getId()))){
			result.setBusiFail("保存角色'" + roleVo.getName() + "'失败, 英文名已存在");
			return result;
		}
		if(StringUtils.isBlank(roleVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(roleVo,SecurityHelper.getCurrentUserId());
		roleService.update(roleVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增会员信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(RoleVo roleVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, roleVo)){
			return result;
		}
		if (!"true".equals(checkName(roleVo.getName(),roleVo.getId()))){
			result.setBusiFail("保存角色'" + roleVo.getName() + "'失败, 角色名已存在");
			return result;
		}
		if (!"true".equals(checkEnname( roleVo.getEnname(),roleVo.getId()))){
			result.setBusiFail("保存角色'" + roleVo.getName() + "'失败, 英文名已存在");
			return result;
		}
		preInsert(roleVo,SecurityHelper.getCurrentUserId());
		roleService.add(roleVo);
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除角色信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		RoleVo delVo = new RoleVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		roleService.delete(delVo);
		return result;
	}
	
	
	@RequiresPermissions("sys:role:view")
	@RequestMapping(value = {"list", ""})
	public String list(RoleQo roleQo, Model model) {
		List<RoleVo> list = roleService.findList(roleQo);
		model.addAttribute("list", list);
		return "modules/sys/roleList";
	}

	@RequiresPermissions("sys:role:view")
	@RequestMapping(value = "form")
	public String form(RoleQo roleQo, Model model) {
		
		RoleVo roleVo = new RoleVo();
		if(StringUtils.isNotBlank(roleQo.getId())){
			roleVo = roleService.findById(roleQo.getId());
			roleVo.setMenuList(menuService.findByRoleId(roleQo.getId()));
			roleVo.setOfficeList(officeService.findByRoleId(roleQo.getId()));
		}
		
		if (roleVo.getOffice()==null){
			UserVo currentUser = SecurityHelper.getCurrentUser();
			if(currentUser.getOffice()!=null){
				roleVo.setOffice(officeService.findById(currentUser.getOffice().getId()));
			}
		}
		
		return toForm(roleVo,model);
	}
	private String toForm(RoleVo roleVo, Model model){
		model.addAttribute("roleVo", roleVo);
		model.addAttribute("menuList", menuService.findList(new MenuQo()));
		model.addAttribute("officeList", officeService.findList(new RoleQo()));
		return "modules/sys/roleForm";
	}
	
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "save")
	public String save(RoleVo roleVo, Model model, RedirectAttributes redirectAttributes) {
//		if(!systemService.getUser().isAdmin()&&role.getSysData().equals(Constants.YES)){
//			addMessage(redirectAttributes, "越权操作，只有超级管理员才能修改此数据！");
//			return "redirect:" + adminPath + "/sys/role/?repage";
//		}
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/role/?repage";
		}
		if (!beanValidator(model, roleVo)){
			return toForm(roleVo, model);
		}
		if (!"true".equals(checkName(roleVo.getName(),roleVo.getId()))){
			addMessage(model, "保存角色'" + roleVo.getName() + "'失败, 角色名已存在");
			return toForm(roleVo, model);
		}
		if (!"true".equals(checkEnname( roleVo.getEnname(),roleVo.getId()))){
			addMessage(model, "保存角色'" + roleVo.getName() + "'失败, 英文名已存在");
			return toForm(roleVo, model);
		}
		if(StringUtils.isNotBlank(roleVo.getId())){
			preUpdate(roleVo,SecurityHelper.getCurrentUserId());
			roleService.update(roleVo);
		}else{
			preInsert(roleVo,SecurityHelper.getCurrentUserId());
			roleService.add(roleVo);
		}
		
		addMessage(redirectAttributes, "保存角色'" + roleVo.getName() + "'成功");
		return "redirect:" + adminPath + "/sys/role/?repage";
	}
	
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id, RedirectAttributes redirectAttributes) {
//		if(!systemService.getUser().isAdmin() && role.getSysData().equals(Constants.YES)){
//			addMessage(redirectAttributes, "越权操作，只有超级管理员才能修改此数据！");
//			return "redirect:" + adminPath + "/sys/role/?repage";
//		}
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/role/?repage";
		}
//		if (Role.isAdmin(id)){
//			addMessage(redirectAttributes, "删除角色失败, 不允许内置角色或编号空");
////		}else if (UserUtils.getUser().getRoleIdList().contains(id)){
////			addMessage(redirectAttributes, "删除角色失败, 不能删除当前用户所在角色");
//		}else{
		
		RoleVo delVo = new RoleVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		roleService.delete(delVo);
		
			addMessage(redirectAttributes, "删除角色成功");
//		}
		return "redirect:" + adminPath + "/sys/role/?repage";
	}
	
	/**
	 * 角色分配页面
	 * @param roleVo
	 * @param model
	 * @return
	 */
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "assign")
	public String assign(@RequestParam(required=true)String id, Model model) {
		model.addAttribute("roleVo", roleService.findById(id) );
		List<UserVo> userList = userService.findByRoleId(id);
		model.addAttribute("userList", userList);
		return "modules/sys/roleAssign";
	}
	
	/**
	 * 角色分配
	 * @param roleVo
	 * @param idsArr
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "assignrole")
	public String assignRole(@RequestParam(required=true)String id, @RequestParam(required=true)String[] idsArr, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/role/assign?id="+id;
		}
		
		RoleVo roleVo=roleService.findById(id);
		
		StringBuilder msg = new StringBuilder();
		int newNum = 0;
		for (int i = 0; i < idsArr.length; i++) {
			
			UserVo userVo = userService.assignUserRole(idsArr[i], id);
			
//			UserVo user = systemService.assignUserToRole(roleVo, userService.findById(idsArr[i]));
			
//			if (null != user) {
//			if(res){
//				msg.append("<br/>新增用户【" + user.getName() + "】到角色【" + roleVo.getName() + "】！");
				msg.append("<br/>新增用户【" + userVo.getName() + "】到角色【" + roleVo.getName() + "】！");
				newNum++;
//			}
		}
		addMessage(redirectAttributes, "已成功分配 "+newNum+" 个用户"+msg);
		return "redirect:" + adminPath + "/sys/role/assign?id="+id;
	}
	
	/**
	 * 角色分配 -- 打开角色分配对话框
	 * @param roleVo
	 * @param model
	 * @return
	 */
	@RequiresPermissions("sys:role:view")
	@RequestMapping(value = "usertorole")
	public String selectUserToRole(@RequestParam(required=true)String id, Model model) {
//		UserQo qo=new UserQo();
//		qo.setRoleId(roleVo.getId());
//		List<UserVo> userList = userService.findList(qo);
		
		List<UserVo> userList = userService.findByRoleId(id);
		
		RoleVo roleVo = roleService.findById(id);
		
		model.addAttribute("roleVo", roleVo);
		
		model.addAttribute("userList", userList);
		model.addAttribute("selectIds", Collections3.extractToString(userList, "name", ","));
		model.addAttribute("officeList", officeService.findList(new OfficeQo()));
		return "modules/sys/selectUserToRole";
	}
	
	/**
	 * 角色分配 -- 根据部门编号获取用户列表
	 * @param officeId
	 * @param response
	 * @return
	 */
	@RequiresPermissions("sys:role:view")
	@ResponseBody
	@RequestMapping(value = "users")
	public List<Map<String, Object>> users(@RequestParam(required=true)String officeId, HttpServletResponse response) {
		List<Map<String, Object>> mapList =  new ArrayList();
		UserQo qo = new UserQo();
		qo.setOfficeId(officeId);
		
		List<UserVo>userList=userService.findList(qo);
		
		for (UserVo e : userList) {
			Map<String, Object> map =  new HashMap();
			map.put("id", e.getId());
			map.put("pId", 0);
			map.put("name", e.getName());
			mapList.add(map);			
		}
		return mapList;
	}
	
	/**
	 * 角色分配 -- 从角色中移除用户
	 * @param userId
	 * @param roleId
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("sys:role:edit")
	@RequestMapping(value = "outrole")
	public String outrole(@RequestParam(required=true)String userId, @RequestParam(required=true)String roleId, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/role/assign?id="+roleId;
		}
		RoleVo role = roleService.findById(roleId);
		UserVo user = userService.findById(userId);
		if(role ==null || user == null ){
			logger.warn("找不到用户或角色,userId:"+userId+",roleId:"+roleId);
			addMessage(redirectAttributes, "移除失败,不存在此用户名角色！");
		}else{
//			if (systemService.getUser().getId().equals(userId)) {
//			addMessage(redirectAttributes, "无法从角色【" + role.getName() + "】中移除用户【" + user.getName() + "】自己！");
//		}else {
			user.setRoleList(roleService.findByUserId(user.getId()));
			if (user.getRoleList().size() <= 1){
				addMessage(redirectAttributes, "用户【" + user.getName() + "】从角色【" + role.getName() + "】中移除失败！这已经是该用户的唯一角色，不能移除。");
			}else{
				
				int i = userService.deleteUserRole(userId, roleId);
				if(i==0){
					addMessage(redirectAttributes, "用户【" + user.getName() + "】从角色【" + role.getName() + "】中移除失败！");
				}else{
					addMessage(redirectAttributes, "用户【" + user.getName() + "】从角色【" + role.getName() + "】中移除成功！");
				}
				
//				Boolean flag = systemService.outUserInRole(role, user);
//				if (!flag) {
//					addMessage(redirectAttributes, "用户【" + user.getName() + "】从角色【" + role.getName() + "】中移除失败！");
//				}else {
//					addMessage(redirectAttributes, "用户【" + user.getName() + "】从角色【" + role.getName() + "】中移除成功！");
//				}
				
				
			}		
//		}
		}

		return "redirect:" + adminPath + "/sys/role/assign?id="+role.getId();
	}
	
	

	/**
	 * 验证角色名是否有效
	 * @param oldName
	 * @param name
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "checkName")
	public String checkName(String name,String exclusionId) {
		if (StringUtils.isBlank(name)) {
			return "false";
		}
		RoleQo qo = new RoleQo();
		qo.setName(name);
		
		List<RoleVo> voList = roleService.findList(qo);
		if(CollectionUtils.isEmpty(voList)){
			return "true";
		}
		
		if(StringUtils.isBlank(exclusionId)){
			return "false";
		}
		
		for(RoleVo vo:voList){
			if(!exclusionId.equals(vo.getId())){
				return "false";
			}
		}
					
		return "true";
	}

	/**
	 * 验证角色英文名是否有效
	 * @param oldName
	 * @param name
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "checkEnname")
	public String checkEnname(String enname,String exclusionId) {
		if (StringUtils.isBlank(enname)) {
			return "false";
		} 
			
		RoleQo qo = new RoleQo();
		qo.setEnname(enname);
		
		List<RoleVo> voList = roleService.findList(qo);
		if(CollectionUtils.isEmpty(voList)){
			return "true";
		}
		
		if(StringUtils.isBlank(exclusionId)){
			return "false";
		}
		
		for(RoleVo vo:voList){
			if(!exclusionId.equals(vo.getId())){
				return "false";
			}
		}
		
		return "true";
	}

}
