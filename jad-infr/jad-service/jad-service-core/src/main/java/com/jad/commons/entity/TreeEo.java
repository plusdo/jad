package com.jad.commons.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.OrderBy;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;

public abstract class TreeEo<EO extends BaseEo<ID>,ID extends Serializable> extends BaseEo<ID>{
	
	private static final long serialVersionUID = 1L;

	protected String parentIds; // 所有父级编号
	
	protected String name; 	// 名称
	
	protected Integer sort;		// 排序
	
	public TreeEo() {
		super();
	}
	
	public TreeEo(ID id) {
		super(id);
	}
	
	/**
	 * 父对象，只能通过子类实现，父类实现mybatis无法读取
	 * @return
	 */
	@JsonBackReference
	@NotNull
	public abstract EO getParent();

	/**
	 * 父对象，只能通过子类实现，父类实现mybatis无法读取
	 * @return
	 */
	public abstract void setParent(EO parent);

	@Column(name="parent_ids")
	@Length(min=1, max=2000)
	public String getParentIds() {
		return parentIds;
	}
	
	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	@Length(min=1, max=100)
	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="sort")
	@OrderBy("sort")
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	public ID getParentId() {
		ID id = null;
		if (getParent() != null){
			return getParent().getId();
		}
		return id;
	}
	

	
}
