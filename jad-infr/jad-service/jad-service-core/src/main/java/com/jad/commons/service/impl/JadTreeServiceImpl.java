package com.jad.commons.service.impl;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.jad.commons.utils.StringUtils;
import com.jad.commons.dao.TreeDao;
import com.jad.commons.entity.TreeEo;
import com.jad.commons.service.ServiceException;
import com.jad.commons.service.TreeService;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.QueryObject;
import com.jad.commons.vo.TreeVo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.utils.EntityUtils;



@Transactional(readOnly = true)
public class JadTreeServiceImpl<EO extends TreeEo<EO,ID>, 
	ID extends Serializable,VO extends TreeVo<VO,ID>> 
	extends AbstractServiceImpl<EO,ID,VO> implements TreeService<VO,ID>{
	
	private static final Logger logger = LoggerFactory.getLogger(JadTreeServiceImpl.class);
	
	private static final Integer DEF_SORT = 30;
	
	@Transactional(readOnly = false)
	public void add(VO vo) {
		
		Assert.notNull(vo);
		
		if(vo.getSort() == null){
			vo.setSort(DEF_SORT);
		}
		// 如果没有设置父节点，则代表为跟节点，有则获取父节点实体
		if (vo.getParent() == null || vo.getParentId() == null
				|| !StringUtils.isNotBlank(vo.getParentId().toString()) ){
			vo.setParent(newRootParent());
		}else{
			
			VO parent = this.findById(vo.getParentId());
			
			if(parent==null){
				parent = newRootParent();
			}
			vo.setParent(parent);	
			
		}
		// 设置新的父节点串
		vo.setParentIds(vo.getParent().getParentIds()+vo.getParent().getId()+",");
		
		curdHelper.add(vo);
	}
	

	
	@Transactional(readOnly = false)
	public void delete(VO vo) {
		Assert.notNull(vo);
		Assert.notNull(vo.getId());
		curdHelper.delete(vo);
		List<VO>subList = findSubList(vo.getId());
		if(subList==null || subList.isEmpty()){
			return;
		}
		for(VO subVo:subList){
			curdHelper.delete(subVo);
		}
	}
	
	@Transactional(readOnly = false)
	public void update(VO vo) {
		
		Assert.notNull(vo);
		Assert.notNull(vo.getId());
		
		if(vo.getParent() == null || vo.getParentId() ==null
				|| !StringUtils.isNotBlank(vo.getParentId().toString()) ){//不修改父节点
			vo.setParent(null);
			vo.setParentIds(null);
			curdHelper.update(vo);
			return;
		}
		
		VO parent = findById(vo.getParentId());
		if(parent == null && vo.getDefRootId().equals(vo.getParentId())){ //修改的父节点是根节点
			parent = newRootParent();
		}
		
		if(parent==null){
//			throw new ServiceException("修改失败,找不到id为"+vo.getParentId()+"的"+getEoMetaInfo().getObjName()+"类型的数据");
			//从业务上，这里的代码应该永远不会被执行
			logger.warn("没有找到id为"+vo.getParentId()+"的数据，voType:"+vo.getClass().getName());
			vo.setParent(null);
			vo.setParentIds(null);
			curdHelper.update(vo);
			return;

		}
		vo.setParent(parent);
		
		ID id = vo.getId();
		VO oldVo = this.findById(id);
		
		if(oldVo==null){
//			throw new ServiceException("修改失败,找不到id为"+vo.getId()+"的"+getEoMetaInfo().getObjName()+"类型的数据");
			logger.warn("找不到id为:"+vo.getId()+"的数据，跳过修改");
			return;
		}
		
		// 获取修改前的parentIds，用于更新子节点的parentIds
		String oldParentIds = oldVo.getParentIds(); 
		
		//新的parentIds
		String newParentIds = vo.getParent().getParentIds()+vo.getParent().getId()+",";
		
		if(oldParentIds!=null && StringUtils.isNotBlank(oldParentIds) 
				&& oldParentIds.equals(newParentIds)){ //没有改变层级关系
			vo.setParent(null);
			vo.setParentIds(null);
			curdHelper.update(vo);
			return;
		}
		
		// 设置新的父节点串
		vo.setParentIds(newParentIds);
		
		curdHelper.update(vo);
		
		// 更新子节点 parentIds
//		TreeQo<ID> qo=new TreeQo<ID>();
//		qo.setParentIdsLike(","+vo.getId()+",");
//		List<EO> list = getTreeDao().findByQo(QoUtil.wrapperNormalDataQo(qo));
				
		List<EO> list = getTreeDao().findSubList(vo.getId());
		for ( EO e : list){
			if (e.getParentIds() != null && oldParentIds != null){
				String subNewParentIds = e.getParentIds().replace(oldParentIds, vo.getParentIds());
				e.setParentIds(subNewParentIds);
				getTreeDao().updateParentIds((ID)e.getId(),(ID)e.getParent().getId(),e.getParentIds());
			}
		}
		
	}
	
	/**
	 * 查询子节点列表
	 * @param parentId
	 * @return
	 */
	public List<VO>findSubList(ID parentId){
		Assert.notNull(parentId);
		return EntityUtils.copyToVoList(getTreeDao().findSubList(parentId), curdHelper.getVoMetaInfo().getMetaClass());
	}
	
	private VO newRootParent(){
		VO parent = curdHelper.newVo();
		parent.setId(parent.getDefRootId());
		parent.setParentIds("");
		return parent;
	}
	
	protected TreeDao<EO,ID> getTreeDao(){
		JadEntityDao<EO,ID> dao = getDao();
		if(dao==null){
			throw new ServiceException("无法获得dao,请确认service"+this.getClass().getName()+"中已经正常注入了dao");
		}
		if(! (dao instanceof TreeDao )){
			throw new ServiceException("无法在service"+this.getClass().getName()+"中，将"+dao.getClass().getName()+"转换成TreeDao类型的dao");
		}
		
		return (TreeDao<EO,ID>)dao;
	}


	@Override
	public int updateParentIds(ID id, ID parentId, String parentIds) {
		Assert.notNull(id);
		Assert.notNull(parentId);
		Assert.notNull(parentIds);
		return getTreeDao().updateParentIds(id, parentId, parentIds);
	}


//	@Override
//	public List<VO> findByParentIdsLike(String parentIds) {
//		Assert.notBlank(parentIds);
//		return EntityUtils.copyToVoList(getTreeDao().findByParentIdsLike(parentIds), getVoMetaInfo().getMetaClass());
//	}

	@Override
	@Transactional(readOnly = false)
	public int updateSort(ID id, Integer sort) {
		Assert.notNull(id);
		Assert.notNull(sort);
		return getTreeDao().updateSort(id, sort);
	}






}
