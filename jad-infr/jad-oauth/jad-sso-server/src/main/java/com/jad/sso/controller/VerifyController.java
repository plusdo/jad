package com.jad.sso.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jad.sso.annotation.Action;
import com.jad.sso.annotation.Login;
import com.jad.sso.util.CaptchaUtil;
import com.jad.web.mvc.BaseController;

/**
 * 验证码
 * @author Administrator
 *
 */
@Controller
public class VerifyController extends BaseController {
	
	protected Logger logger = LoggerFactory.getLogger(VerifyController.class);
	/**
	 * 验证码 （注解跳过权限验证）
	 */
	@Login(action = Action.Skip)
	@ResponseBody
	@RequestMapping("/verify")
	public void verify(HttpServletResponse response) {
		try {
			String verifyCode = CaptchaUtil.outputImage(response.getOutputStream());
			System.out.println("验证码:" + verifyCode);
		} catch (IOException e) {
			logger.error("生成验证码失败,"+e.getMessage(),e);
		}
	}
	
	
	
}
