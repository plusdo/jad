package com.jad.commons.security.shiro;

import java.io.Serializable;

import org.apache.shiro.session.Session;

import com.jad.commons.utils.IdGen;

public class SessionIdGenerator extends IdGen implements org.apache.shiro.session.mgt.eis.SessionIdGenerator{

	@Override
	public Serializable generateId(Session session) {
		return IdGen.uuid();
	}

}
