package com.jad.commons.security.shiro;

public class ApiPrincipal extends Principal{
	
	public ApiPrincipal(String id, String loginName, String name,
			boolean mobileLogin) {
		
		super(id, loginName, name, mobileLogin);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username; //
	private Object data;//此身份携带的附加数，比如用户信息，权限等
	
	
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

}
