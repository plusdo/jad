package com.jad.cache.redis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.cache.DefaultRedisCachePrefix;
import org.springframework.data.redis.cache.RedisCachePrefix;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.jad.cache.common.JadAbstractCacheManager;
import com.jad.cache.common.JadCache;


/**
 * 这个类参考了 org.springframework.data.redis.cache.RedisCacheManager
 * @author hechuan
 *
 */
public class JadRedisCacheManager extends JadAbstractCacheManager    {
	
	private static final Logger logger = LoggerFactory.getLogger(JadRedisCacheManager.class);

	@SuppressWarnings("rawtypes")//
	private RedisTemplate template;
	
	private boolean usePrefix = true;
	
	private RedisCachePrefix cachePrefix = new DefaultRedisCachePrefix();
	
	@Override
	protected synchronized Collection<? extends Cache> loadCaches() {
		Assert.notNull(this.getCacheClient(),"没有注入缓存客户端");
		Assert.notNull(template, "没有注入RedisTemplate");
		return addConfiguredCachesIfNecessary(getCacheClient().isAutoStart() ? loadAndInitRemoteCaches() : Collections.<Cache> emptyList());
		
	}
	
	protected Cache getMissingCache(String name) {
			
		Assert.notNull(getCacheClient(),"没有注入缓存客户端,"+this.getLoggerInfo(name));
		
		if(!getCacheClient().isAutoCreateCache()){
			logger.warn("无法创建名称为:"+name+"的缓存，因为当前缓存管理器不允许自动创建缓存,clientName"+getCacheClient().getClientName());
			return null;
		}
		
		logger.debug("当前缓存中管理器中没有找到缓存，自动生成一个,"+this.getLoggerInfo(name));
		
		return newCache(name);
			
	}


	@Override
	public Cache newCache(JadCache jc) {
		Assert.notNull(jc);
		Assert.notNull(this.getCacheClient(),"创建缓存失败,没有注入缓存客户端,"+this.getLoggerInfo(jc.getName()));
		
		int activityTime = jc.getDefActivityTime();
		boolean allowNullValues = jc.isAllowNullValues();
		
		return newCache(jc.getName(),activityTime,allowNullValues);
	}
	
	protected JadRedisCache newCache(String cacheName) {
		return newCache(cacheName,getCacheClient().getDefActivityTime(),getCacheClient().isAllowNullValues());
	}
	
	protected JadRedisCache newCache(String cacheName,int activityTime,boolean allowNullValues) {
		
		Assert.notNull(this.getCacheClient(),"没有注入缓存客户端");
		Assert.notNull(this.template, "没有注入RedisTemplate");
		return new JadRedisCache(getCacheClient(),cacheName,
				activityTime,allowNullValues,
				(usePrefix ? cachePrefix.prefix(cacheName) : null),
				template) ;
	}
	
	protected Cache createAndAddCache(String cacheName) {
		addCache(newCache(cacheName));
		return super.getCache(cacheName);
	}
	
	
	protected Collection<? extends Cache> addConfiguredCachesIfNecessary(Collection<? extends Cache> caches) {

		Assert.notNull(caches, "Caches must not be null!");

		Collection<Cache> result = new ArrayList<Cache>(caches);

		for (String cacheName : getCacheNames()) {

			boolean configuredCacheAlreadyPresent = false;

			for (Cache cache : caches) {

				if (cache.getName().equals(cacheName)) {
					configuredCacheAlreadyPresent = true;
					break;
				}
			}

			if (!configuredCacheAlreadyPresent) {
				result.add(getCache(cacheName));
			}
		}

		return result;
	}
	
	
	protected List<Cache> loadAndInitRemoteCaches() {

		List<Cache> caches = new ArrayList<Cache>();

		try {
			Set<String> cacheNames = loadRemoteCacheKeys();
			if (!CollectionUtils.isEmpty(cacheNames)) {
				for (String cacheName : cacheNames) {
					if (null == super.getCache(cacheName)) {
						caches.add(newCache(cacheName));
					}
				}
			}
		} catch (Exception e) {
			if (logger.isWarnEnabled()) {
				logger.warn("Failed to initialize cache with remote cache keys.", e);
			}
		}

		return caches;
	}
	
	@SuppressWarnings("unchecked")
	protected Set<String> loadRemoteCacheKeys() {
		return (Set<String>) template.execute(new RedisCallback<Set<String>>() {

			@Override
			public Set<String> doInRedis(RedisConnection connection) throws DataAccessException {

				// we are using the ~keys postfix as defined in RedisCache#setName
				Set<byte[]> keys = connection.keys(template.getKeySerializer().serialize("*~keys"));
				Set<String> cacheKeys = new LinkedHashSet<String>();

				if (!CollectionUtils.isEmpty(keys)) {
					for (byte[] key : keys) {
						cacheKeys.add(template.getKeySerializer().deserialize(key).toString().replace("~keys", ""));
					}
				}

				return cacheKeys;
			}
		});
	}
	
	

	public RedisTemplate getTemplate() {
		return template;
	}

	public void setTemplate(RedisTemplate template) {
		this.template = template;
	}

	public boolean isUsePrefix() {
		return usePrefix;
	}

	public void setUsePrefix(boolean usePrefix) {
		this.usePrefix = usePrefix;
	}






	
	
	
	
	
	
}
