package com.jad.cache.ehcache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;



@Cacheable(value="accountService")
public class AccountService {

	private static Logger logger=LoggerFactory.getLogger(AccountService.class); 
	
	
//	 @Cacheable(value="accountCache",condition="#userName.length() <= 4")// 缓存名叫 accountCache
	 @Cacheable(value="accountCacheddd")// 缓存名叫 accountCache
	 public Account getAccountByName(String userName) {
		 // 方法内部实现不考虑缓存逻辑，直接实现业务
		 return getFromDB(userName); 
	 }
	 
	 @Cacheable(value="accountCache2")
	 public Account getAccountByName2(String userName) {
		 logger.debug("调用getAccountByName2。。。");
		 return new Account();
	 }
	 
	 public Account getAccountByName3(String userName) {
		 logger.debug("调用getAccountByName3。。。");
		 return new Account();
	 }
	 

	 private Account getFromDB(String userName) {
		 logger.debug("从数据库中查询account");
//		 return new Account();
		 return null;
		 
	 }	

}
