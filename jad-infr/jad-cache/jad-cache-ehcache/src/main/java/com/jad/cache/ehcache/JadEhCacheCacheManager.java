package com.jad.cache.ehcache;

import java.util.Collection;
import java.util.LinkedHashSet;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheManagerUtils;
import org.springframework.util.Assert;

import com.jad.cache.common.CacheException;
import com.jad.cache.common.JadAbstractCacheManager;
import com.jad.cache.common.JadCache;

/**
 * 自定义ehcache管理器
 * 
 * @author hechuan
 *
 */
public class JadEhCacheCacheManager extends JadAbstractCacheManager{
	
	private static final Logger logger = LoggerFactory.getLogger(JadEhCacheCacheManager.class);
	
	private net.sf.ehcache.CacheManager cacheManager;
	
	
	public JadEhCacheCacheManager() {
	}
	public JadEhCacheCacheManager(net.sf.ehcache.CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	
	/**
	 * 加载ehcache.xml配置文件中的cache
	 */
	@Override
	protected Collection<Cache> loadCaches() {
		Status status = getCacheManager().getStatus();
		if (!Status.STATUS_ALIVE.equals(status)) {
			throw new CacheException("必须是一个alive状态的状态，当前缓存状态是:" + status.toString());
		}
		Assert.notNull(this.getCacheClient(),"没有注入缓存客户端");
		
		String[] names = getCacheManager().getCacheNames();
		Collection<Cache> caches = new LinkedHashSet<Cache>(names.length);
		for (String name : names) {
			caches.add(newCache(name));
		}
		return caches;
	}
	
	@Override
	protected Cache getMissingCache(String name) {
		
		Assert.notNull("无法获得cache,因为没有注入缓存客户端,"+this.getLoggerInfo(name));
		
		if(!getCacheClient().isAutoCreateCache()){
			logger.warn("无法创建名称为:"+name+"的缓存，因为当前缓存客户端不允许自动创建缓存,clientName"+getCacheClient().getClientName());
			return null;
		}
		
		return this.newCache(name);
		
	}
	
	
	public Cache newCache(JadCache jc){
		Assert.notNull(this.getCacheClient(),"创建缓存失败,没有注入缓存客户端,"+this.getLoggerInfo(jc.getName()));
		Ehcache ehcache = getCacheManager().getEhcache(jc.getName());
		if (ehcache == null) {
			ehcache = addDefaultCache(getCacheManager(), jc.getName());
		}
		return new JadEhCacheCache(getCacheClient(),ehcache,jc.getDefActivityTime(),jc.isAllowNullValues());
	}
	
	public Cache newCache(String name) {
		Assert.notNull(this.getCacheClient(),"创建缓存失败,没有注入缓存客户端,"+this.getLoggerInfo(name));
		int activityItme = getCacheClient().getDefActivityTime() == null ? 0 : getCacheClient().getDefActivityTime().intValue() ;
		boolean allowNullValues = getCacheClient().getAllowNullValues() == null ? false:getCacheClient().getAllowNullValues().booleanValue();
		
		Ehcache ehcache = getCacheManager().getEhcache(name);
		if (ehcache == null) {
			ehcache = addDefaultCache(getCacheManager(), name);
		}
		
		return new JadEhCacheCache(getCacheClient(),ehcache,activityItme,allowNullValues);
	}
	
	public static net.sf.ehcache.Cache addDefaultCache(net.sf.ehcache.CacheManager cacheManager,String name){
		synchronized (cacheManager) {
			if(cacheManager.cacheExists(name)){
				logger.warn("缓存管理器中存在名称为["+name+"]的缓存");
			}else{
				cacheManager.addCache(name);
			}
			
			return cacheManager.getCache(name);
		}
	
	}
	

	
	public void setCacheManager(net.sf.ehcache.CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}

	public net.sf.ehcache.CacheManager getCacheManager() {
		return this.cacheManager;
	}
	
	
	@Override
	public void afterPropertiesSet() {
		if (getCacheManager() == null) {
			setCacheManager(EhCacheManagerUtils.buildCacheManager());
		}
		super.afterPropertiesSet();
	}

	
	
	
	
	

}
