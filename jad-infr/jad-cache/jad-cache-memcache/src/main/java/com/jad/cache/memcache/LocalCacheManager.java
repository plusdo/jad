package com.jad.cache.memcache;

import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.alisoft.xplatform.asf.cache.ICache;
import com.alisoft.xplatform.asf.cache.ICacheManager;
import com.alisoft.xplatform.asf.cache.impl.DefaultCacheImpl;


/**
 * 本地CacheManager
 * 
 * 因本框架采用阿里技术实现的memcache客户端，它自带了一个map实现的本地缓存,本框架对它进行了二次封装
 * 用于在memcache服务器无法启动的情况下，直接切换到本地缓存
 *
 */
@SuppressWarnings("rawtypes")
public class LocalCacheManager implements ICacheManager,InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(LocalCacheManager.class);
	

	/**
	 * 是否复用主缓存
	 * 
	 * 阿里实现的本地缓存中，为每个cache实例维护了一个后台线程，用以定时清理过期的数据
	 * 在缓存的数据不算多时，服务器配置不高的情况下，可设这个参数为true,那么所有的数据都被缓存到同一个cache实例，以减少后台线程的数量
	 * 
	 */
	private boolean multiplexMasterCache = true;
	
	/**
	 * 非复用情况下（multiplexMasterCache为false）的cache集合
	 */
	private ConcurrentHashMap<String, ICache> cacheMap ;
	
	/**
	 * 复用情况下（multiplexMasterCache为false）,系统个client只有一个cache实例
	 */
	private ICache cache ;
	
	/**
	 * 主缓存客户端名称
	 */
	private String masterCacheName ;
	
	@Override
	public ICache getCache(String name) {
		if(this.isMultiplexMasterCache()){
			return cache;
		}
		ICache cache = cacheMap.get(name);
		if (cache == null) {
			logger.info("当前缓存管理器中没有名字为"+name+"的缓存，自动生成一个");
			cache = new DefaultCacheImpl();
			ICache oldCache = cacheMap.putIfAbsent(name, cache);
			if (oldCache != null) {
				cache = oldCache;
			}
		}
		return cache;
		
	}

	@Override
	public void setConfigFile(String configFile) {
		// TODO Auto-generated method stub
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
	}

	@Override
	public void stop() {
		if(this.isMultiplexMasterCache()){
			cache.clear();
			return;
		}
		for(ICache cache :cacheMap.values()){
			cache.clear();
		}
		
		
	}

	@Override
	public void reload(String configFile) {
		// TODO Auto-generated method stub
	}

	@Override
	public void clusterCopy(String fromCache, String cluster) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setResponseStatInterval(int seconds) {
		// TODO Auto-generated method stub
	}

	
	public void afterPropertiesSet(){
		if(this.isMultiplexMasterCache()){
			cache = new DefaultCacheImpl();
		}else{
			cacheMap = new ConcurrentHashMap<String, ICache>();
		}
	}


	public boolean isMultiplexMasterCache() {
		return multiplexMasterCache;
	}

	public void setMultiplexMasterCache(boolean multiplexMasterCache) {
		this.multiplexMasterCache = multiplexMasterCache;
	}

	public ICache getCache() {
		return cache;
	}

	public ConcurrentHashMap<String, ICache> getCacheMap() {
		return cacheMap;
	}
	
	public ICache addCache(String name) {
		
		if(this.isMultiplexMasterCache()){
			
			if(cache==null){
				cache = new DefaultCacheImpl();
			}
			return cache;
		
		} else {
			ICache cache = new DefaultCacheImpl();
			cacheMap.put(name, cache);
			return cache;
		}
		
	}

	public String getMasterCacheName() {
		return masterCacheName;
	}

	public void setMasterCacheName(String masterCacheName) {
		this.masterCacheName = masterCacheName;
	}

	
	

}


