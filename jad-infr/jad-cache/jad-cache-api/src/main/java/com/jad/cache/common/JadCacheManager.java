package com.jad.cache.common;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

/**
 * 缓存管理器
 * 封装spring的缓存管理器
 * @author Administrator
 *
 */
public interface JadCacheManager extends CacheManager  {

	/**
	 * 初始化
	 * @param jc
	 * @return
	 */
	public Cache initCache(JadCache jc);
	
	/**
	 * 创建一个新的
	 * @param jc
	 * @return
	 */
	public Cache newCache(JadCache jc);
	

}
