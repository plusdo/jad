package com.jad.cache.common.impl;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;

import com.jad.cache.common.AbstractCacheClientManager;
import com.jad.cache.common.CacheClient;
import com.jad.cache.common.MasterCacheManager;



/**
 * 单客户端管理器
 *
 */
public class SingleClientManager extends AbstractCacheClientManager{
	
	/**
	 * 管理的客户端
	 */
	private CacheClient cacheClient;
	
	
	@Override
	protected void initClients() {
		if(this.getCacheClient()==null){
			return;
		}
		
		initClient(this.getCacheClient());
		getCacheClients().put(this.getCacheClient().getClientName(),this.getCacheClient());
	}
	

	public CacheClient getCacheClient() {
		return cacheClient;
	}

	public void setCacheClient(CacheClient cacheClient) {
		this.cacheClient = cacheClient;
	}

	/**
	 * @param registry
	 */
	protected void registerMasterManagerIfNecessary(BeanDefinitionRegistry registry){
		
		//注册主缓存管理器
		if (!registry.containsBeanDefinition(CACHE_MANAGER_BEAN_NAME)) {
			RootBeanDefinition beanDefinition=new RootBeanDefinition(MasterCacheManager.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			
			beanDefinition.getPropertyValues().add("defClientName", cacheClient.getClientName());
			
			registry.registerBeanDefinition(CACHE_MANAGER_BEAN_NAME, beanDefinition);
		}
	}
	
}

