package com.jad.cache.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jad.cache.common.api.CacheHelper;

public class LocalCacheTest  {

	private static Logger logger = LoggerFactory.getLogger(LocalCacheTest.class); 
	
	@Test
    public void testCache() throws Exception {
		
		AccountService accountService = (AccountService)context.getBean("accountService");
		
		logger.debug("调用第一次。。。");
		
		accountService.getAccountByName("testUserName");
		
//		Thread.sleep(4000);//休眠一下，等缓存过期
		
		logger.debug("调用第二次。。。");
		
		accountService.getAccountByName("testUserName");
		
		CacheHelper.getInstance("accountCache6").put("sssddd", "ssssssss",999);
		
		CacheHelper.getInstance("accountCache6").get("sssdd");
		
		logger.debug("测试通过。。。");
		
	}
	
	protected ApplicationContext context ;
	
	@BeforeTest
    public void baseBeforeTest() throws Exception {
		logger.debug("context 正在初始化...");
		context = new ClassPathXmlApplicationContext(getContextConfigFile());
		logger.debug("context 初始化完成...");
	}
	
	public String getContextConfigFile() {
		return "spring-cache-local.xml";
	}
	
	
	
}
