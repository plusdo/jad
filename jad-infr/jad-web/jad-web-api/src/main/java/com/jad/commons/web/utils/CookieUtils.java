/**
 */
package com.jad.commons.web.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.utils.Exceptions;
import com.jad.commons.utils.StringUtils;

/**
 * Cookie工具类
 */
public class CookieUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(CookieUtils.class);
	
	/* 浏览器关闭时自动删除 */
	public final static int CLEAR_BROWSER_IS_CLOSED = -1;
	
	/* 立即删除 */
	public final static int CLEAR_IMMEDIATELY_REMOVE = 0;
	
	
	private static int DEFAULT_AGE=60*60*24;//默认一天

	/**
	 * 设置 Cookie（生成时间为1天）
	 * @param name 名称
	 * @param value 值
	 */
	public static void setCookie(HttpServletResponse response, String name, String value) {
		setCookie(response, name, value, "/",DEFAULT_AGE,false,false);
	}
	
	/**
	 * 设置 Cookie
	 * @param name 名称
	 * @param value 值
	 * @param maxAge 生存时间（单位秒）
	 * @param uri 路径
	 */
	public static void setCookie(HttpServletResponse response, String name, String value, String path) {
		setCookie(response, name, value, path, DEFAULT_AGE,false,false);
	}
	
	/**
	 * 设置 Cookie
	 * @param name 名称
	 * @param value 值
	 * @param maxAge 生存时间（单位秒）
	 * @param uri 路径
	 */
	public static void setCookie(HttpServletResponse response, String name, String value, int maxAge) {
		setCookie(response, name, value, "/", maxAge,false,false);
	}
	

	public static void setCookie(HttpServletResponse response, String name, String value, String path, int maxAge) {
		setCookie(response, name, value,path, maxAge,false,false);
	}
	
	
	/**
	 * 设置 Cookie
	 * @param name 名称
	 * @param value 值
	 * @param maxAge 生存时间（单位秒）
	 * @param uri 路径
	 * @param httpOnly 只读设置
	 * @param secured 是否只在Https协议下传输设置
	 */
	public static void setCookie(HttpServletResponse response, String name, String value, String path, int maxAge, boolean httpOnly, boolean secured) {
		Cookie cookie = new Cookie(name, null);
		cookie.setPath(path);
		cookie.setMaxAge(maxAge);
		try {
			cookie.setValue(URLEncoder.encode(value, "utf-8"));
		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
			Exceptions.unchecked(e);
		}
		
		/** Cookie 只在Https协议下传输设置 */
		if (secured) {
			cookie.setSecure(secured);
		}

		/** Cookie 只读设置 */
		if (httpOnly) {
			addHttpOnlyCookie(response, cookie);
		} else {
			/*
			 * //servlet 3.0 support
			 * cookie.setHttpOnly(httpOnly);
			 */
			response.addCookie(cookie);
		}
		
		
	}
	
	
	
	/**
	 * 获得指定Cookie的值
	 * @param name 名称
	 * @return 值
	 */
	public static String getCookie(HttpServletRequest request, String name) {
		return getCookie(request, null, name, false);
	}
	/**
	 * 获得指定Cookie的值，并删除。
	 * @param name 名称
	 * @return 值
	 */
	public static String clearCookie(HttpServletRequest request, HttpServletResponse response, String name) {
		return getCookie(request, response, name, true);
		
	}
	
	/**
	 * 删除cookei，不判断cookie是否存在
	 * @param response
	 * @param name
	 * @return
	 */
	public static boolean clearCookie(HttpServletResponse response, String name ) {
		return clearCookie(response,name,null,null);
	}
	
	/**
	 * 
	 * 
	 * @param response
	 * @param cookieName
	 *            cookie name
	 * @param domain
	 *            Cookie所在的域
	 * @param path
	 *            Cookie 路径
	 * @return boolean
	 */
	public static boolean clearCookie(HttpServletResponse response, String cookieName, String domain, String path) {
		boolean result = false;
		try {
			Cookie cookie = new Cookie(cookieName, "");
			cookie.setMaxAge(CLEAR_IMMEDIATELY_REMOVE);
			if(StringUtils.isNotBlank(domain)){
				cookie.setDomain(domain);
			}
			
			if(StringUtils.isNotBlank(path)){
				cookie.setPath(path);
			}
			
			response.addCookie(cookie);
			logger.info("clear cookie " + cookieName);
			result = true;
		} catch (Exception e) {
			logger.error("clear cookie " + cookieName + " is exception!\n" + e.getMessage(),e);
		}
		return result;
	}
	
	/**
	 * 获得指定Cookie的值
	 * @param request 请求对象
	 * @param response 响应对象
	 * @param name 名字
	 * @param isRemove 是否移除
	 * @return 值
	 */
	public static String getCookie(HttpServletRequest request, HttpServletResponse response, String name, boolean isRemove) {
		String value = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					try {
						value = URLDecoder.decode(cookie.getValue(), "utf-8");
					} catch (UnsupportedEncodingException e) {
//						e.printStackTrace();
						Exceptions.unchecked(e);
					}
					if (isRemove) {
						cookie.setMaxAge(CLEAR_IMMEDIATELY_REMOVE);
						response.addCookie(cookie);
					}
				}
			}
		}
		return value;
	}
	
	

	/**
	 * 
	 * <p>
	 * 解决 servlet 3.0 以下版本不支持 HttpOnly
	 * </p>
	 * 
	 * @param response
	 *            HttpServletResponse类型的响应
	 * @param cookie
	 *            要设置httpOnly的cookie对象
	 */
	public static void addHttpOnlyCookie(HttpServletResponse response, Cookie cookie) {
		if (cookie == null) {
			return;
		}
		/**
		 * 依次取得cookie中的名称、值、 最大生存时间、路径、域和是否为安全协议信息
		 */
		String cookieName = cookie.getName();
		String cookieValue = cookie.getValue();
		int maxAge = cookie.getMaxAge();
		String path = cookie.getPath();
		String domain = cookie.getDomain();
		boolean isSecure = cookie.getSecure();
		StringBuffer sf = new StringBuffer();
		sf.append(cookieName + "=" + cookieValue + ";");

		if (maxAge >= 0) {
			sf.append("Max-Age=" + cookie.getMaxAge() + ";");
		}

		if (domain != null) {
			sf.append("domain=" + domain + ";");
		}

		if (path != null) {
			sf.append("path=" + path + ";");
		}

		if (isSecure) {
			sf.append("secure;HTTPOnly;");
		} else {
			sf.append("HTTPOnly;");
		}

		response.addHeader("Set-Cookie", sf.toString());
	}
	
}
