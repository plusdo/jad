package com.jad.web.mvc.dynamic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.web.utils.RequestUtils;
import com.jad.core.sys.qo.DictQo;
import com.jad.web.mvc.BaseController;
import com.jad.web.mvc.dynamic.DynamicReqContext;


/**
 * 动态 Controller
 * 用于缺省 Controller 时使用
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = {"${adminPath}/{modelName}/{subModel}/"})
public class DynamicController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(DynamicController.class);
	
	@ModelAttribute
	public DynamicReqContext get(
			HttpServletRequest request, HttpServletResponse response, 
			@PathVariable(value="modelName") String modelName,
			@PathVariable(value="subModel") String subModel) {
		
		return DynamicReqContext.buildReqContext(request, response, modelName, subModel);
		
	}
	
	@SuppressWarnings("rawtypes")
	@RequiresPermissions("cms:link:view")
	@RequestMapping(value = {"list", ""})
	public String list(DynamicReqContext reqContext,Model model) {
		logger.debug("访问公共controller,addr:"+RequestUtils.getRemoteAddr(reqContext.getRequest())+",uri:"+reqContext.getRequest().getRequestURI());
		System.out.println("来这里了。。。,modelName:");
		
//		Page page = reqContext.getCurdService().findPage(getPage(reqContext.getRequest(), reqContext.getResponse()), qo); 
//      model.addAttribute("page", page);
		
//		List voList = reqContext.getCurdService().findAll();
//		model.addAttribute("list", voList);
//		model.addAttribute("qo", new DictQo());
		
		Object obj = reqContext.getRequest().getParameterNames();
		
		
		return reqContext.getListUrl();
	}
	
	@RequiresPermissions("cms:link:view")
	@RequestMapping(value = "form")
	public String form(DynamicReqContext reqContext,Model model) {
		return toForm(reqContext,model);
	}
	
	public String toForm(DynamicReqContext reqContext,Model model) {
//		model.addAttribute("linkVo", linkVo);
		return reqContext.getFormUrl();
	}
	
	@RequiresPermissions("cms:link:edit")
	@RequestMapping(value = "save")
	public String save(DynamicReqContext reqContext, Model model, RedirectAttributes redirectAttributes) {
		
//		if (!beanValidator(model, linkVo)){
//			return toForm(linkVo, model);
//		}
		
		return "redirect:" + adminPath + "/cms/link/?repage";
	}
	
	@RequiresPermissions("cms:link:edit")
	@RequestMapping(value = "delete")
	public String delete(DynamicReqContext reqContext,@RequestParam(required=true)String id, RedirectAttributes redirectAttributes) {
		
		return null;
	}
	

}



