package com.jad.web.mvc.swagger.conf;

import static org.springframework.util.Assert.notNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;

public class ApiDocketFactoryBean implements FactoryBean<Docket>,InitializingBean {

	private Docket docket;
	
	private ApiDocketConf apiDocketConf;
	
	
	protected ApiInfo apiInfo() {
		
		ApiInfoBuilder builder = new ApiInfoBuilder();
		if(StringUtils.isNotBlank(apiDocketConf.getTitle())){
			builder.title(apiDocketConf.getTitle());
		}
		if(StringUtils.isNotBlank(apiDocketConf.getDescription())){
			builder.description(apiDocketConf.getDescription());
		}
		if(StringUtils.isNotBlank(apiDocketConf.getTermsOfServiceUrl())){
			builder.termsOfServiceUrl(apiDocketConf.getTermsOfServiceUrl());
		}
		if(StringUtils.isNotBlank(apiDocketConf.getLicense())){
			builder.license(apiDocketConf.getLicense());
		}
		if(StringUtils.isNotBlank(apiDocketConf.getLicenseUrl())){
			builder.licenseUrl(apiDocketConf.getLicenseUrl());
		}
		if(StringUtils.isNotBlank(apiDocketConf.getVersion())){
			builder.version(apiDocketConf.getVersion());
		}
		
		Contact contact = new Contact(apiDocketConf.getContactName(),apiDocketConf.getContactUrl(),apiDocketConf.getContactEmail());
		
		builder.contact(contact);
		 
		return builder.build();
	}

	public void afterPropertiesSet() throws Exception{
		notNull(apiDocketConf, "Property 'groupName' is required");
		notNull(apiDocketConf.getGroupName(), "Property 'groupName' is required");
		
		ApiSelectorBuilder selBuilder = new Docket(DocumentationType.SWAGGER_2)
				.groupName(apiDocketConf.getGroupName())
				.apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class));
		
		if(StringUtils.isNotBlank(apiDocketConf.getPath())){
			selBuilder.paths(PathSelectors.ant(apiDocketConf.getPath()));
		}else{
			selBuilder.paths(PathSelectors.any());
		}
		
		docket = selBuilder.build();
		
//		docket = new Docket(DocumentationType.SWAGGER_2)
//				.groupName(apiDocketConf.getGroupName())
//				.apiInfo(apiInfo())
//				.select()
//				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
//				.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
////				.paths(PathSelectors.ant(apiDocketConf.getPath())) 
//				.paths(PathSelectors.any())
//				.build();
	}

	
	
	@Override
	public Docket getObject() throws Exception {
		if (this.docket == null) {
			afterPropertiesSet();
		}
		return this.docket;
	}

	@Override
	public Class<?> getObjectType() {
		return this.docket == null ? Docket.class : this.docket.getClass();
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	public void setApiDocketConf(ApiDocketConf apiDocketConf) {
		this.apiDocketConf = apiDocketConf;
	}


}
