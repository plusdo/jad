package com.jad.web.mvc.interceptor;

import javax.servlet.http.HttpServletRequest;

import com.jad.commons.context.RequestLogContext;

public class LogInterceptior extends ReqLogInterceptor{
	
//	private static SystemService systemService = SpringContextHolder.getBean("systemService");
	
	/**
	 * 获取菜单名称路径（如：系统设置-机构用户-用户管理-编辑）
	 */
//	public static String getMenuNamePath(String requestUri, String permission){
//		String href = StringUtils.substringAfter(requestUri, RequestUtils.getAdminPath());
//		Map<String, String> menuMap =systemService.getAllMenuPathMap();
//		
//		String menuNamePath = menuMap.get(href);
//		if (menuNamePath == null){
//			for (String p : StringUtils.split(permission)){
//				menuNamePath = menuMap.get(p);
//				if (StringUtils.isNotBlank(menuNamePath)){
//					break;
//				}
//			}
//			if (menuNamePath == null){
//				return "";
//			}
//		}
//		return menuNamePath;
//	}
	
	public RequestLogContext getRequestContext(HttpServletRequest request,Object handler){
		
		RequestLogContext context=super.getRequestContext(request, handler);
		
//		String permission = "";
//		if (handler instanceof HandlerMethod){
//			Method m = ((HandlerMethod)handler).getMethod();
//			RequiresPermissions rp = m.getAnnotation(RequiresPermissions.class);
//			permission = (rp != null ? StringUtils.join(rp.value(), ",") : "");
//		}
//		context.setTitle(getMenuNamePath(context.getRequestUri(), permission));
		
		return context;
		
	}




}
