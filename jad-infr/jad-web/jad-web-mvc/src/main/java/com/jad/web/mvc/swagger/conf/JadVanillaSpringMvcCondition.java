package com.jad.web.mvc.swagger.conf;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.google.common.annotations.VisibleForTesting;

public class JadVanillaSpringMvcCondition implements Condition {

	  @Override
	  public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
	    try {
	      classByName(context, "org.springframework.boot.SpringApplication");
	      return false;
	    } catch (ClassNotFoundException e) {
	      return true;
	    }
	  }

	  @VisibleForTesting
	  Class<?> classByName(ConditionContext context, String clazz) throws ClassNotFoundException {
	    return context.getClassLoader().loadClass(clazz);
	  }
	
}
