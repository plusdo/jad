package com.jad.web.mvc.dynamic;


import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import com.jad.commons.annotation.DynamicReqSupport;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.DynamicPojo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.VoMetaInfo;
import com.jad.dao.enums.RespositoryType;
import com.jad.dao.utils.EntityUtils;

public class DynamicReqScannerConfigurer implements BeanDefinitionRegistryPostProcessor, ApplicationContextAware,InitializingBean{
	
	private static final Logger logger = LoggerFactory.getLogger(DynamicReqScannerConfigurer.class);
	
	private static final String DYNAMIC_SERVICE_NAME = "com.jad.commons.service.impl.DynamicCurdServiceImpl";
	private static final String DYNAMIC_DAO_NAME = "com.jad.dao.dynamic.DynamicDaoImpl";
	
	private ApplicationContext applicationContext;
	
	private String repositoryType;
	
	private  Class dynamicServiceClass;
	private  Class dynamicDaoClass;
	
	private String baseEoPackage;
	
	private boolean dynamic = true;
	
	/**
	 * 所有DynamicReqSuppoert
	 * key为EO类的简写名称，value为class实例
	 * (目前要求系统中所有CommentEo子类必须有不同的名称)
	 */
	private static Map<String,DynamicReqAttr>DYNAMIC_REQSUPPOERT_MAP=new ConcurrentHashMap<String,DynamicReqAttr>();
	
	
	public DynamicReqScannerConfigurer(){
		try {
			dynamicServiceClass = Class.forName(DYNAMIC_SERVICE_NAME);
			dynamicDaoClass = Class.forName(DYNAMIC_DAO_NAME);
		} catch (Exception e) {
			throw new RuntimeException("无法初始化DynamicReqScannerConfigurer,"+e.getMessage(),e);
		}
	}
	
	static DynamicReqAttr findDynamicReqAttr(String name){
		return DYNAMIC_REQSUPPOERT_MAP.get(name);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		if(!dynamic){
			return ;
		}
		Assert.notNull(this.baseEoPackage, "开启动请求时必须配置baseEoPackage");
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		if(!dynamic){
			return ;
		}
		Map<String,DynamicReqSupportContext> map = scanEo(baseEoPackage);
		synchronized (this) {
			for(DynamicReqSupportContext supportContext:map.values()){
				buildDynamicReqAttr(supportContext,registry);
			}
		}
		
		
	}
	
	
	
	private DynamicReqAttr buildDynamicReqAttr(DynamicReqSupportContext supportContext,BeanDefinitionRegistry registry){
		
		DynamicReqAttr attr = new DynamicReqAttr();
		
		Class eoClass = supportContext.annotation.eoClass();
		if(eoClass == DynamicPojo.class){ //默认配置
			if(!DynamicPojo.class.isAssignableFrom(supportContext.clazz) ){
				throw new RuntimeException("类"+supportContext.clazz.getName()+"没有通过注解指定eoClass,那么它需要实现DynamicPojo接口");
			}
			eoClass = supportContext.clazz;
		}
		
		attr.setEoMetaInfo(EntityUtils.getEoInfo(eoClass));
		
		String name = StringUtils.uncapitalize(eoClass.getSimpleName());
		if(name.endsWith("Eo")){
			name = name.substring(0, name.length()-2);
		}
		if(DYNAMIC_REQSUPPOERT_MAP.containsKey(name)){
			throw new RuntimeException("DynamicReqAttr名称冲突:"+name+","+DYNAMIC_REQSUPPOERT_MAP.get(name).getName());
		}
		DYNAMIC_REQSUPPOERT_MAP.put(name, attr);
		
		attr.setName(name);
		
		
		Class voClass = supportContext.annotation.voClass();
		if(voClass == DynamicPojo.class){ //默认配置
			if(!DynamicPojo.class.isAssignableFrom(supportContext.clazz) ){
				throw new RuntimeException("类"+supportContext.clazz.getName()+"没有通过注解指定voClass,那么它需要实现DynamicPojo接口");
			}
			voClass = supportContext.clazz;
		}
		attr.setVoMetaInfo(EntityUtils.getVoInfo(voClass));
		
		Class qoClass = supportContext.annotation.qoClass();
		if(qoClass == DynamicPojo.class){ //默认配置
			if(!DynamicPojo.class.isAssignableFrom(supportContext.clazz) ){
				throw new RuntimeException("类"+supportContext.clazz.getName()+"没有通过注解指定qoClass,那么它需要实现DynamicPojo接口");
			}
			qoClass = supportContext.clazz;
		}
		attr.setQoMetaInfo(EntityUtils.getQoInfo(qoClass));
		
		String listUrl = supportContext.annotation.listUrl();
		String formUrl = supportContext.annotation.formUrl();
		attr.setListUrl(listUrl);
		attr.setFormUrl(formUrl);
		
		
		String serviceBeanName = name+"Service";
		String daoBeanName = name+"Dao"; 
		
		attr.setServiceBeanName(serviceBeanName);
		attr.setDaoBeanName(daoBeanName);
		
		registryDaoIfNecessary(daoBeanName,eoClass,registry);
		
		registryServiceIfNecessary(serviceBeanName,attr.getEoMetaInfo(),attr.getVoMetaInfo(),daoBeanName,registry);
		
		
		
		return attr;
	}
	
	
	private void registryDaoIfNecessary(String daoBeanName,Class eoClass,BeanDefinitionRegistry registry){
		try {
			if (!registry.containsBeanDefinition(daoBeanName) && !registry.isAlias(daoBeanName) ) {
				
				
				logger.debug("自动注入bean:"+daoBeanName+",class:"+DYNAMIC_DAO_NAME);
				RootBeanDefinition beanDefinition=new RootBeanDefinition(dynamicDaoClass);
				beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
				beanDefinition.setLazyInit(false);
				beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
				beanDefinition.getPropertyValues().add("entityClass", eoClass);
				
				registry.registerBeanDefinition(daoBeanName, beanDefinition);
				
				
				//注入 daoDelegate
				String daoDelegateBeanName = daoBeanName + AbstractJadEntityDao.DELEGATE_BEAN_NAME_SUFFIX;
				
				if (!registry.containsBeanDefinition(daoDelegateBeanName) && !registry.isAlias(daoDelegateBeanName) ) {
					
					logger.debug("自动注入bean:"+daoDelegateBeanName+",class:"+DYNAMIC_DAO_NAME);
					
					if(repositoryType==null){
						if(applicationContext.containsBean(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME)){
							repositoryType = applicationContext.getBean(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME).toString();
						}
					}
					if(repositoryType==null){
						throw new RuntimeException("自动注入bean失败,name:"+ daoBeanName+",原因:未知的持久化类型,请检查配置");
					}
					
					if(RespositoryType.MYBATIS.getRespositoryType().equals(repositoryType)){
						
						if(!applicationContext.containsBean("mapperScannerConfigurer")){
							throw new RuntimeException("自动注入bean失败,name:"+ daoBeanName+",原因:没有注入mapper扫描器,请检查配置");
						}
						
						Object scanner = applicationContext.getBean("mapperScannerConfigurer");
						Method method = scanner.getClass().getDeclaredMethod("newJadJaoBeanDefinition", String.class);
						if(method == null){
							throw new RuntimeException("自动注入bean失败,name:"+ daoBeanName+",mapper扫描器中没有newJadJaoBeanDefinition方法，可能是类型不正确,请检查配置");
						}
						
						RootBeanDefinition beanDelegateDefinition = (RootBeanDefinition)method.invoke(scanner, daoDelegateBeanName);
						
						registry.registerBeanDefinition(daoDelegateBeanName, beanDelegateDefinition);
						
					}else{
//						TODO
						throw new RuntimeException("自动注入bean失败,name:"+ daoBeanName+",原因:当前不支持久化类型:" + repositoryType);
					}
					
				}
				
				
			}
		} catch (Exception e) {
			logger.error("自动注入bean失败,name:"+daoBeanName+","+e.getMessage(),e);
			throw new RuntimeException("自动注入bean失败,name:"+ daoBeanName);
		}
	}
	
	
	private void registryServiceIfNecessary(
			String serviceBeanName,
			EoMetaInfo eoMetaInfo,VoMetaInfo voMetaInfo,String daoBeanName,
			BeanDefinitionRegistry registry){
		
		try {
			if (!registry.containsBeanDefinition(serviceBeanName) && !registry.isAlias(serviceBeanName)) {
				logger.debug("自动注入bean:"+serviceBeanName+",class:"+DYNAMIC_SERVICE_NAME);
				RootBeanDefinition beanDefinition=new RootBeanDefinition(dynamicServiceClass);
				beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
				beanDefinition.setLazyInit(false);
				beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
				
				beanDefinition.getPropertyValues().add("eoMetaInfo", eoMetaInfo);
				beanDefinition.getPropertyValues().add("voMetaInfo", voMetaInfo);
				beanDefinition.getPropertyValues().add("dao", new RuntimeBeanReference(daoBeanName));
			
				registry.registerBeanDefinition(serviceBeanName, beanDefinition);
			}
		} catch (Exception e) {
			logger.error("自动注入bean失败,name:"+serviceBeanName+","+e.getMessage(),e);
			throw new RuntimeException("自动注入bean失败,name:"+serviceBeanName);
		}
	}
	
	
	static class DynamicReqSupportContext{
		String className;
		DynamicReqSupport annotation;
		Class clazz;
		DynamicReqSupportContext(Class clazz,DynamicReqSupport annotation){
			this.clazz = clazz;
			this.className = clazz.getName();
			this.annotation = annotation;
		}
	}
	
	private static Map<String,DynamicReqSupportContext> scanEo(String baseEoPackage) {
		
		Map<String,DynamicReqSupportContext> map = new HashMap<String,DynamicReqSupportContext>();
				
		ResourcePatternResolver resolver = (ResourcePatternResolver) new PathMatchingResourcePatternResolver();
		
		MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resolver);
		
		
		String[] pkgArr = baseEoPackage.split(",");
		
		try {
			
			for(String pkgPath:pkgArr){
				
				String pkg = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
						+ ClassUtils.convertClassNameToResourcePath(pkgPath) + "/**/*.class";
				
				Resource[] resources = resolver.getResources(pkg);
				
				if (resources != null && resources.length > 0) {
					MetadataReader metadataReader = null;
					for (Resource resource : resources) {
						if (resource.isReadable()) {
							metadataReader = metadataReaderFactory.getMetadataReader(resource);
							Class clazz = Class.forName(metadataReader.getClassMetadata().getClassName());
							
							DynamicReqSupport annotation = (DynamicReqSupport)clazz.getAnnotation(DynamicReqSupport.class);
							if( annotation!=null && !clazz.isInterface() ){
								String name = clazz.getName();
								if(map.containsKey(name)){
									throw new RuntimeException("CommonEo类名冲突:"+clazz.getName()+","+map.get(name).className);
								}
								logger.debug("扫描到动态请求实体类:"+clazz.getName());
								map.put(name, new DynamicReqSupportContext(clazz,annotation));
								
							}
							
						}
					}
				}
			}
			
		} catch (Exception e) {
			throw new RuntimeException("扫描应用中所有的EO类时发生异常:" + baseEoPackage+","+e.getMessage(), e);
		}
		
		return map;
		
	}
	
	
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	}
	

	public String getBaseEoPackage() {
		return baseEoPackage;
	}

	public void setBaseEoPackage(String baseEoPackage) {
		this.baseEoPackage = baseEoPackage;
	}

	public boolean isDynamic() {
		return dynamic;
	}

	public void setDynamic(boolean dynamic) {
		this.dynamic = dynamic;
	}
	
	

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


}
