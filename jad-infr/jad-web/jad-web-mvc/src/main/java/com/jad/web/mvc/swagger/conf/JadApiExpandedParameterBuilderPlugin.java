package com.jad.web.mvc.swagger.conf;

import static com.google.common.base.Optional.fromNullable;
import static com.google.common.collect.Lists.transform;
import static org.springframework.core.annotation.AnnotationUtils.getAnnotation;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.service.AllowableListValues;
import springfox.documentation.service.AllowableValues;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ExpandedParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterExpansionContext;
import springfox.documentation.swagger.schema.ApiModelProperties;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.jad.commons.annotation.CURD;
import com.jad.commons.enums.CurdType;

public class JadApiExpandedParameterBuilderPlugin implements ExpandedParameterBuilderPlugin{

	@Override
	public boolean supports(DocumentationType delimiter) {
		return DocumentationType.SWAGGER_12.equals(delimiter)
		        || DocumentationType.SWAGGER_2.equals(delimiter);
	}

	@Override
	public void apply(ParameterExpansionContext context) {
		Optional<CURD> apiParamOptional = findCurdAnnotation(context.getField().getRawMember());
	    if (apiParamOptional.isPresent() && apiParamOptional.get()!=null) {
	    	fromCurdParam(context, apiParamOptional.get());
	    }
	}
	
	private CurdType getCurdTypeFrom(Parameter param){
		if(param.getVendorExtentions()==null || param.getVendorExtentions().isEmpty()){
			return null;
		}
		for(VendorExtension ext:param.getVendorExtentions()){
			if(JadModelAttributeParameterExpander.CURD_TYPE.equals(ext.getName())){
				return (CurdType)ext.getValue();
			}
		}
		return null;
	}
	
	private void fromCurdParam(ParameterExpansionContext context, CURD curdParam) {
//	    String allowableProperty = emptyToNull(curdParam.allowableValues());
//	    AllowableValues allowable = allowableValues(fromNullable(allowableProperty), context.getField().getRawMember());
//	    context.getParameterBuilder()
//	            .description(curdParam.value())
//	            .defaultValue(curdParam.defaultValue())
//	            .required(curdParam.required())
//	            .allowMultiple(curdParam.allowMultiple())
//	            .allowableValues(allowable)
//	            .parameterAccess(curdParam.access())
//	            .hidden(curdParam.hidden())
//	            .build();
		ParameterBuilder pb = context.getParameterBuilder();
		if(curdParam!=null){
			if(StringUtils.isNotBlank(curdParam.label())){
				pb.description(curdParam.label());
			}
			if(curdParam.required()){
				pb.required(true);
			}
//			Parameter p = pb.build();
//			CurdType curdType = getCurdTypeFrom(p);
//			if(curdType!=null){
//				if(curdParam.required()){
//					pb.required(true);
//				}
//			}
		}
		
		
//	    context.getParameterBuilder()
////	    .parameterType("query")
//        .description(curdParam.label())
//        .defaultValue("")
//        .required(false)
//        .allowMultiple(false)
////        .allowableValues(allowable)
//        .parameterAccess(null)
//        .hidden(false)
//        .build();
	  }
	
	 private AllowableValues allowableValues(final Optional<String> optionalAllowable, final Field field) {

		    AllowableValues allowable = null;
		    if (field.getType().isEnum()) {
		      allowable = new AllowableListValues(getEnumValues(field.getType()), "LIST");
		    } else if (optionalAllowable.isPresent()) {
		      allowable = ApiModelProperties.allowableValueFromString(optionalAllowable.get());
		    }

		    return allowable;
		  }

		  private List<String> getEnumValues(final Class<?> subject) {
		    return transform(Arrays.asList(subject.getEnumConstants()), new Function<Object, String>() {
		      @Override
		      public String apply(final Object input) {
		        return input.toString();
		      }
		    });
		  }
	
	  public static Optional<CURD> findCurdAnnotation(AnnotatedElement annotated) {
		    return fromNullable(getAnnotation(annotated, CURD.class));
		  }
	  
	  

}
